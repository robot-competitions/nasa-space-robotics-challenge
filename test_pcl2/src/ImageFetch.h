/*
 * ImageFetch.h
 *
 *  Created on: Sep 14, 2016
 *      Author: rmerriam
 */

#ifndef SRC_IMAGEFETCH_H_
#define SRC_IMAGEFETCH_H_

using namespace cv;
//---------------------------------------------------------------------------------------------------------------------
class ImageFetch {
public:
    ImageFetch(Mat image, BlobFinder& bf) :
            mCurImage(image), mBlobFinder(bf) {
    }

    bool doBlobs();

private:

    bool findBlobs(const KeyPoints kp, Mat& image);

    Mat mCurImage;

    BlobFinder& mBlobFinder;

};

#endif /* SRC_IMAGEFETCH_H_ */
