/*
 * BlobFinder.h
 *
 *  Created on: Sep 21, 2016
 *      Author: rmerriam
 */

#ifndef BLOBFINDER_H_
#define BLOBFINDER_H_
//---------------------------------------------------------------------------------------------------------------------
#include <opencv2/features2d/features2d.hpp>

using namespace cv;
using KeyPoints = vector<KeyPoint>&;
//---------------------------------------------------------------------------------------------------------------------
class BlobFinder {
public:
     BlobFinder();

     KeyPoints detect( const Mat image );

     KeyPoints keypoints() {
        return mKeyPoints;
     }

private:
     SimpleBlobDetector::Params params;
     SimpleBlobDetector* mDetector;
     vector<KeyPoint> mKeyPoints;

};
#endif /* BLOBFINDER_H_ */
