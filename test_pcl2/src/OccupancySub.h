/*
 * OccupancySub.h
 *
 *  Created on: Sep 14, 2016
 *      Author: rmerriam
 */

#ifndef OccupancySub_H_
#define OccupancySub_H_

//  points2:=/multisense/camera/points2

#include <SubscribeTopic.h>
#include <nav_msgs/OccupancyGrid.h>
using namespace nav_msgs;
//---------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------------
class OccupancySub : public SubscribeTopic<OccupancyGridPtr> {
public:
    OccupancySub(const char* msg = "projected_map") :
        SubscribeTopic(msg) {
    }
private:
    virtual void process() override {
    }

//    uint32_t mImgSeq {};
};


#endif /* OccupancySub_H_ */
