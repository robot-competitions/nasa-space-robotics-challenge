/*
 * BlobFinder.cpp
 *
 *  Created on: Sep 21, 2016
 *      Author: rmerriam
 */
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/features2d/features2d.hpp"

using namespace cv;
using namespace std;

#include "BlobFinder.h"
//---------------------------------------------------------------------------------------------------------------------
BlobFinder::BlobFinder() {
//      Change thresholds
    params.minThreshold = 10;
    params.maxThreshold = 100;
    params.thresholdStep = 2;
//    params.minRepeatability = 0;
//    params.minDistBetweenBlobs = 0;

    // Filter by Area.
    params.filterByArea = true;
    params.minArea = 300;
    params.maxArea = 30000;

    // Filter by Circularity
    params.filterByCircularity = true;
    params.minCircularity = 0.2;

    // Filter by Convexity
    params.filterByConvexity = false;
    params.minConvexity = 0.87;

    // Filter by Inertia
    params.filterByInertia = false;
    params.minInertiaRatio = 0.8;

    params.filterByColor = true;
    params.blobColor = 255;
    mDetector = new SimpleBlobDetector(params);
}
//---------------------------------------------------------------------------------------------------------------------
KeyPoints BlobFinder::detect(const Mat image) {
    mKeyPoints.clear();
    mDetector->detect(image, mKeyPoints);
    return mKeyPoints;
}
