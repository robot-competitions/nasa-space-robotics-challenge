///*
// * ImageFetch.cpp
// *
// *  Created on: Sep 14, 2016
// *      Author: rmerriam
// */

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "mystic.h"
#include <tf2_ros/transform_listener.h>
#include <tf2/impl/utils.h>
#include <tf2/LinearMath/Vector3.h>
using namespace cv;

#include "BlobFinder.h"
#include "ImageFetch.h"
//---------------------------------------------------------------------------------------------------------------------
bool ImageFetch::doBlobs() {
    KeyPoints keypoints = mBlobFinder.detect(mCurImage);
    bool res = findBlobs(keypoints, mCurImage);
    return res;
}
//---------------------------------------------------------------------------------------------------------------------
bool ImageFetch::findBlobs(const KeyPoints keypoints, Mat& matImage) {

    if (keypoints.size() != 0) {
        for (auto k : keypoints) {
            Point ipt = k.pt;

            // Draw circles around key points
            if (matImage.rows > 60 && matImage.cols > 60) {

                // check if point is in range
//                if (ipt.x < 330 || ipt.x > 690 || ipt.y < 52 || ipt.y > 370) {
//                    continue;
//                }

// display(k.size * 3.1415);
// cerr << tab;
// display(ipt.x, ipt.y);
//                // cerr << tab;
                // cerr << nl;

                circle(matImage, ipt, k.size, CV_RGB(255, 255, 255));
            }
        }

    }
    return true;
}
