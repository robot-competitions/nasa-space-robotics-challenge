#include <open_cv.h>
#include <opencv2/calib3d/calib3d.hpp>

using namespace cv;
#include <pcl/filters/voxel_grid.h>

#include <mystic.h>
using namespace mys;

#include <NeckTraj.h>
#include <ChestTraj.h>

#include <ImageVision.h>
//#include "BlobFinder.h"
//#include "ImageFetch.h"
#include <ObjectFinder.h>
#include <PathCheck.h>

//---------------------------------------------------------------------------------------------------------------------
typedef pcl::PointCloud<pcl::PointXYZRGB> PCloud;
typedef boost::shared_ptr<const PCloud> PCloudConstPtr;
typedef boost::shared_ptr<PCloud> PCloudPtr;
//---------------------------------------------------------------------------------------------------------------------
constexpr char version[] { "1.0" };

const std::string TEST_WINDOW = "test";
const std::string ASIS_WINDOW = "as is";
//---------------------------------------------------------------------------------------------------------------------
void move_neck(NeckTraj& neck, int cur_neck = 30) {

    neck.moveTime(0.25);
    neck.delayTime(0.5);

//    neck.resetPose();
//    neck.send();

    neck.setPose(Vector3(cur_neck * M_PI / 180, 0, 0));
    neck.send();
}
//---------------------------------------------------------------------------------------------------------------------
void reset_neck(NeckTraj& neck) {

    neck.moveTime(0.25);
    neck.delayTime(0.5);

    neck.resetPose();
    neck.send();
}
//---------------------------------------------------------------------------------------------------------------------
void move_chest() {

    ChestTraj mChestTraj;

    mChestTraj.delayTime(0.5);
    mChestTraj.moveTime(0.25);

    mChestTraj.resetPose();
    mChestTraj.send();

//    mChestTraj.delayTime(0.0);
//    mChestTraj.incPose(Vector3(0, 30 * M_PI / 180, 0));
//    mChestTraj.send();

}
//---------------------------------------------------------------------------------------------------------------------
void report_obj(ObjectFinder& obj) {
//    display(cerr, obj.name()) << tab;
//    display(cerr, obj.lower()) << tab;
//    display(cerr, obj.upper()) << tab;
//    display(cerr, obj.colors()) << tab;
    cerr << nl;
}
//---------------------------------------------------------------------------------------------------------------------
Mat show(Mat res, const PCloud& pc, const Contours contours, ObjectFinder& obj, const double& min_area, const Scalar& disp_color =
    Scalar(0, 255, 0)) {
    Contours big_cs;

    for (uint16_t i = 0; i < contours.size(); i++) {

        double area = contourArea(contours[i]);

        if (area > min_area) {

            big_cs.push_back(contours[i]);

            Point2i avg = accumulate(contours[i].begin(), contours[i].end(), Point(0, 0));
            avg.x /= contours[i].size();
            avg.y /= contours[i].size();

            display(cerr, avg) << tab;
            display(cerr, area) << tab;

            Vec3b bgr { res.at<Vec3b>(avg.y, avg.x) };
            display(cerr, bgr) << tab;

            pcl::PointXYZRGB pt = pc.at(avg.x, avg.y);
            display(cerr, pt.x, pt.y, pt.z);
            display(cerr, (int)(pt.b), (int)(pt.g), (int)(pt.r)) << tab;

            const tf2::Transform& world_pt = obj.getWorldLocation(pt.x, pt.y, pt.z);

            display(cerr, world_pt.getOrigin()) << nl;
        }
    }

    drawContours(res, big_cs, -1, disp_color, CV_FILLED);
    cerr << nl;
    return res;
}
//---------------------------------------------------------------------------------------------------------------------
void chessboard(Mat& res) {
    Size patternsize(4, 4); //interior number of corners
    vector<Point2f> corners; //this will be filled by the detected corners

    //CALIB_CB_FAST_CHECK saves a lot of time on images
    //that do not contain any chessboard corners
    bool patternfound = findChessboardCorners(res, patternsize, corners,
                                              CALIB_CB_ADAPTIVE_THRESH + CALIB_CB_NORMALIZE_IMAGE);
    drawChessboardCorners(res, patternsize, corners, patternfound);
    display(cerr, corners.size()) << " corners " << nl;

    for (Point2f pt : corners) {
        display(cerr, pt) << tab;
    }
    cerr << nl;
}
//---------------------------------------------------------------------------------------------------------------------
void findObjects(PointCloud2Sub& pc2) {
    enum {
        eBlueWheel, eRedWheel, ePath, eBorder, eSteps, eRails1, eRails2, eDoor, eButton, eSolarBox, eSolarPanel, eBlack, eWhite,
    };

    static ObjectFinder wheel_blue { eBlueWheel, gimp2cv(220, 0.35, 0.3), gimp2cv(255, 1.0, 0.50) };
    static ObjectFinder wheel_red { eRedWheel, gimp2cv(0, 0.90, 0.45), gimp2cv(5, 1.0, 0.65) };
//    static ObjectFinder wheel_door { eDoor, gimp2cv(55, 0, .10), gimp2cv(65, 0.05, .20) };

    static ObjectFinder button_red { eButton, gimp2cv(0, 0.70, 0.25), gimp2cv(10, 1.0, 0.60) };
    static ObjectFinder solar_panel { eSolarPanel, gimp2cv(200, 0.70, 0.10), gimp2cv(210, 1.0, 0.20) };
    static ObjectFinder solar_box { eSolarBox, gimp2cv(0, 0.0, 0.22), gimp2cv(0, 0.01, 0.24) };

    static ObjectFinder check_black { eBlack, gimp2cv(0, 0.0, 0.065), gimp2cv(0, 0.01, 0.075) };
    static ObjectFinder check_white { eWhite, gimp2cv(0, 0.0, 0.605), gimp2cv(0, 0.01, 0.615) };

    //    static ObjectFinder steps { eSteps, gimp2cv(205, 0, 0.35), gimp2cv(250, 0.10, .50) };
//
//    static ObjectFinder stair_rails1(eRails1, gimp2cv(55, 0.80, 0.30), gimp2cv(60, 1.0, .750)); // stair rails
//    static ObjectFinder stair_rails2(eRails2, gimp2cv(205, 0, 0.35), gimp2cv(250, 0.10, .50)); // stair rails

    static ObjectFinder path { ePath, gimp2cv(0, 0, 0.15), gimp2cv(0, 0.05, .20) };

    const PCloud& pc { pc2.pointCloud() };
    ImageVision iv(pc2.pointCloud());
    Mat res { iv.image() };
    //            updateWindow(res, "original");

//    chessboard(res);

    report_obj(button_red);
    show(res, pc, button_red.process(iv.image()), button_red, 10, Scalar(0, 0, 255));

//    report_obj(solar_panel);
//    show(res, pc, solar_panel.process(iv.image()), solar_panel, 100, Scalar(127, 0, 0));

//    report_obj(solar_box);
//    show(res, pc, solar_box.process(iv.image()), solar_box, 500, Scalar(0, 127, 0));

//    report_obj(check_black);
//    show(res, pc, check_black.process(iv.image()), check_black, 600, Scalar(0, 127, 127));

//    report_obj(check_white);
//    show(res, pc, check_white.process(iv.image()), check_white, 600, Scalar(0, 127, 127));

    //    report_obj(wheel_red);
//    show(res, pc, wheel_red.process(iv.image()), wheel_red, 1000, Scalar(0, 0, 255));

//    report_obj(wheel_door);
//    show(res, pc, wheel_door.process(iv.image()), wheel_door, 1000, Scalar(0, 0, 127));

//    report_obj(wheel_blue);
//    show(res, pc, wheel_blue.process(iv.image()), wheel_blue, 1000, Scalar(255, 0, 0));

//    report_obj(steps);
//    show(res, pc, steps.process(iv.image()), steps, 10000, Scalar(0, 127, 0));

//    report_obj(stair_rails1);
//    show(res, pc, stair_rails1.process(iv.image()), stair_rails1, 10000, Scalar(127, 0, 0));

//    report_obj(path);
//    show(res, pc, path.process(iv.image()), path, 10000, Scalar(127, 127, 127));

//    report_obj(stair_rails2);
//    show(res, pc, stair_rails2.process(iv.image()), stair_rails2, Scalar(127, 0, 255));

    ImageVision result(res);
    updateWindow(result.flipped(), "combined");
}
//---------------------------------------------------------------------------------------------------------------------
bool showPath(PathCheck& pc, PointCloud2Sub& pc2) {
    bool res { false };

    if (pc2.isValid()) {

        res = pc.process();
//        display(cerr, pc.worldLocation().getOrigin()) << nl;

        ImageVision iv(pc2.pointCloud());
        int iv_mid { iv.image().cols / 2 };

        Mat path = pc.path();
        Mat gray_bgr;
        cvtColor(path, gray_bgr, CV_GRAY2BGR, 3);

        iv.apply(gray_bgr, pc.subImage());

        circle(iv.image(), Point(iv_mid, pc.endPixel() - 1), 10, CV_RGB(170, 170, 170));
        updateWindow(iv.flipped(), "combined");

    }
    return res;
}
//---------------------------------------------------------------------------------------------------------------------
void doProcessing() {
    namedWindow("combined");
//    setOpenGlContext("combined");

    PointCloud2Sub pc2;
    PathCheck pc(pc2);
    NeckTraj neck;
//    move_chest();

    while ( !pc2.isValid()) {
        Duration(0.05).sleep();
    }

    reset_neck(neck);
    move_neck(neck, 30);

    for (int i = 0; i < 200; ++i) {
        if (pc2.isValid()) {

#if 1

            findObjects(pc2);
#else
            move_neck(neck);
            if (showPath(pc, pc2)) {
                reset_neck(neck);
                showPath(pc, pc2);
            }
#endif
        }
        Duration(0.1).sleep();
    }
}

//---------------------------------------------------------------------------------------------------------------------
#include "OccupancySub.h"
void oc() {
    OccupancySub map_sub;

    while ( !map_sub.isValid()) {
        Duration(0.05).sleep();
    }

    for (int i = 0; i < 2; i++) {
        OccupancyGrid map { map_sub.topic() };

        display(cerr, map.data.size()) << nl;

        const MapMetaData& info { map.info };
        display(cerr, info.origin);
        display(cerr, info.height, info.width);
        display(cerr, info.resolution) << nl;

        const vector<int8_t>& data { map.data };

        for (uint16_t row = 0; row < info.height; ++row) {

            for (uint16_t col = 0; col < info.width; ++col) {

                int pos = (row * info.width) + col;

//                int pos = (col * info.height) + row;
                int val = data.at(pos);
                if (val == 0) {
                    cerr << sp;
                }
                else if (val < 0) {
                    cerr << 'o';
                }
                else {
                    cerr << '*';
                }
//                display(cerr, (int)data.at(pos)) << sp;
            }
            cerr << nl;
        }

        Duration(.250).sleep();
    }
}
//---------------------------------------------------------------------------------------------------------------------
#include <pcl/filters/passthrough.h>

void vxl() {
    using namespace sensor_msgs;

    PointCloud2Sub pc2("voxel");

    while ( !pc2.isValid()) {
        Duration(0.05).sleep();
    }

    for (int i { }; i < 1; ++i) {
//        PCloud p { pc2.topic() };

        PCloudConstPtr ptr { new pcl::PointCloud<pcl::PointXYZRGB>(pc2.topic()) };
        PCloudPtr ptr_out { new pcl::PointCloud<pcl::PointXYZRGB> };


        pcl::PassThrough<pcl::PointXYZRGB> pass;
        pass.setInputCloud(ptr);
        pass.setFilterFieldName("x");
        pass.setFilterLimits(0.25, 2);
        pass.filter( *ptr_out);

        PCloudPtr ptr_y { new pcl::PointCloud<pcl::PointXYZRGB> };

        double y_limit { 1.25 / 2 };
        pass.setInputCloud(ptr_out);
        pass.setFilterFieldName("y");
        pass.setFilterLimits( -y_limit, y_limit);
        pass.filter( *ptr_y);

        PCloud& p( *ptr_y);

        cerr << "sizes ";
        display(cerr, ptr->size(), p.size()) << nl;

        sort(p.begin(), p.end(), [](const PointXYZRGB& lhs, const PointXYZRGB& rhs)
        {
            return lhs.z < rhs.z;
        });

        for (PCloud::iterator it = p.begin(); it != p.end(); ++it) {
            const PointXYZRGB& pt { *it };

            display(cerr, RPY { pt.z, pt.y, pt.x }, RPY { pt.b, pt.g, pt.r }) << nl;

        }
        Duration(0.250).sleep();
        cerr << nl;
    }

}
//---------------------------------------------------------------------------------------------------------------------
int main(int argc, char** argv) {

    init(argc, argv, "test_voxel");
    ROS_INFO_STREAM(ros::this_node::getName() << version);
    NodeHandle mNodeHandle;

    cerr << std::fixed << std::setprecision(2);
    ros::Time::sleepUntil(ros::Time(26));

    AsyncSpinner spinner(0);
    spinner.start();
    ROS_INFO_STREAM(ros::this_node::getName() << " spin started ");

//    doProcessing();
    vxl();

    return 0;
}
