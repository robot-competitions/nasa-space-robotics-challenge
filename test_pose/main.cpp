#include <mystic.h>
using namespace mys;

#include <RobotPose.h>
#include <start_task.h>
#include <TaskStatus.h>
#include <WorldFrame.h>
#include <ros_transform.h>
#include <display.h>
constexpr float version { 0.1 };
//---------------------------------------------------------------------------------------------------------------------

int main(int argc, char** argv) {
    init(argc, argv, "test_pose");
    NodeHandle nh;

    Time::waitForValid();

    ROS_INFO_STREAM(ros::this_node::getName() << " initialized v." << version);

    RobotPose rp { "robot_pose" };

    AsyncSpinner spinner(0);
    spinner.start();

    while ( !rp.isValid()) {
        ros::Duration(0.05).sleep();
    }
//    rp.display("start2");

//    WorldFrame wf("pelvis");
    WorldFrame wf("leftFoot");

    for (int i = 0; i < 200; ++i) {
        wf.update();
        display(cerr, wf.getTranslation()) << tab;
        display(cerr, wf.getRPY().z() * 180.0 / M_PI) << nl;

        display(cerr, wf.getWorldLocation(Vector3(0, 0, 0)).getOrigin()) << nl;

        ros::Duration(0.500).sleep();
    }

    return 0;
}
