#include <thread>
#include <iostream>
using namespace std;

#include <mystic.h>
using namespace mys;
#include <ChestTraj.h>

constexpr float version { 0.1 };

//---------------------------------------------------------------------------------------------------------------------
const double deg2rad(const double& deg) {
    return deg * M_PI / 180;
}
//---------------------------------------------------------------------------------------------------------------------
const double rad2deg(const double& rad) {
    return rad * 180.0 / M_PI;
}
//---------------------------------------------------------------------------------------------------------------------
void exercise(ChestTraj& mChestTraj) {
    double roll { 0.2 };
    double pitch { 0.1 };
    double yaw { 0.0 };

    uint32_t tim { 0 };

    for (double r = -roll; r <= roll; r += 0.1) {
        for (double p = -pitch; p <= pitch; p += 0.1) {
            for (double y = -yaw; y <= yaw; y += 0.1) {

                mChestTraj.moveTime( ++tim / 2.0);
                mChestTraj.incPose(r, p, y);
                mChestTraj.send();
                mys::display(r, p, y);
                cerr << mys::nl;
            }
        }
    }
}
//---------------------------------------------------------------------------------------------------------------------

int main(int argc, char** argv) {
    init(argc, argv, "test_head_neck");
    NodeHandle nh;

    Time::waitForValid();

    ROS_INFO_STREAM(ros::this_node::getName() << " initialized v." << version);

    AsyncSpinner spinner(0);
    spinner.start();

    cerr << std::fixed << std::setprecision(2);
    ros::Time::sleepUntil(ros::Time(26));

    ChestTraj mChestTraj;
//    exercise(mChestTraj);

    ROS_INFO_STREAM(ros::this_node::getName() << " Test Chest");

    mChestTraj.delayTime(0.5);
    mChestTraj.moveTime(0.25);
    mChestTraj.update();

    display(cerr, mChestTraj.getRPY()) << nl;

//    mChestTraj.resetPose();
//    mChestTraj.send();

    display(cerr, mChestTraj.updateRPY()) << nl;

    Vector3 wf { mChestTraj.updateRPY() };
    Vector3 pose { 0, 0, 0 };

    pose.setZ(deg2rad(20));
    mChestTraj.incPose(pose);
    mChestTraj.send();

    mChestTraj.setPose(pose / -2);
    mChestTraj.send();

    mChestTraj.update();
    display(cerr, mChestTraj.getRPY()) << nl;

    //
//
//    display(cerr, mChestTraj.updateRPY()) << nl;

//    mChestTraj.incPose(mChestTraj.updateRPY() + Vector3(0, 0, -M_PI_4));
//
//    mChestTraj.send();
//
//    display(cerr, mChestTraj.updateRPY()) << nl;
    return 0;
}
