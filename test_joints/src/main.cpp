

#include <mystic.h>
#include <JointStatus.h>

constexpr char version[] { "1.0" };

//---------------------------------------------------------------------------------------------------------------------
int main(int argc, char** argv) {
    using namespace mys;
    init(argc, argv, "test_joints");
    ROS_INFO_STREAM(ros::this_node::getName() << version);
    NodeHandle mNodeHandle;

    cerr << std::fixed << std::setprecision(2);
    ros::Time::sleepUntil(ros::Time(26));

    AsyncSpinner spinner(0);
    spinner.start();
    ROS_INFO_STREAM(ros::this_node::getName() << " spin started ");

    JointStatus js;

    for (int i = 0; i < 10; ++i) {
        if (js.isSubscribed()) {
            display(cerr, js.getName(JointStatus::Joint::lowerNeckPitch)) << tab;
            display(cerr, js.getPosition(JointStatus::Joint::lowerNeckPitch), js.getEffort(JointStatus::Joint::lowerNeckPitch)) << nl;
        }
        ::sleep(1);
    }

    ROS_INFO_STREAM(ros::this_node::getName() << " wait for shutdown ");

    return 0;
}
