/*
 * main.cpp
 *
 *  Created on: Apr 6, 2016
 *      Author: rmerriam
 */

#include <mystic.h>

#include <RobotPose.h>
#include <TaskStatus.h>
#include <HarnessStatus.h>
#include <Scoring.h>
#include <std_msgs/String.h>

using namespace mys;
using namespace ros;
//---------------------------------------------------------------------------------------------------------------------
template <typename T>
const T deg2rad(const T& deg) {
    return deg * M_PI / 180;
}
//---------------------------------------------------------------------------------------------------------------------
template <typename T>
const T rad2deg(const T& rad) {
    return rad * 180.0 / M_PI;
}
constexpr float version { 0.1 };
//--------------------------------------------------------------------------------------------------------------------------
string status_text[] {
    "Rel", "Att", "Att", "Low", "Std", "Fre"
};
//--------------------------------------------------------------------------------------------------------------------------
int main(int argc, char** argv) {

    bool was_harnessed { false };
    uint32_t harness_time { };

    try {
        init(argc, argv, "mystic_console");

        const NodeHandle nh { };
        Time::waitForValid();
        AsyncSpinner spinner(0);
        spinner.start();

        const RobotPose mPose { };
        TaskStatus mTaskStatus;
        HarnessStatus mHarnessStatus;
        Scoring mScoring;

        cout << setprecision(2) << fixed;

        while (ros::ok()) {

            cout << "---------------" << nl;
            cout << "TI:" << Time::now().sec << nl;

            cout << "TC:" << mTaskStatus.task() << '/' << mTaskStatus.checkPt() << nl;
            cout << "TT:" << mTaskStatus.startTime().toSec() << sp << mTaskStatus.elapsedTime().toSec() << nl;

            cout << "TF:" << mTaskStatus.isFinished() << nl;

            cout << "SC:"
                 << (int)mScoring.totalScore()
                 << sp
                 << mScoring.totalTime().sec
                 << sp
                 << mScoring.totalPenalty()
                 << nl;

            cout << "RP:";
            display(cout, mPose.poseX(), mPose.poseY(), mPose.poseZ());
            display(cout, rad2deg(mPose.angular().x), rad2deg(mPose.angular().y), rad2deg(mPose.angular().z)) << nl;

            cout << "HR:" << status_text[mHarnessStatus.status()] << nl;

            //==========================================================================================================
            //  Error checks and reportin
            bool have_error { };

            //-------------------
            //  Harness hung up

            if (mHarnessStatus.status() == HarnessStatus::eNone) { // not in harness
                was_harnessed = false;
                harness_time = Time::now().sec;
            }
            else {
                if (was_harnessed) { // was harnessed
                    uint32_t time_harnessed { Time::now().sec - harness_time };

                    cout << "HT:" << time_harnessed << nl;
                    if (time_harnessed > 20) {
                        cout << "BH: bad harness time" << nl;
                        have_error = true;
                    }
                }
                else {  // wan't harnessed so track time in harness
                    was_harnessed = true;
                    harness_time = Time::now().sec;
                }
            }

            //-------------------
            //  Robot Pose Bad
            if (mPose.poseZ() < 0.5) {
                cout << "PE:";
                display(cout, mPose.poseZ()) << nl;
                have_error = true;
            }

            if (have_error) {
                cout << "ERRORS" << nl;
            }

            cout << flush;

            Duration(0.500).sleep();
        }
    }
    catch (exception& e) {
        cout << "\n\n  exception " << e.what() << '\n';
    }

    return 0;
}

