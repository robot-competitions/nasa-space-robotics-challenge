#ifndef SEQUENCENODE_H
#define SEQUENCENODE_H

#include <ControlNode.h>

namespace BT {
    class SequenceNode : public ControlNode {
    public:
        SequenceNode(std::string Name);
        virtual ~SequenceNode();

        void Exec();

        int GetType();
    };
}

#endif
