#ifndef CONTROLNODE_H
#define CONTROLNODE_H

#include <vector>

#include <TreeNode.h>

namespace BT
{
    class ControlNode : public TreeNode
    {
    protected:
        // Children vector
        std::vector<TreeNode*> mChildNodes;

        // Children states
        std::vector<NodeState> mChildStates;

        // Vector size
        unsigned int mNumKids;

    public:
        // Constructor
        ControlNode(std::string Name);
        virtual ~ControlNode();

        // The method used to fill the child vector
        void AddChild(TreeNode* Child);

        // The method used to know the number of children
        unsigned int getChildrenNumber();
	std::vector<TreeNode*> getChildren();
        // The method used to interrupt the execution of the node
        bool Halt();
        void ResetColorState();
        void haltChildren(int i);
        int getDepth();

        // Methods used to access the node state without the
        // conditional waiting (only mutual access)
        bool WriteState(NodeState StateToBeSet);
    };
}

#endif
