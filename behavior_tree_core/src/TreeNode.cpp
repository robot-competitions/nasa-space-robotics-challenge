#include <TreeNode.h>

using namespace BT;

TreeNode::TreeNode(std::string Name) :
        Semaphore(0) {
    // Initialization
    this->mName = Name;
    mStateUpdated = false;
    mState = Idle;
}

TreeNode::~TreeNode() {
}

NodeState TreeNode::getNodeState() {
    NodeState ReadState;
    // Lock acquistion
    boost::unique_lock<boost::mutex> UniqueLock(mStateMutex);

    // Wait until the state is updated by the node thread
    while (mStateUpdated == false)
        mStateConditionVariable.wait(UniqueLock);

    // Reset the StateUpdated flag
    mStateUpdated = false;

    // State save
    ReadState = mState;

    // Releasing the node thread;
    mStateConditionVariable.notify_all();

    // Take the state and unlock the mutex
    return ReadState;
}

void TreeNode::setNodeState(NodeState StateToBeSet) {

    if (StateToBeSet != Idle) {
        SetColorState(StateToBeSet);
    }

    // Lock acquistion
    boost::unique_lock<boost::mutex> UniqueLock(mStateMutex);

    // State update
    mState = StateToBeSet;
    mStateUpdated = true;

    // Notification and unlock of the mutex
    mStateConditionVariable.notify_all();

    // Waiting for the father to read the state
    mStateConditionVariable.wait(UniqueLock);
}

NodeState TreeNode::readState() {
    // Lock acquistion
    boost::lock_guard<boost::mutex> LockGuard(mStateMutex);

    return mState;
}

NodeState TreeNode::ReadColorState() {
    return ColorState;
}

void TreeNode::SetColorState(NodeState ColorStateToBeSet) {
    // State update
    ColorState = ColorStateToBeSet;
    display(mName, ColorState);
}

float TreeNode::GetXPose() {
    return x_pose_;
}

void TreeNode::SetXPose(float x_pose) {
    x_pose_ = x_pose;
}

float TreeNode::GetXShift() {
    return x_shift_;
}

void TreeNode::SetXShift(float x_shift) {
    x_shift_ = x_shift;
}

std::string TreeNode::get_name() {
    return mName;
}

namespace BT {

    void display(const std::string& name, const NodeState ns) {
        std::cerr << name;
        switch (ns) {
            case Success:
                std::cerr << " Success";
                break;
            case Failure:
                std::cerr << " Failure";
                break;
            case Running:
                std::cerr << " Running";
                break;
            case Idle:
                std::cerr << " Idle";
                break;
            case Halted:
                std::cerr << " Halted";
                break;
            case Exit:
                std::cerr << " Exit";
                break;
        }
        std::cerr << std::endl;
    }

}
