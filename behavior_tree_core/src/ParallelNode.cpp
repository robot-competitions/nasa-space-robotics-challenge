#include <ParallelNode.h>

using namespace BT;

ParallelNode::ParallelNode(std::string Name) : ControlNode::ControlNode(Name)
{
    // Initializations
    N = std::numeric_limits<unsigned int>::max();
    StateUpdate = false;

    // Thread start
    Thread = boost::thread(&ParallelNode::Exec, this);
}

ParallelNode::~ParallelNode() {}

void ParallelNode::SetThreshold(unsigned int N)
{
    // Vector size initialization
    mNumKids = mChildNodes.size();

    // Checking N correctness
    if (N > mNumKids)
    {
        std::stringstream S;
        S << "Wrong N threshold for '" << mName << "'. M=" << mNumKids << " while N=" << N << ". N should be <= M.";
        throw BehaviorTreeException(S.str());
    }

    this->N = N;

    // First tick for the thread
    Semaphore.Signal();
}

void ParallelNode::Exec()
{
    unsigned int i;

    // Waiting for a first tick to come
    Semaphore.Wait();

    // Checking N correctness
    if (N == std::numeric_limits<unsigned int>::max())
    {
        throw BehaviorTreeException("'" + mName + "' has no valid N threashold set. You should set it before ticking the node.");
    }

    // Vector construction
    for (i=0; i<mNumKids; i++)
    {
        ChildStatesUpdated.push_back(false);
    }

    while(true)
    {
        // Waiting for a tick to come
        Semaphore.Wait();

        if(readState() == Exit)
        {
            // The behavior tree is going to be destroied
            return;
        }

        // Variables reset
        StateUpdate = false;
        Successes = Failures = Runnings = 0;
        for (i=0; i<mNumKids; i++)
        {
            ChildStatesUpdated[i] = false;
        }

        // Checking if i was halted
        if (readState() != Halted)
        {
            std::cout << mName << " ticked, ticking actions..." << std::endl;

            // If not, before ticking the children, checking if a state can
            // be immediatelly returned.
            for(i = 0; i<mNumKids; i++)
            {
                if (mChildNodes[i]->mType != Action)
                {
                    continue;
                }

                mChildStates[i] = mChildNodes[i]->readState();

                if (mChildStates[i] == Success)
                {
                    // The action node has finished, sync
                    mChildNodes[i]->Semaphore.Signal();

                    Successes++;
                    ChildStatesUpdated[i] = true;
                }
                else if (mChildStates[i] == Failure)
                {
                    // The action node has finished, sync
                    mChildNodes[i]->Semaphore.Signal();

                    Failures++;
                    ChildStatesUpdated[i] = true;
                }
                else if (mChildStates[i] == Running)
                {
                    Runnings++;
                    ChildStatesUpdated[i] = true;
                }

                if (Successes >= N)
                {
                    // Returning success
                    setNodeState(Success);
                    std::cout << mName << " returning Success! " << std::endl;
                    StateUpdate = true;

                    // Exit the for loop
                    break;
                }
                else if (Failures > mNumKids - N)
                {
                    // Returning failure
                    setNodeState(Failure);
                    std::cout << mName << " returning Failure! " << std::endl;
                    StateUpdate = true;

                    // Exit the for loop
                    break;
                }
                else if (Runnings > mNumKids - N && Runnings >= N)
                {
                    // Neither a Success nor a Failure could be returned
                    // Returning Running
                    setNodeState(Running);
                    std::cout << mName << " returning Running! " << std::endl;
                    StateUpdate = true;

                    // Exit the for loop
                    break;
                }
            }

            if (StateUpdate == true)
            {
                // If it is known what to return...
                std::cout << mName << " knows what to return... " << std::endl;

                if (readState() == Success || readState() == Failure)
                {
                    // Halting the running actions
                    for (i=0; i<mNumKids; i++)
                    {
                        if (ChildStatesUpdated[i] == false || mChildStates[i] != Running)
                        {
                            continue;
                        }

                        std::cout << mName << " trying halting (action) child number " << i << "..." << std::endl;

                        if (mChildNodes[i]->halt() == false)
                        {
                            // this means that, before this node could set its child state
                            // to "Halted", the child had already written the action outcome;
                            // sync with him ignoring its state;
                            mChildNodes[i]->Semaphore.Signal();

                            std::cout << mName << " halting of child number " << i << " failed!" << std::endl;
                        }
                        else
                        {
                            std::cout << mName << " halting of child number " << i << " succedeed!" << std::endl;
                        }

                        // updating its vector cell;
                        mChildStates[i] = Idle;
                    }

                    // Ticking the other children, but halting them if they
                    // return Running.
                    std::cout << mName << " ticking the remaining children... " << std::endl;

                    for(i = 0; i<mNumKids; i++)
                    {
                        if (ChildStatesUpdated[i] == true)
                        {
                            continue;
                        }

                        // ticking it;
                        mChildNodes[i]->Semaphore.Signal();

                        // retrive its state as soon as it is available;
                        mChildStates[i] = mChildNodes[i]->getNodeState();
                        if (mChildStates[i] == Running)
                        {
                            std::cout << mName << " halting (control) child number " << i << "..." << std::endl;

                            // if it is running, halting it;
                            mChildNodes[i]->halt();

                            // sync with it (it's waiting on the semaphore);
                            mChildNodes[i]->Semaphore.Signal();
                        }

                        // updating its vector cell;
                        mChildStates[i] = Idle;
                        ChildStatesUpdated[i] = true;
                    }

                    // Resetting the node state
                    if (readState() != Halted)
                    {
                        WriteState(Idle);
                    }

                    // Next cicle
                    continue;
                }
                else if (Runnings + Failures + Successes < mNumKids)
                {
                    // Returning running! But some children haven't been ticked yet!

                    std::cout << mName << " ticking the remaining children and ignoring their state... " << std::endl;

                    // Ticking the remaining children (ignoring their states)
                    for(i = 0; i<mNumKids; i++)
                    {
                        if (ChildStatesUpdated[i] == true)
                        {
                            continue;
                        }

                        if (mChildNodes[i]->mType == Action)
                        {
                            // if it's an action:
                            // read its state;
                            NodeState ActionState = mChildNodes[i]->readState();

                            if (ActionState == Idle)
                            {
                                // if it's "Idle":
                                // ticking it;
                                mChildNodes[i]->Semaphore.Signal();

                                // retriving its state as soon as it is available;
                                mChildStates[i] = mChildNodes[i]->getNodeState();
                            }
                            else if (ActionState == Running)
                            {
                                // It's ok, it can continue running
                                mChildStates[i] = Running;
                            }
                            else
                            {
                                // if it's "Success" of "Failure" (it can't be "Halted"!):
                                // sync with it;
                                mChildNodes[i]->Semaphore.Signal();

                                // ticking it;
                                mChildNodes[i]->Semaphore.Signal();

                                // retriving its state as soon as it is available;
                                mChildStates[i] = mChildNodes[i]->getNodeState();
                            }
                        }
                        else
                        {
                            // if it's not an action:
                            // ticking it;
                            mChildNodes[i]->Semaphore.Signal();

                            // retrive its state as soon as it is available;
                            mChildStates[i] = mChildNodes[i]->getNodeState();
                        }
                    }

                    continue;
                }
                else
                {
                    // Returning Running! All the children have already been ticked.

                    continue;
                }
            }

            // If it wasn't possible to decide which state to return
            // by considerating only the action nodes, the remaining children
            // must be ticked and their state must be considered

            std::cout << mName << " doesn't know yet what to return, ticking the remaining children..." << std::endl;

            // For each remained child (conditions and controls):
            for(i = 0; i<mNumKids; i++)
            {
                if (ChildStatesUpdated[i] == true)
                {
                    continue;
                }

                // ticking it;
                mChildNodes[i]->Semaphore.Signal();

                // retrive its state as soon as it is available;
                mChildStates[i] = mChildNodes[i]->getNodeState();

                if (mChildStates[i] == Success)
                {
                    Successes++;
                    ChildStatesUpdated[i] = true;
                }
                else if (mChildStates[i] == Failure)
                {
                    Failures++;
                    ChildStatesUpdated[i] = true;
                }
                else
                {
                    Runnings++;
                    ChildStatesUpdated[i] = true;
                }

                if (Successes >= N)
                {
                    // Returning success
                    setNodeState(Success);
                    std::cout << mName << " returning Success! " << std::endl;
                    StateUpdate = true;

                    // Exit the for loop
                    break;
                }
                else if (Failures > mNumKids - N)
                {
                    // Returning failure
                    setNodeState(Failure);
                    std::cout << mName << " returning Failure! " << std::endl;
                    StateUpdate = true;

                    // Exit the for loop
                    break;
                }
                else if (Runnings > mNumKids - N && Runnings >= N)
                {
                    // Neither a Success nor a Failure could be returned
                    // Returning Running
                    setNodeState(Running);
                    std::cout << mName << " returning Running! " << std::endl;
                    StateUpdate = true;

                    // Exit the for loop
                    break;
                }
            }

            if (StateUpdate == true && readState() != Running)
            {
                std::cout << mName << " knows what to return... " << std::endl;

                // Halting all the running nodes (that have been ticked!)
                for (i=0; i<mNumKids; i++)
                {
                    if (ChildStatesUpdated[i] == false || mChildStates[i] != Running)
                    {
                        continue;
                    }

                    if (mChildNodes[i]->mType == Action)
                    {
                        std::cout << mName << " trying halting (action) child number " << i << "..." << std::endl;

                        if (mChildNodes[i]->halt() == false)
                        {
                            // this means that, before this node could set its child state
                            // to "Halted", the child had already written the action outcome;
                            // sync with him ignoring its state;
                            mChildNodes[i]->Semaphore.Signal();

                            std::cout << mName << " halting of child number " << i << " failed!" << std::endl;
                        }
                        else
                        {
                            std::cout << mName << " halting of child number " << i << " succedeed!" << std::endl;
                        }
                    }
                    else
                    {
                        // halting it;
                        mChildNodes[i]->halt();

                        // sync with it (it's waiting on the semaphore);
                        mChildNodes[i]->Semaphore.Signal();

                        std::cout << mName << " halting child number " << i << "!" << std::endl;
                    }

                    // updating its vector cell;
                    mChildStates[i] = Idle;
                }

                // Ticking the other children, but halting them is they
                // return Running.
                std::cout << mName << " ticking the remaining children... " << std::endl;

                for(i = 0; i<mNumKids; i++)
                {
                    if (ChildStatesUpdated[i] == true)
                    {
                        continue;
                    }

                    // ticking it;
                    mChildNodes[i]->Semaphore.Signal();

                    // retrive its state as soon as it is available
                    mChildStates[i] = mChildNodes[i]->getNodeState();

                    if (mChildStates[i] == Running)
                    {
                        std::cout << mName << " halting (control) child number " << i << "..." << std::endl;

                        // if it is running, halting it;
                        mChildNodes[i]->halt();

                        // sync with it (it's waiting on the semaphore);
                        mChildNodes[i]->Semaphore.Signal();

                        // updating its vector cell;
                        mChildStates[i] = Idle;
                    }
                }

                // Resetting the node state
                if (readState() != Halted)
                {
                    WriteState(Idle);
                }
            }
            else if (StateUpdate == true && Runnings + Failures + Successes < mNumKids)
            {
                // Returning running, but there still children to be ticked

                std::cout << mName << " ticking the remaining children and ignoring their state... " << std::endl;

                // Ticking the remaining children (ignoring their states)
                for(i = 0; i<mNumKids; i++)
                {
                    if (ChildStatesUpdated[i] == true)
                    {
                        continue;
                    }

                    // ticking it;
                    mChildNodes[i]->Semaphore.Signal();

                    // state sync;
                    mChildStates[i] = mChildNodes[i]->getNodeState();
                }
            }
            else if (StateUpdate == false)
            {
                // Returning running!
                setNodeState(Running);
                std::cout << mName << " returning Running! " << std::endl;
                StateUpdate = true;
            }
        }
        else
        {
            // If it was halted, all the "busy" children must be halted too
            std::cout << mName << " halted! Halting all the children..." << std::endl;

     /*       for(i=0; i<M; i++)
            {
                if (ChildNodes[i]->Type != Action && ChildStates[i] == Running)
                {
                    // if the control node was running:
                    // halting it;
                    ChildNodes[i]->Halt();

                    // sync with it (it's waiting on the semaphore);
                    ChildNodes[i]->Semaphore.Signal();

                    std::cout << Name << " halting child number " << i << "!" << std::endl;
                }
                else if (ChildNodes[i]->Type == Action && ChildNodes[i]->ReadState() == Running)
                {
                    std::cout << Name << " trying halting child number " << i << "..." << std::endl;

                    // if it's a action node that hasn't finished its job:
                    // trying to halt it:
                    if (ChildNodes[i]->Halt() == false)
                    {
                        // this means that, before this node could set its child state
                        // to "Halted", the child had already written the action outcome;
                        // sync with him ignoring its state;
                        ChildNodes[i]->Semaphore.Signal();

                        std::cout << Name << " halting of child number " << i << " failed!" << std::endl;
                    }
                    else
                    {
                        std::cout << Name << " halting of child number " << i << " succedeed!" << std::endl;
                    }
                }
                else if (ChildNodes[i]->Type == Action && ChildNodes[i]->ReadState() != Idle)
                {
                    // if it's a action node that has finished its job:
                    // ticking it without saving its returning state;
                    ChildNodes[i]->Semaphore.Signal();
                }

                // updating its vector cell
                ChildStates[i] = Idle;
            }
*/
            haltChildren(0);
            // Resetting the node state
            WriteState(Idle);
        }
    }
}


int ParallelNode::GetType()
{
    // Lock acquistion

    return PARALLEL;
}
