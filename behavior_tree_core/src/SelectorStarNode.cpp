#include <SelectorStarNode.h>

using namespace BT;

SelectorStarNode::SelectorStarNode(std::string Name) : ControlNode::ControlNode(Name)
{
    // Thread start
    Thread = boost::thread(&SelectorStarNode::Exec, this);
}

SelectorStarNode::~SelectorStarNode() {}

void SelectorStarNode::Exec()
{
    unsigned int i;

    // Waiting for the first tick to come
    Semaphore.Wait();

    // Vector size initialization
    mNumKids = mChildNodes.size();

    // Simulating a tick for myself
    Semaphore.Signal();
    i = 0; //Initialize the index of the child to tick

    while(true)
    {
        // Waiting for a tick to come
        Semaphore.Wait();

        if(readState() == Exit)
        {
            // The behavior tree is going to be destroied
            return;
        }

        // Checking if i was halted
        if (readState() != Halted)
        {
            // If not, the children can be ticked
            std::cout << mName << " ticked, ticking children..." << std::endl;

            // For each child:
            while(i < mNumKids)
            {
                if (mChildNodes[i]->mType == Action)
                {
                    // 1) if it's an action:
                    // 1.1) read its state;
                    NodeState ActionState = mChildNodes[i]->readState();

                    if (ActionState == Idle)
                    {
                        // 1.2) if it's "Idle":
                        // 1.2.1) ticking it;
                        mChildNodes[i]->Semaphore.Signal();

                        // 1.2.2) retrive its state as soon as it is available;
                        mChildStates[i] = mChildNodes[i]->getNodeState();
                    }
                    else if (ActionState == Running)
                    {
                        // 1.3) if it's "Running":
                        // 1.3.1) saving "Running"
                        mChildStates[i] = Running;
                    }
                    else
                    {
                        // 1.4) if it's "Success" of "Failure" (it can't be "Halted"!):
                        // 1.2.1) ticking it;
                        mChildNodes[i]->Semaphore.Signal();

                        // 1.2.2) saving the read state;
                        mChildStates[i] = ActionState;
                    }
                }
                else
                {
                    // 2) if it's not an action:
                    // 2.1) ticking it;
                    mChildNodes[i]->Semaphore.Signal();

                    // 2.2) retrive its state as soon as it is available;
                    mChildStates[i] = mChildNodes[i]->getNodeState();
                }

                // 3) if the child state is not a success:
                if(mChildStates[i] != Failure)
                {


                    // 3.1) the node state is equal to it;
                    setNodeState(mChildStates[i]);

                    // 3.2) state reset;
                    WriteState(Idle);
                    if (mChildStates[i] == Success)
                    {
                     i = 0; // Final State of rhe selector node. Child index reinitialized
                     }
                    // 3.3) all the next action or control child nodes must be halted:
                   /* for(int j=i+1; j<M; j++)
                    {
                        if (ChildNodes[j]->Type != Action && ChildStates[j] == Running)
                        {
                            // 3.3.1) if the control node was running:
                            // 3.3.1.1) halting it;
                            ChildNodes[j]->Halt();

                            // 3.3.1.2) sync with it (it's waiting on the semaphore);
                            ChildNodes[j]->Semaphore.Signal();

                            // 3.3.1.3) updating its vector cell
                            ChildStates[j] = Idle;

                            std::cout << Name << " halting child number " << j << "!" << std::endl;
                        }
                        else if (ChildNodes[j]->Type == Action && ChildNodes[j]->ReadState() == Running)
                        {
                            std::cout << Name << " trying halting child number " << j << "..." << std::endl;

                            // 3.3.2) if it's a action node that hasn't finished its job:
                            // 3.3.2.1) trying to halt it:
                            if (ChildNodes[j]->Halt() == false)
                            {
                                // 3.3.2.1.1) this means that, before this node could set its child state
                                //            to "Halted", the child had already written the action outcome;
                                //            sync with him ignoring its state;
                                ChildNodes[j]->Semaphore.Signal();

                                std::cout << Name << " halting of child number " << j << " failed!" << std::endl;
                            }

                            std::cout << Name << " halting of child number " << j << " succedeed!" << std::endl;
                        }
                        else if (ChildNodes[j]->Type == Action && ChildNodes[j]->ReadState() != Idle)
                        {
                            // 3.3.3) if it's a action node that has finished its job:
                            // 3.3.3.1) ticking it without saving its returning state;
                            ChildNodes[j]->Semaphore.Signal();
                        }
                    }*/

                    std::cout << mName << " returning " << mChildStates[i] << "!" << std::endl;

                    // 3.4) the while loop must end here.
                    break;
                } else if(mChildStates[i] == Failure)//if child i has failed the selector star node can tick the next child
                {
                    i ++;
                }
            }

            if (i == mNumKids)
            {
                // 4) if all of its children return "failure":
                // 4.1) the node state must be "failure";
                setNodeState(Failure);
                i = 0;
                // 4.2) resetting the state;
                WriteState(Idle);

                std::cout << mName << " returning " << Success << "!" << std::endl;
            }
        }
        else
        {
            // If it was halted, all the "busy" children must be halted too
            std::cout << mName << " halted! Halting all the children..." << std::endl;

            /*for(unsigned int j=0; j<M; j++)
            {
                if (ChildNodes[j]->Type != Action && ChildStates[j] == Running)
                {
                    // if the control node was running:
                    // halting it;
                    ChildNodes[j]->Halt();

                    // sync with it (it's waiting on the semaphore);
                    ChildNodes[j]->Semaphore.Signal();

                    std::cout << Name << " halting child number " << j << "!" << std::endl;
                }
                else if (ChildNodes[j]->Type == Action && ChildNodes[j]->ReadState() == Running)
                {
                    std::cout << Name << " trying halting child number " << j << "..." << std::endl;

                    // if it's a action node that hasn't finished its job:
                    // trying to halt it:
                    if (ChildNodes[j]->Halt() == false)
                    {
                        // this means that, before this node could set its child state
                        // to "Halted", the child had already written the action outcome;
                        // sync with him ignoring its state;
                        ChildNodes[j]->Semaphore.Signal();

                        std::cout << Name << " halting of child number " << j << " failed!" << std::endl;
                    }

                    std::cout << Name << " halting of child number " << j << " succedeed!" << std::endl;
                }
                else if (ChildNodes[j]->Type == Action && ChildNodes[j]->ReadState() != Idle)
                {
                    // if it's a action node that has finished its job:
                    // ticking it without saving its returning state;
                    ChildNodes[j]->Semaphore.Signal();
                }

                // updating its vector cell;
                ChildStates[j] = Idle;
            }
*/
            haltChildren(0);
            // Resetting the node state
            WriteState(Idle);
        }
    }
}


int SelectorStarNode::GetType()
{
    // Lock acquistion

    return SELECTORSTAR;
}
