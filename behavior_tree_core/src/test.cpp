#include "BehaviorTree.h"
#include <ros/ros.h>
using namespace ros;
using namespace BT;

int mainx(int argc, char **argv) {
    try {
        int TickPeriod_milliseconds = 1000;
        ros::init(argc, argv, "ros btree");

        AsyncSpinner spinner(0);
        spinner.start();

        ROSAction* action = new ROSAction("action");
        ROSCondition* condition = new ROSCondition("condition");

        SequenceNode* sequence1 = new SequenceNode("seq1");

        sequence1->AddChild(condition);
        sequence1->AddChild(action);

        Execute(sequence1, TickPeriod_milliseconds); //from BehaviorTree.cpp
    }
    catch (BehaviorTreeException& Exception) {
        std::cerr << Exception.what() << std::endl;
    }

    return 0;
}

