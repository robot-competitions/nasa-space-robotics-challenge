#include <SequenceNode.h>

using namespace BT;

SequenceNode::SequenceNode(std::string Name) :
        ControlNode::ControlNode(Name) {

    Thread = boost::thread( &SequenceNode::Exec, this);
}

SequenceNode::~SequenceNode() {
}

void SequenceNode::Exec() {
    unsigned int i;

    // Waiting for the first tick to come
    Semaphore.Wait();

    mNumKids = mChildNodes.size();

    // Simulating a tick for myself
    Semaphore.Signal();

    while (true) {
        // Waiting for a tick to come
        Semaphore.Wait();

        if (readState() == Exit) {
            // The behavior tree is going to be destroyed
            return;
        }

        // Checking if i was halted
        if (readState() != Halted) {

            // If not, the children can be ticked

            // For each child:
            for (i = 0; i < mNumKids; i++) {

                if (mChildNodes[i]->mType == Action) {
                    NodeState ActionState = mChildNodes[i]->readState();

                    if (ActionState == Idle) {
                        mChildNodes[i]->Semaphore.Signal();
                        mChildStates[i] = mChildNodes[i]->getNodeState();
                    }
                    else if (ActionState == Running) {
                        mChildStates[i] = Running;
                    }
                    else {
                        mChildNodes[i]->Semaphore.Signal();
                        mChildStates[i] = ActionState;
                    }
                }
                else {
                    mChildNodes[i]->Semaphore.Signal();
                    mChildStates[i] = mChildNodes[i]->getNodeState();
                }

                if (mChildStates[i] != Success) {
                    setNodeState(mChildStates[i]);
                    WriteState(Idle);
                    haltChildren(i + 1);
                    break;
                }
            }

            if (i == mNumKids) {
                setNodeState(Success);
                WriteState(Idle);
            }
        }
        else {
            haltChildren(0);
            WriteState(Idle);
        }
    }
}

int SequenceNode::GetType() {
    return SEQUENCE;
}

