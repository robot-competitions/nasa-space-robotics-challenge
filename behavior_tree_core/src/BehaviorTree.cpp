#include<BehaviorTree.h>

using namespace BT;
//---------------------------------------------------------------------------------------------------------------------
void Execute(ControlNode* root, int TickPeriod_milliseconds) {

    // Starts in another thread the drawing of the BT
    boost::thread t( &drawTree, root);

    root->ResetColorState();

    while (true) {

        // Ticking the root node
        root->Semaphore.Signal();
        root->getNodeState();

        if (root->readState() != Running) {
            //when the root returns a status it resets the colors of the tree
            root->ResetColorState();
        }
        boost::this_thread::sleep(boost::posix_time::milliseconds(TickPeriod_milliseconds));
    }
}
