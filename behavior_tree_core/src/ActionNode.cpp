#include <ActionNode.h>

using namespace BT;

ActionNode::ActionNode(std::string Name) :
        LeafNode::LeafNode(Name) {
    mType = Action;
}

ActionNode::~ActionNode() {
}

bool ActionNode::writeState(NodeState StateToBeSet) {

    if (StateToBeSet != Idle) {
        SetColorState(StateToBeSet);
    }
    boost::lock_guard<boost::mutex> LockGuard(mStateMutex);

    // Check for spurious "Halted"
    if (mState == Halted && StateToBeSet != Idle && StateToBeSet != Exit) {
        return false;
    }

    mState = StateToBeSet;
    return true;
}

int ActionNode::getType() {
    return ACTION;
}
