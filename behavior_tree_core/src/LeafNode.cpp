#include <LeafNode.h>

using namespace BT;

LeafNode::LeafNode(std::string Name) :
        TreeNode(Name) {
}

LeafNode::~LeafNode() {
}

void LeafNode::ResetColorState() {
    ColorState = Idle;
}

int LeafNode::getDepth() {
    return 0;
}
