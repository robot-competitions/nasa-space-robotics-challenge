#include <Conditions/ROSCondition.h>

using namespace BT;

enum Status {
    RUNNING, SUCCESS, FAILURE
};

ROSCondition::ROSCondition(std::string Name) :
        ConditionNode::ConditionNode(Name) {
    mType = Condition;

    // Thread start
    Thread = boost::thread( &ROSCondition::Exec, this);
}

ROSCondition::~ROSCondition() {
}

void ROSCondition::Exec() {

    ROS_INFO("Waiting For the Condition %s to start", mName.c_str());

    actionlib::SimpleActionClient<behavior_tree_core::BTAction> ac(mName, true);

    ac.waitForServer(); //will wait for infinite time until the server starts
    ROS_INFO("The Condition %s has started", mName.c_str());

    behavior_tree_core::BTGoal goal;
    while (true) {

        ROS_INFO("The Condition %s waiting for tick", mName.c_str());

        // Waiting for a tick to come
        Semaphore.Wait();
        ROS_INFO("The Condition %s ticked", mName.c_str());

        node_result.status = 0;

        ROS_INFO("I am running the request");

        if (readState() == Exit) {
            // The behavior tree is going to be destroyed
            return;
        }

        // Condition checking and state update
        // do{
        ac.sendGoal(goal);
        ac.waitForResult(ros::Duration(30.0));
        node_result = *(ac.getResult());

        std::cerr << " Condition Status" << node_result.status << "!" << std::endl;

        // }while(node_result.status != 2 && node_result.status != 1 ); //if it is not halted and has not returned a status

        if (node_result.status == SUCCESS) {
            setNodeState(BT::Success);
            std::cerr << mName << " returning Success" << Success << "!" << std::endl;
        }
        else if (node_result.status == FAILURE) {
            setNodeState(BT::Failure);
            std::cerr << mName << " returning Failure" << Failure << "!" << std::endl;
        }
        else {
            setNodeState(BT::Failure);
            std::cerr << mName << " returning NOTHING" << Failure << "!" << std::endl;
        }

        // Resetting the state
        WriteState(Idle);
    }
}
