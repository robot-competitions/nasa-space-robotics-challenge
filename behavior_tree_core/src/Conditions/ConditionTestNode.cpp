#include <Conditions/ConditionTestNode.h>

using namespace BT;
//---------------------------------------------------------------------------------------------------------------------
ConditionTestNode::ConditionTestNode(std::string Name) :
        ConditionNode::ConditionNode(Name) {

    mType = Condition;
    // Thread start
    Thread = boost::thread( &ConditionTestNode::Exec, this);
}
//---------------------------------------------------------------------------------------------------------------------
ConditionTestNode::~ConditionTestNode() {
}
//---------------------------------------------------------------------------------------------------------------------
void ConditionTestNode::Exec() {
    int i = 0;
    while (true) {

        Semaphore.Wait();

        if (readState() == Exit) {
            return;
        }

        // Condition checking and state update
        i++;
        if (i < 5) {
            setNodeState(Success);
        }
        else if (i < 10) {
            setNodeState(Failure);
        }
        else {
            setNodeState(Failure);
            i = 0;
        }
        std::cerr << std::endl;

        // Resetting the state
        WriteState(Idle);
    }
}
