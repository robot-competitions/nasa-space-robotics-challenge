#include <DecoratorNegationNode.h>

using namespace BT;

DecoratorNegationNode::DecoratorNegationNode(std::string Name) : ControlNode::ControlNode(Name)
{
    // Thread start
    Thread = boost::thread(&DecoratorNegationNode::Exec, this);
}

DecoratorNegationNode::~DecoratorNegationNode() {}

void DecoratorNegationNode::Exec()
{


    // Waiting for the first tick to come
    Semaphore.Wait();

    // Vector size initialization
    mNumKids = mChildNodes.size();

    // Simulating a tick for myself
    Semaphore.Signal();

    while(true)
    {
        // Waiting for a tick to come
        Semaphore.Wait();

        if(readState() == Exit)
        {
            // The behavior tree is going to be destroied
            return;
        }

        // Checking if i was halted
        if (readState() != Halted)
        {
            // If not, the children can be ticked
            std::cout << mName << " ticked, ticking children..." << std::endl;




                if (mChildNodes[0]->mType == Action)
                {
                    // 1) if it's an action:
                    // 1.1) read its state;
                    NodeState ActionState = mChildNodes[0]->readState();

                    if (ActionState == Idle)
                    {
                        // 1.2) if it's "Idle":
                        // 1.2.1) ticking it;
                        mChildNodes[0]->Semaphore.Signal();

                        // 1.2.2) retrive its state as soon as it is available;
                        mChildStates[0] = mChildNodes[0]->getNodeState();
                    }
                    else if (ActionState == Running)
                    {
                        // 1.3) if it's "Running":
                        // 1.3.1) saving "Running"
                        mChildStates[0] = Running;
                    }
                    else
                    {
                        // 1.4) if it's "Success" of "Failure" (it can't be "Halted"!):
                        // 1.2.1) ticking it;
                        mChildNodes[0]->Semaphore.Signal();

                        // 1.2.2) saving the read state;
                        mChildStates[0] = ActionState;
                    }
                }
                else
                {
                    // 2) if it's not an action:
                    // 2.1) ticking it;
                    mChildNodes[0]->Semaphore.Signal();

                    // 2.2) retrive its state as soon as it is available;
                    mChildStates[0] = mChildNodes[0]->getNodeState();
                }

                // 3) if the child state is a success:
                if(mChildStates[0] == Success)
                {
                    // 3.1) the node state is equal to failure since I am negating the status
                    setNodeState(Failure);

                    // 3.2) resetting the state;
                    WriteState(Idle);

                    std::cout << mName << " returning " << Failure << "!" << std::endl;
                }
                else if(mChildStates[0] == Failure)
                {
                    // 4.1) the node state is equal to success since I am negating the status
                    setNodeState(Success);

                    // 4.2) state reset;
                    WriteState(Idle);

                    std::cout << mName << " returning " << Success << "!" << std::endl;

                } else
                // 5) if the child state is  running
                {
                    // 5.1) the node state is equal to running
                    setNodeState(Running);

                    // 5.2) state reset;
                    WriteState(Idle);
                }

        }
        else
        {
            // If it was halted, all the "busy" children must be halted too
            std::cout << mName << " halted! Halting all the children..." << std::endl;

                if (mChildNodes[0]->mType != Action && mChildStates[0] == Running)
                {
                    // if the control node was running:
                    // halting it;
                    mChildNodes[0]->halt();

                    // sync with it (it's waiting on the semaphore);
                    mChildNodes[0]->Semaphore.Signal();

                    std::cout << mName << " halting child  "  << "!" << std::endl;
                }
                else if (mChildNodes[0]->mType == Action && mChildNodes[0]->readState() == Running)
                {
                    std::cout << mName << " trying halting child  "  << "..." << std::endl;

                    // if it's a action node that hasn't finished its job:
                    // trying to halt it:
                    if (mChildNodes[0]->halt() == false)
                    {
                        // this means that, before this node could set its child state
                        // to "Halted", the child had already written the action outcome;
                        // sync with him ignoring its state;
                        mChildNodes[0]->Semaphore.Signal();

                        std::cout << mName << " halting of child  "  << " failed!" << std::endl;
                    }

                    std::cout << mName << " halting of child  "  << " succedeed!" << std::endl;
                }
                else if (mChildNodes[0]->mType == Action && mChildNodes[0]->readState() != Idle)
                {
                    // if it's a action node that has finished its job:
                    // ticking it without saving its returning state;
                    mChildNodes[0]->Semaphore.Signal();
                }

                // updating its vector cell
                mChildStates[0] = Idle;


            // Resetting the node state
            WriteState(Idle);
        }
    }
}

int DecoratorNegationNode::GetType()
{
    // Lock acquistion

    return DECORATOR;
}


void DecoratorNegationNode::AddChild(TreeNode* Child)
{
    // Checking if the Child is not already present

        if (mChildNodes.size() > 0)
        {
            throw BehaviorTreeException("Decorators can have only one child");
        }


    mChildNodes.push_back(Child);
    mChildStates.push_back(Idle);
}
