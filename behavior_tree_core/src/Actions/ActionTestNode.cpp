#include <Actions/ActionTestNode.h>

using namespace BT;
//---------------------------------------------------------------------------------------------------------------------
ActionTestNode::ActionTestNode(std::string Name) :
        ActionNode::ActionNode(Name) {
    mType = Action;
    // Thread start
    Thread = boost::thread( &ActionTestNode::exec, this);
}
//---------------------------------------------------------------------------------------------------------------------
ActionTestNode::~ActionTestNode() {
}
//---------------------------------------------------------------------------------------------------------------------
void ActionTestNode::exec() {

    while (true) {
        Semaphore.Wait();

        if (readState() == Exit) {
            SetColorState(Exit);
            return;
        }

        // Running state
        setNodeState(Running);

        // Perform action until parent sets a non-running state?
        int i = 0;
        while (readState() == Running and i++ < 5) {
            writeState(readState());
            boost::this_thread::sleep(boost::posix_time::milliseconds(800));
        }

        if (readState() == Exit) {
            return;
        }
        else {
            // trying to set the outcome state:
            if (writeState(status_) != true) { // meanwhile, my parent halted me!
                writeState(Idle);
                continue;
            }
        }

        // Synchronization
        // (my parent is telling me that it has read my new state)
        Semaphore.Wait();

        if (readState() == Exit) {
            return;
        }

        std::cerr << mName;
        writeState(Idle);
    }
}
//---------------------------------------------------------------------------------------------------------------------
bool ActionTestNode::halt() {
    boost::lock_guard<boost::mutex> LockGuard(mStateMutex);

    // Checking for "Running" correctness
    if (mState != Running) {
        return false;
    }
    SetColorState(Idle);

    mState = Halted;
    return true;
}
//---------------------------------------------------------------------------------------------------------------------
void ActionTestNode::setBehavior(NodeState status) {
    status_ = status;
}
