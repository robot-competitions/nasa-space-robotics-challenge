#include <Actions/ROSAction.h>

using namespace BT;

enum Status {
    RUNNING, SUCCESS, FAILURE
};

ROSAction::ROSAction(std::string Name) :
        ActionNode::ActionNode(Name) {
    mType = Action;
    // Thread start
    Thread = boost::thread( &ROSAction::exec, this);

}

ROSAction::~ROSAction() {
}

void ROSAction::exec() {

    //bool hasReturned = false;
    // create the action client
    // true causes the client to spin its own thread
    ROS_INFO("Waiting For the Actuator %s to start", mName.c_str());

    actionlib::SimpleActionClient<behavior_tree_core::BTAction> ac(mName, true);

    ac.waitForServer(); //will wait for infinite time until the server starts
    ROS_INFO("The Actuator %s has started", mName.c_str());

    behavior_tree_core::BTGoal goal;
    node_result.status = RUNNING; //

    while (true) {
        // Waiting for a tick to come
        ROS_INFO("The Actuator %s waiting for tick", mName.c_str());

        Semaphore.Wait();
        ROS_INFO("The Actuator %s ticked", mName.c_str());

        if (readState() == Exit) {
            // The behavior tree is going to be destroyed
            return;
        }

        // Running state
        setNodeState(Running);
        std::cerr << mName << " returning " << Running << "!" << std::endl;
        node_result.status = RUNNING;
        // Perform action...
        ROS_INFO("I am running the request to %s", mName.c_str());
        ac.sendGoal(goal);
        do {
            node_result = *(ac.getResult()); //checking the result
        } while (node_result.status == RUNNING && readState() == Running);
        ROS_INFO("The Server Has Replied Or I the node is halted");

        std::cerr << mName << " RETURNING " << node_result.status << "!" << std::endl;

        if (readState() == Exit) {
            // The behavior tree is going to be destroyed
            return;
        }

        if (node_result.status == SUCCESS) {
            // trying to set the outcome state:
            if (writeState(Success) != true) {
                // meanwhile, my father halted me!
                std::cerr << mName << " Halted!" << std::endl;
                ROS_INFO("I am cancelling the request");
                ac.cancelGoal();
                // Resetting the state
                writeState(Idle);
                continue;
            }

            std::cerr << mName << " returning Success " << Success << "!" << std::endl;
        }
        else if (node_result.status == FAILURE) {
            // trying to set the outcome state:
            if (writeState(Failure) != true) {
                // meanwhile, my father halted me!
                std::cerr << mName << " Halted!" << std::endl;
                ROS_INFO("I am cancelling the request");
                ac.cancelGoal();
                // Resetting the state
                writeState(Idle);
                continue;
            }

            std::cerr << mName << " returning Failure" << Failure << "!" << std::endl;
        }
        else { //it means that the parent has halted the node

            std::cerr << mName << " Halted!" << std::endl;
            ROS_INFO("I am cancelling the request");
            ac.cancelGoal();
            // Resetting the state
            writeState(Idle);
            continue;

            std::cerr << mName << " returning NOTHING (HALTED)" << Failure << "!" << std::endl;
        }

        std::cerr << mName << " returning " << Success << "!" << std::endl;

        // Synchronization
        // (my father is telling me that it has read my new state)
        Semaphore.Wait();

        if (readState() == Exit) {
            // The behavior tree is going to be destroied
            return;
        }

        // Resetting the state
        writeState(Idle);
    }
}

bool ROSAction::halt() {

    ROS_INFO("I am Halting the client");
    // Lock acquistion
    boost::lock_guard<boost::mutex> LockGuard(mStateMutex);

    // Checking for "Running" correctness
    if (mState != Running) {
        return false;
    }

    mState = Halted;
    return true;
}
