#include <ControlNode.h>

using namespace BT;

ControlNode::ControlNode(std::string Name) :
        TreeNode::TreeNode(Name) {
    mType = Control;
}

ControlNode::~ControlNode() {
}

void ControlNode::AddChild(TreeNode* Child) {
    // Checking if the Child is not already present
    for (unsigned int i = 0; i < mChildNodes.size(); i++) {
        if (mChildNodes[i] == Child) {
            throw BehaviorTreeException("'" + Child->mName + "' is already a '" + mName + "' child.");
        }
    }

    mChildNodes.push_back(Child);
    mChildStates.push_back(Idle);
}

unsigned int ControlNode::getChildrenNumber() {
    return mChildNodes.size();
}

bool ControlNode::Halt() {
    // Lock acquistion
    boost::lock_guard<boost::mutex> LockGuard(mStateMutex);

    mState = Halted;
    return true;
}

bool ControlNode::WriteState(NodeState StateToBeSet) {
    // Lock acquistion
    boost::lock_guard<boost::mutex> LockGuard(mStateMutex);

    mState = StateToBeSet;
    return true;
}

std::vector<TreeNode*> ControlNode::getChildren() {
    return mChildNodes;
}

void ControlNode::ResetColorState() {

    SetColorState(Idle);
    for (unsigned int i = 0; i < mChildNodes.size(); i++) {
        mChildNodes[i]->ResetColorState();
    }
}

void ControlNode::haltChildren(int i) {
    for (unsigned int j = i; j < mChildNodes.size(); j++) {
        if (mChildNodes[j]->mType != Action && mChildStates[j] == Running) {
            // if the control node was running:
            // halting it;
            mChildNodes[j]->halt();

            // sync with it (it's waiting on the semaphore);
            mChildNodes[j]->Semaphore.Signal();

            std::cout << mName << " halting child number " << j << "!" << std::endl;
        }
        else if (mChildNodes[j]->mType == Action && mChildNodes[j]->readState() == Running) {
            std::cout << mName << " trying halting child number " << j << "..." << std::endl;

            // if it's a action node that hasn't finished its job:
            // trying to halt it:
            if (mChildNodes[j]->halt() == false) {
                // this means that, before this node could set its child state
                // to "Halted", the child had already written the action outcome;
                // sync with him ignoring its state;
                mChildNodes[j]->Semaphore.Signal();

                std::cout << mName << " halting of child number " << j << " failed!" << std::endl;
            }

            std::cout << mName << " halting of child number " << j << " succedeed!" << std::endl;
        }
        else if (mChildNodes[j]->mType == Action && mChildNodes[j]->readState() != Idle) {
            // if it's a action node that has finished its job:
            // ticking it without saving its returning state;
            mChildNodes[j]->Semaphore.Signal();
        }

        // updating its vector cell
        mChildStates[j] = Idle;
    }

}

int ControlNode::getDepth() {
    int depMax = 0;
    int dep = 0;
    for (size_t i = 0; i < mChildNodes.size(); i++) {
        dep = (mChildNodes[i]->getDepth());
        if (dep > depMax) {
            depMax = dep;
        }

    }
    return 1 + depMax;
}

