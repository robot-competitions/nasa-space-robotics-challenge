#include <mystic.h>
#include <thread>
#include <cstdlib>
#include <unistd.h>

#include <ros/ros.h>
using namespace ros;

#include "BlobFinder.h"
#include "ImageFetch.h"

using namespace ros;
constexpr char version[] { "1.2" };
//---------------------------------------------------------------------------------------------------------------------
void pelvis() {
    RaisePelvis rp;
    rp.subscribeWait();
    rp(0.85);
    cerr << rp.publish() << tab;
    cerr << "pelvis done" << nl;
}
//---------------------------------------------------------------------------------------------------------------------
void move_closer() {
    constexpr double y_pos = 0.18;
    constexpr double step = 1.0;

    Walk walk;
    walk.newPath();
    walk.addStep(mystic::eLeft, step / 2, y_pos, false);
    walk.addStep(mystic::eRight, step, -y_pos, false);
    walk.addStep(mystic::eLeft, step, y_pos, false);

//    walk.addStep(mystic::eRight, step/2, y_pos, false);
    walk.doPath();
    walk.waitForFinished();
    cerr << "walk done" << nl;
}
//---------------------------------------------------------------------------------------------------------------------
void image() {
    BlobFinder bf;
    ImageFetch ic("pcl2_msg", bf);
    ic();
}
//---------------------------------------------------------------------------------------------------------------------
void exec_prog(const char* argv[]) {
    pid_t my_pid;

    if (0 == (my_pid = fork())) {
        if ( -1 == execvp(argv[0], (char **)argv)) {
            perror(argv[2]);
        }
    }
}
//---------------------------------------------------------------------------------------------------------------------
//const char* spin_args[] = { "bash", "-c",
//        "source /opt/nasa/indigo/setup.bash && rostopic pub -1 /multisense/set_spindle_speed std_msgs/Float64 '{data: 6.0}'", 0 };

const char* start_args[] = { "bash", "-c", "source /opt/nasa/indigo/setup.bash && rostopic pub -1 /srcsim/qual1/start std_msgs/Empty", 0 };
//---------------------------------------------------------------------------------------------------------------------
int main(int argc, char** argv) {

    init(argc, argv, "qual1_task");
    ROS_INFO_STREAM(ros::this_node::getName() << " qual1 started " << version);
    NodeHandle mNodeHandle;

    cerr << std::fixed << std::setprecision(2);
    ros::Time::sleepUntil(ros::Time(26));

    AsyncSpinner spinner(0);
    spinner.start();
    ROS_INFO_STREAM(ros::this_node::getName() << " qual1 spin started ");

    thread img(image);
    move_closer();
    pelvis();


    ::sleep(4);

    ROS_INFO_STREAM(ros::this_node::getName() << " start lights");
//    exec_prog(spin_args);
    exec_prog(start_args);
    cerr << "start sent" << nl;


    ROS_INFO_STREAM(ros::this_node::getName() << " wait for shutdown ");
    waitForShutdown();

    return 0;
}
