/*
 * ConsolePublish.cpp
 *
 *  Created on: Dec 22, 2016
 *      Author: rmerriam
 */
//---------------------------------------------------------------------------------------------------------------------
#include "mystic.h"

#include "opencv2/core/core.hpp"
using namespace cv;

#include "ConsolePublish.h"
//---------------------------------------------------------------------------------------------------------------------
ConsolePublish::ConsolePublish(const char* msg) :
        PublishTemplate { msg } {
}

void ConsolePublish::operator ()(const geometry_msgs::Vector3& loc, const Vec3s& color) {
    using namespace geometry_msgs;
    mTopic.x = loc.x;
    mTopic.y = loc.y;
    mTopic.z = loc.z;

    mTopic.r = color[0];
    mTopic.g = color[1];
    mTopic.b = color[2];
    sendMessage();
    ++mCount;
}
//---------------------------------------------------------------------------------------------------------------------
void ConsolePublish::sendMessage() {
    mPublisher.publish(mTopic);
}
