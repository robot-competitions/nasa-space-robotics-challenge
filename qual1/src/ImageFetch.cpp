///*
// * ImageFetch.cpp 
// *
// *  Created on: Sep 14, 2016
// *      Author: rmerriam
// */
#include <cv_bridge/cv_bridge.h>

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "mystic.h"
#include <tf2_ros/transform_listener.h>
#include <tf2/impl/utils.h>
#include <tf2/LinearMath/Vector3.h>
using namespace cv;

#include "BlobFinder.h"
#include "ImageFetch.h"
//---------------------------------------------------------------------------------------------------------------------
ImageFetch::ImageFetch(const char* msg, BlobFinder& bf) :
        mBlobFinder(bf), mState(wait_for_white) {

    mSubscriber = mNodeHandle.subscribe(msg, 1, &ImageFetch::getImage, this);

    ROS_INFO_STREAM(ros::this_node::getName() << " image subscribe ready");

}
//---------------------------------------------------------------------------------------------------------------------
void ImageFetch::getImage(const PCloudPtr& cloud) {
    cv_bridge::CvImagePtr cv_ptr;

    try {
        mNewImage = cloud;
        mImageSeq = mNewImage->header.seq;
    }
    catch (cv_bridge::Exception& e) {
        ROS_ERROR("cv_bridge exception: %s", e.what());
        return;
    }
} //---------------------------------------------------------------------------------------------------------------------
float ImageFetch::calcMidPcl2(const float left_value, const float right_value, const float coord_factor) const {
    float mid_value = (right_value - left_value) * coord_factor + left_value;
//    // cerr << "calc " << left_value << tab << right_value << tab << coord_factor << tab << mid_value << nl;
    return mid_value;
}
//---------------------------------------------------------------------------------------------------------------------
void ImageFetch::convertToImage() {

    using namespace pcl;
    mCloud = *mNewImage;

    // convert cloud to a ROS image in a message, then convert to OpenCV image
    sensor_msgs::Image pc_msg;
    toROSMsg(mCloud, pc_msg);

    mCurImage = cv_bridge::toCvCopy(pc_msg, sensor_msgs::image_encodings::RGB8)->image;
//    updateWindow(mCurImage);
}
//---------------------------------------------------------------------------------------------------------------------
bool ImageFetch::doBlobs(const bool first) {

    Mat diff;
    absdiff(mCurImage, mPrevImage, diff);

    KeyPoints keypoints = mBlobFinder.detect(diff);

    bool res { false };
    if (keypoints.size() != 0) {

        geometry_msgs::Vector3 pos;
        Vec3s color;

        res = findBlobs(keypoints, diff, pos, color);
//        // cerr << nl;

        if (res && first) {
            emitConsoleMsg(pos, color);
        }
    }
//    updateWindow(diff);
    return res;
}
//---------------------------------------------------------------------------------------------------------------------
void ImageFetch::emitConsoleMsg(const geometry_msgs::Vector3& pos, Vec3s color) {
    geometry_msgs::Vector3 t_pos { transform(pos) };

    // cerr << ">>> ";
    // display(pos.x, pos.y, pos.z);
    // cerr << tab;
    // display(t_pos.x, t_pos.y, t_pos.z);
    // cerr << tab;
    // cerr << color;
    // cerr << nl;

    mConsole(t_pos, color);
}
//---------------------------------------------------------------------------------------------------------------------
bool ImageFetch::findBlobs(const KeyPoints keypoints, Mat& matImage, geometry_msgs::Vector3& pos, Vec3s& color) {

    bool res { false };

    if (keypoints.size() != 0) {
        for (auto k : keypoints) {
            Point ipt = k.pt;
            Vec3b intensity = mCurImage.at<Vec3b>(ipt);

            // Draw circles around key points
            if (matImage.rows > 60 && matImage.cols > 60) {

                int cnt {};
                int has_clr {};
                for (auto i = 0; i < 3; i++) {
                    if (intensity[i] > 240) {
                        cnt++;
                        has_clr = i;
                    }
                }

                // check if point is in range
//                if (ipt.x < 330 || ipt.x > 690 || ipt.y < 52 || ipt.y > 370) {
//                    continue;
//                }

                // display(k.size * 3.1415);
                // cerr << tab;
                // display(ipt.x, ipt.y);
//                // cerr << tab;
                // cerr << nl;

                if (cnt == 1) {

                    color[has_clr] = 1;
                    circle(matImage, ipt, k.size, CV_RGB(255, 255, 255));

                    pcl::PointXYZRGB pt = mCloud.at(ipt.x, ipt.y);

                    if (isnan(pt.x)) {
                        pt = findHorizBorders(ipt);
                    }

                    pos.x = pt.x;
                    pos.y = pt.y;
                    pos.z = pt.z;

                    res = true;
                }
            }
        }
    }
    return res;
}
//---------------------------------------------------------------------------------------------------------------------
pcl::PointXYZRGB ImageFetch::findHorizBorders(const Point& img_pt) {

    // input is a position in the center of a color blob but the PCL position data is NAN

    // cerr << nl;

    // get the point values at the point of interest, i.e. center of blob
    pcl::PointXYZRGB& mid_values = mCloud.at(img_pt.x, img_pt.y);

    // cerr << "h mid" << setw(4) << img_pt << ' ' << mid_values << '\n';

    // walk point to the left until hit a value that is a color value, i.e. GT 110
    pcl::PointXYZRGB left_values;
    Point left_coords;
    for (int i = 0; i < 80; i++) {
        pcl::PointXYZRGB& pt = mCloud.at(img_pt.x - i, img_pt.y);

        // cerr << '[' << (int)pt.r << ' ' << (int)pt.g << ' ' << (int)pt.b << ']';

        if ( !isnan(pt.x) && (pt.r + pt.g + pt.b) < 240) {  // hit border when sum of colors less than 240
            left_values = pt;
            left_coords = Point(img_pt.x - i + 1, img_pt.y);        // back up to last one with color

            // cerr << nl << "h left" << setw(4) << left_coords << ' ' << pt;

            break;
        }
    }
    // cerr << nl;

    // walk point to the right until hit a value that is a color value, i.e. GT 110
    pcl::PointXYZRGB right_values;
    Point right_coords;
    for (int i = 0; i < 80; i++) {
        pcl::PointXYZRGB& pt = mCloud.at(img_pt.x + i, img_pt.y);

        // cerr << '[' << (int)pt.r << ' ' << (int)pt.g << ' ' << (int)pt.b << ']';

        if ( !isnan(pt.x) && (pt.r + pt.g + pt.b) < 240) {  // hit border when sum of colors less than 240
            right_values = pt;
            right_coords = Point(img_pt.x + i - 1, img_pt.y);    // back up to last one with color

            // cerr << nl << "h right" << setw(4) << right_coords << ' ' << pt;

            break;
        }
    }
    // cerr << nl;

    // got right and left positions with colors
    // calculate the depth values for the midpoint
    float coord_factor = float((right_coords.x - img_pt.x)) / (right_coords.x - left_coords.x);

    mid_values = left_values;
    mid_values.x = calcMidPcl2(left_values.x, right_values.x, coord_factor);
    mid_values.y = calcMidPcl2(left_values.y, right_values.y, coord_factor);
    mid_values.z = calcMidPcl2(left_values.z, right_values.z, coord_factor);

    // cerr << "h mid" << setw(4) << img_pt << ' ' << mid_values << '\n';
    // cerr << nl;
    return mid_values;
}
//---------------------------------------------------------------------------------------------------------------------
bool ImageFetch::isWhite() {

    if ( !mCurImage.empty()) {
        Vec3b intensity = mCurImage.at<Vec3b>(275, 500);
        if (intensity[0] > 240 && intensity[1] > 240 && intensity[2] > 240) {
            return true;
        }
    }
    return false;
}
//---------------------------------------------------------------------------------------------------------------------
void ImageFetch::operator ()() {
    int seq {};

    namedWindow(OPENCV_WINDOW);

    while (ros::ok()) {
#if 1
//        cerr << Time::now().sec << nl;
        if (Time::now().sec > 70) {
            // cerr << "timeout abort" << nl;
//            requestShutdown();
            shutdown();
            throw 1;
        }
#endif
        if (seq != mImageSeq) {
            seq = mImageSeq;

            // convert to template PCL
            convertToImage();

            if (mPrevImage.empty()) {
                mPrevImage = mCurImage;
            }
            processImage();
        }
        else {
            std::this_thread::yield();
        }
        // cerr << flush;
    }
}

//---------------------------------------------------------------------------------------------------------------------
void ImageFetch::processImage() {
    static bool first { false };
//    updateWindow(mCurImage);

    switch (mState) {
        case wait_for_white:
            if (isWhite()) {
                ROS_INFO_STREAM(ros::this_node::getName() << " starting run ");
                mState = wait_for_light;
            }
            else {
                mPrevImage.release(); // will cause new capture
            }
            break;

        case wait_for_light:
            static bool do_rpt { true };
            if (do_rpt) {
                do_rpt = false;
            }
            if ( !isWhite()) {
                mState = finish;
            }
            else if (doBlobs(first)) {
                mState = track_light;
                do_rpt = true;
                first = true;
            }
            else {
                mPrevImage = mCurImage;
            }
            break;

        case track_light:
            if ( !doBlobs(first)) {
                mState = wait_for_light;
                first = false;
            }
            break;

        case finish:
            ROS_INFO_STREAM(ros::this_node::getName() << " run complete ");
            cout << "light count " << mConsole.mCount << nl;
//            requestShutdown();
            shutdown();
            break;
    }

}
//---------------------------------------------------------------------------------------------------------------------
geometry_msgs::Vector3 ImageFetch::transform(const geometry_msgs::Vector3 l_loc) {

    using namespace geometry_msgs;
    TransformStamped ts_in;
    TransformStamped ts_out;

    ts_in.header.frame_id = "left_camera_optical_frame";
    ts_in.child_frame_id = "left_camera_optical_frame";
    ts_in.header.stamp = Time(0);
    ts_in.transform.translation = l_loc;

    const string upperNeck { "upperNeckPitchLink" };
    const string head { "head" };

    while (true) {
        try {
            tfBuffer.transform(ts_in, ts_out, head, Duration(0.5));
            break;
        }
        catch (exception& e) {
            // cerr << e.what() << '\n';
        }
    }
    return ts_out.transform.translation;
}
//---------------------------------------------------------------------------------------------------------------------
// Update GUI Window
void ImageFetch::updateWindow(const Mat& image) {

    if ( !image.empty()) {
        cv::imshow(OPENCV_WINDOW, image);
    }
    waitKey(1);
}
