/*
 * MoveArm.h
 *
 *  Created on: Dec 6, 2016
 *      Author: rmerriam
 */

#ifndef SRC_CONSOLEPUBLISH_H_
#define SRC_CONSOLEPUBLISH_H_
//---------------------------------------------------------------------------------------------------------------------
#include <srcsim/Console.h>

#include "PublishTemplate.h"
//---------------------------------------------------------------------------------------------------------------------
class ConsolePublish : public PublishTemplate<srcsim::Console> {
public:
    ConsolePublish(const char* msg);
    virtual ~ConsolePublish() override {
    }

    void operator ()(const geometry_msgs::Vector3& loc, const Vec3s& color);

    int mCount {};

private:
    virtual void sendMessage() override;

    srcsim::Console mMsg;



};
#endif /* SRC_CONSOLEPUBLISH_H_ */
