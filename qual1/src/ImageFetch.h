/*
 * ImageFetch.h
 *
 *  Created on: Sep 14, 2016
 *      Author: rmerriam
 */

#ifndef SRC_IMAGEFETCH_H_
#define SRC_IMAGEFETCH_H_

//#include "ConsolePublish.h"

#include <pcl_ros/point_cloud.h>
using namespace pcl;

//---------------------------------------------------------------------------------------------------------------------
typedef pcl::PointCloud<pcl::PointXYZRGB> PCloud;
typedef boost::shared_ptr<const PCloud> PCloudConstPtr;
typedef boost::shared_ptr<PCloud> PCloudPtr;
//---------------------------------------------------------------------------------------------------------------------
using namespace cv;
//---------------------------------------------------------------------------------------------------------------------
class ImageFetch {
public:
    ImageFetch(const char* msg, BlobFinder& bf);

    void operator ()();

    bool isSubscribed() {
        return mSubscriber.getNumPublishers() != 0;
    }

//    geometry_msgs::Vector3 transform(const geometry_msgs::Vector3);

private:

    tf2_ros::Buffer tfBuffer;
    tf2_ros::TransformListener tfListener { tfBuffer };

    void getImage(const PCloudPtr& cloud);

    float calcMidPcl2(const float left_value, const float right_value, const float coord_factor) const;
    void convertToImage();
    bool doBlobs(const bool first);
    bool findBlobs(const KeyPoints kp, Mat& image, geometry_msgs::Vector3& pos, Vec3s& color);
    pcl::PointXYZRGB findHorizBorders(const Point& img_pt);
    bool isWhite();
    void processImage();
    void updateWindow(const Mat& image);
    void emitConsoleMsg(const geometry_msgs::Vector3& pos, cv::Vec3s color);

    enum ProcState {
        wait_for_white, wait_for_light, track_light, finish
    };

    const std::string OPENCV_WINDOW = "Light Tracking";

    NodeHandle mNodeHandle;
    Subscriber mSubscriber;

    PCloudConstPtr mNewImage;
    PCloud mCloud;

    Mat mCurImage;
    Mat mPrevImage;

    int mImageSeq {};

    BlobFinder& mBlobFinder;
    ProcState mState;
    ConsolePublish mConsole { "/srcsim/qual1/light" };

};

#endif /* SRC_IMAGEFETCH_H_ */
