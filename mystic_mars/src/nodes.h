/*
 * nodes.h
 *
 *  Created on: Mar 17, 2017
 *      Author: rmerriam
 */

#ifndef NODES_H_
#define NODES_H_

#include <BehaviorTree.h>
using namespace BT;

#include "MarsBlackboard.h"

// action nodes
#include "nodes/Announce.h"
#include "nodes/ArmMove.h"
#include "nodes/Move.h"
#include "nodes/BlackboardStart.h"
#include "nodes/CalcSatConsolePose.h"
#include "nodes/CalcSolarPanelPose.h"
#include "nodes/GetObjectByColor.h"
#include "nodes/HandMove.h"
#include "nodes/ScanPathway.h"
#include "nodes/SetChestPose.h"
#include "nodes/SetHome.h"
#include "nodes/SetNeckPose.h"
#include "nodes/SetPelvisHeight.h"
#include "nodes/SetFootStance.h"
#include "nodes/SetTaskNode.h"
#include "nodes/Turn.h"
#include "nodes/Visualize.h"
#include "nodes/WaitForHarness.h"
#include "nodes/WalkingStop.h"

// condition nodes
#include "nodes/IsMovementDone.h"
#include "nodes/IsCheckpointDone.h"
#include "nodes/IsHarnessActive.h"
#include "nodes/IsPathClear.h"

// decorator nodes

// utility nodes - mainly debug
#include "nodes/DelayNode.h"

#endif /* NODES_H_ */
