/*
 * main.cpp
 *
 *  Created on: Mar 16, 2017
 *      Author: rmerriam
 */
#include <exception>

#include "tree/TreeRoot.h"
#include <RobotPose.h>

constexpr float version { 0616.2030 };
//--------------------------------------------------------------------------------------------------------------------------
void set_log_level() {

    ROS_INFO_STREAM(ros::this_node::getName() << " initialized v." << version);

#if 1
    if (ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Info)) {
#else
        if (console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Debug)) {
#endif
        ros::console::notifyLoggerLevelsChanged();
    }
}
//--------------------------------------------------------------------------------------------------------------------------
int main(int argc, char **argv) {
    init(argc, argv, "MysticMars");
    NodeHandle nh { };

//    ::sleep(10);

    set_log_level();

    while (Time::now().sec > 36000) {
        Duration(0.5).sleep();
    }

    cerr << "mystic mars " << version << nl << setprecision(4) << fixed;
    cout << "mystic mars " << version << nl << setprecision(4) << fixed;

    Time::sleepUntil(ros::Time(27));
    ROS_INFO_STREAM(ros::this_node::getName() << " sim time > 27 " << ros::Time().now());

    AsyncSpinner mSpinner { 0 };
    mSpinner.start();

    MarsBlackboard blackboard;
    TreeRoot tr;

    static TreeNode& root(tr.node);
    ScanPathway scan_path { "scan_path", ScanPathway::eProcess, true };
    Visualize visualize { "visualize" };
    IsHarnessActive harness_active { "harness_active" };
    BlackboardStart blackboard_start { "blackboard start" };
    Announce announce { "main loop", "@@@@@@ main loop " };
    Announce harness_reset { "harness reset", "!!!!!!!!!! HARNESS RESET !!!!!!!!!!" };
    SetTaskNode start_task_1_1 { "start task ", 1, 1 };

    try {
        blackboard_start.eval(blackboard);
        announce.eval(blackboard);
        start_task_1_1.eval(blackboard);

        while (ros::ok()) {

            bool do_reset { false };
            while (harness_active.eval(blackboard) == TreeNode::Success) {
                Duration(.5).sleep();
//                visualize.eval(blackboard);
                do_reset = true;
            }

            if (do_reset) {
                harness_reset.eval(blackboard);
                root.reset();
                harness_reset.eval(blackboard);
            }

            if (root.eval(blackboard) == TreeNode::Success) {
                break;
            }
//            visualize.eval(blackboard);
            Duration(.05).sleep();
        }
    }
    catch (exception& e) {
        cerr << nl << e.what() << nl;
        char ch;
        cin >> ch;
        ROS_INFO_STREAM(ros::this_node::getName() << " exception " << e.what());
    }

    ROS_INFO_STREAM(ros::this_node::getName() << " exit");

    mSpinner.stop();
    return 0;
}
