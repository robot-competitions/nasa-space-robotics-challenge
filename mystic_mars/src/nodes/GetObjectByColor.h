/*
 * getObjectByColor.h
 *
 *  Created on: May 1, 2017
 *      Author: rmerriam
 */

#ifndef GETOBJECTBYCOLOR_H_
#define GETOBJECTBYCOLOR_H_

#include <mystic.h>
using namespace mys;

#include <ActionNode.h>

#include <ImageVision.h>
#include <ObjectFinder.h>
#include "../MarsBlackboard.h"
using namespace BT;
//---------------------------------------------------------------------------------------------------------------------
class GetObjectByColor : public ActionNode, public ObjectFinder {
public:

    GetObjectByColor(const string& name, const ObjectId id, const Vec3b& lower, const Vec3b& upper) :
        ActionNode(name), ObjectFinder(id, lower, upper) {
    }
    virtual ~GetObjectByColor() = default;

    const virtual BT::TreeNode::NodeState eval(Blackboard& bb);

private:
    Vector3 mObjectLocation;
    Vector3 mVisionLocation;

    void calcPosition(Mat res, const PCloud& pc, const Contours& contours);
};
#endif /* GETOBJECTBYCOLOR_H_ */
