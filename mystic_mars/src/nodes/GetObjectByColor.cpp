/*
 * getObjectByColor.cpp
 *
 *  Created on: May 1, 2017
 *      Author: rmerriam
 */

#include "GetObjectByColor.h"
//---------------------------------------------------------------------------------------------------------------------
const BT::TreeNode::NodeState GetObjectByColor::eval(Blackboard& bb) {
    MarsBlackboard& mbb = MarsBlackboard::getMarsBB(bb);
    PointCloud2Sub& pc2 = mbb.point_cloud();

    if (pc2.isValid()) {
        const PCloud& pc { pc2.pointCloud() };
        ImageVision iv(pc2.pointCloud());
        Mat res { iv.image() };

        const Contours& contours { process(iv.image()) };
        if ( !contours.empty()) {
            calcPosition(res, pc, contours);

            mbb.mObjectDistances[id()] = mObjectLocation;
            mbb.mVisonLocations[id()] = mVisionLocation;
            setState(Success);
        }
        else {
            setState(Failure);
        }
    }
    else {
        setState(Running);
    }
    mys::display(indent(cerr), getNodeText()) << mys::nl;
    return getState();
}
//---------------------------------------------------------------------------------------------------------------------
void GetObjectByColor::calcPosition(Mat res, const PCloud& pc, const Contours& contours) {
    Contours big_cs;

    for (uint16_t i = 0; i < contours.size(); i++) {
        big_cs.push_back(contours[i]);
        Point avg = accumulate(contours[i].begin(), contours[i].end(), Point(0, 0));
        avg.x /= contours[i].size();
        avg.y /= contours[i].size();
        pcl::PointXYZRGB pt = pc.at(avg.x, avg.y);

        cerr << "get obj ";
        display(cerr, (int)(pt.b), (int)(pt.g), (int)(pt.r));
        display(cerr, pt.z, pt.x, pt.y);    // switched to robot orientation

        mVisionLocation = Vector3 { pt.z, pt.x, pt.y };

        const tf2::Transform& world_pt = getWorldLocation(pt.x, pt.y, pt.z);
        mObjectLocation = world_pt.getOrigin();

        display(cerr, world_pt.getOrigin()) << nl;

        RotatedRect rrect = minAreaRect(contours[i]);
        Point2f rect_points[4];
        rrect.points(rect_points);
        for (int j = 0; j < 4; j++)
            line(res, rect_points[j], rect_points[(j + 1) % 4], Scalar(0, 255, 0), 1, 8);

    }
    cerr << nl;
    drawContours(res, big_cs, -1, Scalar(0, 255, 0));

    ImageVision result(res);
    updateWindow(result.flipped(), mName);
}
