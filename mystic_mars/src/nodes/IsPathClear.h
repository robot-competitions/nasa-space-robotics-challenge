/*
 * IsPathClear.h
 *
 *  Created on: Apr 28, 2017
 *      Author: rmerriam
 */

#ifndef IsPathClear_H
#define IsPathClear_H

#include <ConditionNode.h>
#include "../MarsBlackboard.h"

using namespace BT;
using namespace mys;
//---------------------------------------------------------------------------------------------------------------------
class IsPathClear : public ConditionNode {
public:
    IsPathClear(const string& name) :
        ConditionNode { name } {
    }
    ~IsPathClear() = default;

    const virtual TreeNode::NodeState eval(Blackboard& bb) override {
        Depth depth;
        mys::display(indent(cerr), getNodeId(), getName()) << " start " << mys::nl;

        MarsBlackboard& mbb = MarsBlackboard::getMarsBB(bb);
//        mbb.mScanAngle;
        double distance = mbb.mScanDistance;

        if (std::isnan(distance)) {
            setState(Failure);
        }
        else {
            setState(Success);
            mbb.mDistanceToObject = distance - 0.6;
        }

        mys::display(indent(cerr), getNodeText()) << mys::nl;
        return getState();
    }

private:

};
#endif
