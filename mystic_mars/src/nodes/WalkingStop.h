#ifndef WalkingStop_H
#define WalkingStop_H
#include <mystic.h>
#include "ConditionNode.h"
#include <WalkStop.h>
//---------------------------------------------------------------------------------------------------------------------

using namespace BT;
//---------------------------------------------------------------------------------------------------------------------
class WalkingStop : public ActionNode {
public:
    WalkingStop(const string& name, ConditionNode& child) :
        ActionNode(name), mChild(child) {
    }
    virtual ~WalkingStop() = default;

    const virtual TreeNode::NodeState eval(Blackboard& bb) override;

protected:
    ConditionNode& mChild;

};
//---------------------------------------------------------------------------------------------------------------------
const TreeNode::NodeState WalkingStop::eval(Blackboard& bb) {
    Depth depth;
    mys::display(indent(cerr), getNodeId(), getName()) << " start " << mys::nl;

    if (mChild.eval(bb) == Success) {
        MarsBlackboard& mbb = MarsBlackboard::getMarsBB(bb);
        WalkStop& stop = mbb.walk_stop();

        stop.send();
        setState(Success);
    }
    else {
        setState(Failure);
    }

    mys::display(indent(cerr), getNodeText()) << mys::nl;

    return getState();
}

#endif
