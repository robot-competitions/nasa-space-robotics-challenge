/*
 * ScanPathway.h
 *
 *  Created on: May 12, 2017
 *      Author: rmerriam
 */

#ifndef SCANPATHWAY_H_
#define SCANPATHWAY_H_
#include <mystic.h>
#include <ImageVision.h>

#include "RunningNodeAbstract.h"
#include "PathCheck.h"
//---------------------------------------------------------------------------------------------------------------------
class ScanPathway : public RunningNodeAbstract {
public:
    enum ScanActs {
        eReset, eProcess, eUpdate
    };
    ScanPathway(const string& name, const ScanActs act, const bool report_nan = false) :
        RunningNodeAbstract(name, 0.0), mAct { act }, mReportNan { report_nan } {

    }
    virtual ~ScanPathway() = default;

protected:
    virtual void nodeNotRunning(Blackboard& bb) override;

private:
    const ScanActs mAct;
    const bool mReportNan;
};
//---------------------------------------------------------------------------------------------------------------------
inline void ScanPathway::nodeNotRunning(Blackboard& bb) {
    MarsBlackboard& mbb = MarsBlackboard::getMarsBB(bb);

    switch (mAct) {
        case eReset:
            mbb.mScanDistance = 0.0;
            mbb.mScanAngle = 0.0;
            setState (Success);
            break;

        case eProcess: {
            PointCloud2Sub& pc2 = mbb.point_cloud();

            if (pc2.isValid()) {
                PathCheck check(pc2);

                if (check.process()) {
                    double x { check.worldLocation().getX() };
                    cerr << "Scan Path ";
                    display(cerr, x) << nl;

                    mbb.mObjectDistances[eScanDistance] = check.worldLocation();
                    mbb.mVisonLocations[eScanDistance] = check.visionLocation();

                    mbb.mScanDistance = x;
                    mbb.mScanAngle = mbb.mChestPose.load().mYaw;
                    setState(Success);
                }
                else {
                    setState(Success);
                    mbb.mScanDistance = std::nan("");
                }
            }
            else {
//                setState (Failure);
                setState(Success);
                mbb.mScanDistance = std::numeric_limits<double>::infinity();
            }
            break;
        }

        case eUpdate:
            mbb.mTurnAngle = mbb.mScanAngle.load();
            mbb.mDistanceToObject = mbb.mScanDistance.load();
            setState(Success);
            break;
    }
}

#endif /* SCANPATHWAY_H_ */
