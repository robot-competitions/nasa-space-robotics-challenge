/*
 * StartupStance.cpp
 *
 *  Created on: Mar 16, 2017
 *      Author: rmerriam
 */
#include <mystic.h>
#include "SetPelvisHeight.h"
//---------------------------------------------------------------------------------------------------------------------
SetPelvisHeight::SetPelvisHeight(const string& name, const double& height, const double& delay) :
    RunningNodeAbstract(name, delay), mHeight(height) {
}
//---------------------------------------------------------------------------------------------------------------------
void SetPelvisHeight::nodeNotRunning(Blackboard& bb) {
    MarsBlackboard& mbb = MarsBlackboard::getMarsBB(bb);

    PelvisHeightTraj& pelvis = mbb.raise_pelvis();

    pelvis.moveTime(mDelayTime);
    pelvis.delayTime(0);
    pelvis.set(mHeight);
    pelvis.send();

    setTimer();
    setState(Running);
}

