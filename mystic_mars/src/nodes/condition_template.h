#ifndef condition_template_H
#define condition_template_H

#include <ConditionNode.h>
using namespace BT;
//---------------------------------------------------------------------------------------------------------------------
class condition_template : public ConditionNode {
public:
    condition_template(const string& name) :
            ConditionNode(name) {
    }
    ~condition_template() = default;

    //---------------------------------------------------------------------------------------------------------------------
    const virtual TreeNode::NodeState eval(Blackboard& bb) override {
        ROS_DEBUG_STREAM(ros::this_node::getName() << getName());

        MarsBlackboard& mbb = MarsBlackboard::getMarsBB(bb);

        setState(Success);
        return getState();
    }

private:

};

#endif
