/*
 * StartupStance.h
 *
 *  Created on: Mar 16, 2017
 *      Author: rmerriam
 */

#ifndef Visualize_H_
#define Visualize_H_

#include <ActionNode.h>

#include "../MarsBlackboard.h"
using namespace BT;

//---------------------------------------------------------------------------------------------------------------------
class Visualize : public ActionNode {
public:
    Visualize(const string& name);
    virtual ~Visualize() = default;

    const virtual TreeNode::NodeState eval(Blackboard& bb) override;

private:

};
#endif /* Visualize_H_ */
