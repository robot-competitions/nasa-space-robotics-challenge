/*
 * Parameters.h
 *
 *  Created on: Jan 18, 2017
 *      Author: rmerriam
 */

#ifndef WalkParams_H_
#define WalkParams_H_
#include <Walk.h>

//---------------------------------------------------------------------------------------------------------------------
struct WalkParams {

    static void walkSetup(Walk& mWalk);

    static constexpr uint8_t num_steps { 1 };
    static constexpr double pelvis_height { 1.05 };

    static constexpr double step_forward { 1.0 };
    static constexpr double step_reverse { 0.5 };
    static constexpr double spread { 0.15 };    // meters
    static constexpr double step_height { 0.1 }; // meters
    static constexpr double swing_time { 0.5 };
    static constexpr double trans_time { 0.4 };
    static constexpr Walk::TrajectoryType traj_type { Walk::eDefault };
};
//---------------------------------------------------------------------------------------------------------------------
inline
void WalkParams::walkSetup(Walk& mWalk) {
    mWalk.transfer(trans_time);
    mWalk.swing(swing_time);
    mWalk.stepHeight(step_height);
    mWalk.trajectoryType(traj_type);
}

#endif /* PARAMS_H_ */
