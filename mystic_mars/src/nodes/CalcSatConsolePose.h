#ifndef CalcSatConsolePose_H
#define CalcSatConsolePose_H

#include <ActionNode.h>
//---------------------------------------------------------------------------------------------------------------------
class CalcSatConsolePose : public BT::ActionNode {
public:
    CalcSatConsolePose(const string& name, const double& adjust_by = mys::no_value);
    virtual ~CalcSatConsolePose() = default;

    const virtual BT::TreeNode::NodeState eval(BT::Blackboard& bb) override;

protected:
    void calcConsoleAngle(double dx, double dy, MarsBlackboard& mbb);

private:
    const double mAdjust;
};
//---------------------------------------------------------------------------------------------------------------------
inline CalcSatConsolePose::CalcSatConsolePose(const string& name, const double& adjust_by) :
    ActionNode::ActionNode { name }, mAdjust { adjust_by } {
}

#endif
