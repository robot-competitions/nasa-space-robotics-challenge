/*
 * WarmUp.h
 *
 *  Created on: Dec 8, 2016
 *      Author: rmerriam
 */

#ifndef ArmMove_H_
#define ArmMove_H_

#include <mystic.h>
using namespace mys;

#include <ArmTraj.h>
#include "../MarsBlackboard.h"
#include "RunningNodeAbstract.h"

using namespace mys;
using namespace BT;

using ArmPositions = std::array<double, 7>;
//---------------------------------------------------------------------------------------------------------------------
class ArmMove : public RunningNodeAbstract {
public:
    ArmMove(const string& name, const mys::Side side, const ArmPositions& positions, const double& delay = 0.5) :
        RunningNodeAbstract { name, delay }, mSide { side }, mPositions(positions) {
    }
    virtual ~ArmMove() = default;

protected:

//    virtual void nodeRunning(Blackboard& bb) override;
    virtual void nodeNotRunning(Blackboard& bb) override;

private:
    mys::Side mSide;
    const ArmPositions mPositions;
};
////---------------------------------------------------------------------------------------------------------------------
//inline void ArmMove::nodeRunning(Blackboard& bb) {
////    MarsBlackboard& mbb = MarsBlackboard::getMarsBB(bb);
//
//
//    RunningNodeAbstract::nodeRunning(bb);
//}

/*
 *         eShoulderRotate, eShoulderJoint, eUpperArmRotate, eElbowJointRoate, eLowerArmRotate, eWristX, eWristY
 *
 *
 * 'leftShoulderPitch'   'leftShoulderRoll'  'leftShoulderYaw'   'leftElbowPitch'    'leftForearmYaw'    'leftWristRoll'     'leftWristPitch'
 * -0.200  -1.200  0.700   -1.500  1.300   0.000   0.000
 *
 * 'rightShoulderPitch'    'rightShoulderRoll'     'rightShoulderYaw'  'rightElbowPitch'   'rightForearmYaw'   'rightWristRoll'    'rightWristPitch'
 * -0.200  1.200   0.700   1.500   1.300   0.000   0.000
 *
 */
//---------------------------------------------------------------------------------------------------------------------
inline void ArmMove::nodeNotRunning(Blackboard& bb) {
    MarsBlackboard& mbb = MarsBlackboard::getMarsBB(bb);
    ArmTraj& arm = (mSide == eLeft) ? mbb.larm_traj() : mbb.rarm_traj();

    // invert some left arm values!!!

    arm.moveTime(1.0);
    arm.delayTime(1.0);

    arm.startMove();

    for (int i = 0; i < ArmTraj::eWristY; ++i) {
        arm.setPose(ArmTraj::ArmPart(i), mPositions[i]);
    }

//    arm.setPose(ArmTraj::eShoulderRotate, 0.0);
//    arm.setPose(ArmTraj::eShoulderJoint, 0.0);
//    arm.setPose(ArmTraj::eUpperArmRotate, -00);
//    arm.setPose(ArmTraj::eElbowJointRotate, 0.00);
//    arm.setPose(ArmTraj::eLowerArmRotate, -1.0);
//    arm.setPose(ArmTraj::eWristX, 0.0);
//    arm.setPose(eWristY, 0.0);

    arm.doMove();

    setTimer();
    setState(Running);
}

#endif /* ArmMove_H_ */
