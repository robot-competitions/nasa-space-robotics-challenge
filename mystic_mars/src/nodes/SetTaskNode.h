#ifndef SkipTaskNode_H
#define SkipTaskNode_H

#include <srcsim/StartTask.h>
#include <srcsim/StartTaskRequest.h>
#include <srcsim/StartTaskResponse.h>
using namespace ros;
using namespace srcsim;

#include <ActionNode.h>

#include "start_task.h"
//---------------------------------------------------------------------------------------------------------------------
class SetTaskNode : public BT::ActionNode {
public:
    //---------------------------------------------------------------------------------------------------------------------
    SetTaskNode(const string& name, const uint8_t task, const uint8_t checkpoint) :
        ActionNode::ActionNode(name), mTask { task }, mCheckpoint { checkpoint } {
    }
    virtual ~SetTaskNode() = default;

    //---------------------------------------------------------------------------------------------------------------------
    const virtual BT::TreeNode::NodeState eval(BT::Blackboard& bb) override {
//        start_task(mTask, mCheckpoint);
        mys::display(indent(cerr), getNodeId(), getName()) << " start " << mys::nl;

        StartTaskRequest req;
        req.task_id = mTask;
        req.checkpoint_id = mCheckpoint;
        StartTaskResponse resp;

        ros::service::call("/srcsim/finals/start_task", req, resp);

        if (resp.success) {
            setState(Success);
        }
        else {
            setState(Failure);
        }
        setState(Success);

        mys::display(indent(cerr), getNodeText()) << mys::nl;
        return getState();
    }

private:
    const uint8_t mTask;
    const uint8_t mCheckpoint;

};
#endif
