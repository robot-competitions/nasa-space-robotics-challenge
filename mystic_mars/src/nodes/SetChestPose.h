/*
 * SetChestPose.h
 *
 *  Created on: Mar 16, 2017
 *      Author: rmerriam
 */

#ifndef SetChestPose_H_
#define SetChestPose_H_

#include <ChestTraj.h>
#include <RPY.h>
#include "../MarsBlackboard.h"
#include "RPYPoseAbstract.h"

using namespace BT;
/*---------------------------------------------------------------------------------------------------------------------
 *  Roll    Forward max 42
 *          Back    max -12
 *  Pitch   Right   max 17
 *  Yaw     Left    max 70
 *
 *---------------------------------------------------------------------------------------------------------------------*/

class SetChestPose : public RPYPoseAbstract<ChestTraj> {
public:

    SetChestPose(const string& name, const double& delay);      //  Constructor to execute move created by another node
    SetChestPose(const string& name, const RPY& rpy = rpy_no_value);    // constructor to move with no delay
    SetChestPose(const string& name, const double& delay, const RPY& rpy = rpy_no_value);   // constructor with delay
    virtual ~SetChestPose() = default;

    virtual void nodeRunning(Blackboard& bb) override;

private:
    virtual ChestTraj& getTraj(const MarsBlackboard& mbb) const override;
    virtual const RPY getBlackBoardPose(const MarsBlackboard& mbb) override;

};
//---------------------------------------------------------------------------------------------------------------------
inline SetChestPose::SetChestPose(const string& name, const double& delay, const RPY& rpy) :
    RPYPoseAbstract(name, delay, rpy) {
}
//---------------------------------------------------------------------------------------------------------------------
inline SetChestPose::SetChestPose(const string& name, const RPY& rpy) :
    SetChestPose(name, 0.0, rpy) {
}
//---------------------------------------------------------------------------------------------------------------------
inline SetChestPose::SetChestPose(const string& name, const double& delay) :
    SetChestPose(name, delay, rpy_no_value) {
}

//---------------------------------------------------------------------------------------------------------------------
inline void SetChestPose::nodeRunning(Blackboard& bb) {
    if (isTimerExpired()) {
        setState(Success);

        MarsBlackboard& mbb = MarsBlackboard::getMarsBB(bb);

        ChestTraj& cj = getTraj(mbb);
        RPY rpy(cj.updateRPY());

        mbb.mChestPose = rpy;
    }
}
//---------------------------------------------------------------------------------------------------------------------
inline ChestTraj& SetChestPose::getTraj(const MarsBlackboard& mbb) const {
    return mbb.chest_traj();
}
//---------------------------------------------------------------------------------------------------------------------
inline const RPY SetChestPose::getBlackBoardPose(const MarsBlackboard& mbb) {
    return mbb.mChestPose;
}

#endif /* SetChestPose_H_ */
