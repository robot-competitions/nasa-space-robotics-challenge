#ifndef BlackBoardStart_H
#define BlackBoardStart_H

#include <ActionNode.h>
//---------------------------------------------------------------------------------------------------------------------
namespace BT {
    class BlackboardStart : public ActionNode {
    public:
        BlackboardStart(const string& name);
        virtual ~BlackboardStart() = default;

        virtual const NodeState eval(Blackboard& bb) override;

    private:

    };
}

#endif
