#include "HandMove.h"

//---------------------------------------------------------------------------------------------------------------------
void HandMove::nodeRunning(Blackboard& bb) {
    RunningNodeAbstract::nodeRunning(bb);

    if (getState() == Success) {
        MarsBlackboard& mbb = MarsBlackboard::getMarsBB(bb);
        HandTraj& move = (mHand == eLeft) ? mbb.lhand_traj() : mbb.rhand_traj();

        move.update();

        display(cerr, move.getTranslation(), rad2deg(move.getRPY()))
        << " palm after move " << nl;

        move.update("torso");
        Vec3 palm_in_torso { move.getTranslation() };
        display(cerr, palm_in_torso, rad2deg(move.getRPY()));
        cerr << " after move torso " << nl;
    }
}
//---------------------------------------------------------------------------------------------------------------------
void HandMove::nodeNotRunning(Blackboard& bb) {
    MarsBlackboard& mbb = MarsBlackboard::getMarsBB(bb);
    HandTraj& move = (mHand == eLeft) ? mbb.lhand_traj() : mbb.rhand_traj();

    move.moveTime(mDelayTime);

    move.update("torso");

    Vec3 palm_in_torso { move.getTranslation() };
    display(cerr, palm_in_torso, rad2deg(move.getRPY()));
    cerr << " palm in torso " << nl;

    move.update();

    Vec3 palm_in_world { move.getTranslation() };
    display(cerr, palm_in_world, rad2deg(move.getRPY()));
    cerr << " palm in world " << nl;

    mys::display(cerr, mPosition, mOrient);
    cerr << " amount to move " << nl;

    tf2::Transform tf { move.getFrameLocation(mPosition.getX(), mPosition.getY(), mPosition.getZ(), "world", "rightPalm") };

    Vec3 move_in_torso { tf.getOrigin() };
    mys::display(cerr, move_in_torso);
    cerr << " move in world " << nl;

    move.update();
    move.incPose(mPosition, mOrient); //zero_vec3);
    move.send();

#if 0
    //    Vec3 moved_pos = palm_in_torso + move_in_torso;
    Vec3 moved_pos = palm_in_torso + mPosition;
    display(cerr, moved_pos);
    cerr << " moved pos in torso" << nl;

    tf2::Transform tfx { move.getFrameLocation(moved_pos.getX(), moved_pos.getY(), moved_pos.getZ(), "torso", "world") };
    Vec3 moved_in_world { tfx.getOrigin() };
    mys::display(cerr, moved_in_world);
    cerr << " moved in world" << nl;

    //    move.update("torso");
    //    display(cerr, move.getTranslation());
    //    cerr << " palm in torso ??" << nl;

    Vec3 orient = move.getRPY();
    //        orient.setX(orient.getX() + deg2rad( -30));

    move.update();
    move.setPose(moved_in_world, orient); //zero_vec3);
    //        Vec3 pos { 0.0, 0.0, 0.0 };
    //        Vec3 { 0.2, 0.0, 0.0 };
    //
    //        move.incPose(pos + Vec3 { 0.2, 0.0, 0.0 });
    //        move.incPose(pos + Vec3 { 0.0, -0.20, 0.0 });
    //        move.incPose(pos + Vec3 { 0.0, 0.0, 0.20 });
    //        move.incPose(pos + Vec3 { -0.2, 0.0, 0.0 });
    //        move.incPose(pos + Vec3 { 0.2, 0.2, 0.0 });
    //        move.incPose(pos + Vec3 { 0.0, 0.0, -0.20 });

//    move.send();
#endif

    setTimer();
    setState(Running);
    //    setState(Success);
}
