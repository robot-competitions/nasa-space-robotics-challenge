#ifndef IsHarnessDone_H
#define IsHarnessDone_H

#include <ConditionNode.h>
#include <HarnessStatus.h>
#include "../MarsBlackboard.h"

using namespace BT;
//---------------------------------------------------------------------------------------------------------------------
string status_text[] {
    "Released", "Attaching", "Attached", "Lowering", "Standing", "Releasing"
};
//---------------------------------------------------------------------------------------------------------------------
class IsHarnessActive : public ConditionNode {
public:
    IsHarnessActive(const string& name) :
        ConditionNode(name) {
    }
    ~IsHarnessActive() = default;

    //---------------------------------------------------------------------------------------------------------------------
    const virtual TreeNode::NodeState eval(Blackboard& bb) override {
        Depth depth;
        mys::display(indent(cerr), getNodeId(), getName()) << " start " << mys::nl;

        MarsBlackboard& mbb = MarsBlackboard::getMarsBB(bb);
        HarnessStatus& harness = mbb.harness_status();

        mys::display(indent(cerr), getNodeId(), getName()) << " " << status_text[harness.status()] << mys::nl;

        if (harness.status() == HarnessStatus::eNone) {
            setState(Failure);
        }
        else {
            setState(Success);
        }
        mys::display(indent(cerr), getNodeText()) << mys::nl;
        return getState();
    }
};

#endif
