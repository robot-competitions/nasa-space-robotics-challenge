/*
 * Visualize.cpp
 *
 *  Created on: Mar 16, 2017
 *      Author: rmerriam
 */
#include <mystic.h>
#include <PointCloud2Sub.h>
#include <ImageVision.h>
using namespace cv;

#include "Visualize.h"
//---------------------------------------------------------------------------------------------------------------------
Visualize::Visualize(const string& name) :
        ActionNode::ActionNode(name) {
}
//---------------------------------------------------------------------------------------------------------------------
const BT::TreeNode::NodeState Visualize::eval(Blackboard& bb) {
    Depth depth;
    mys::display(indent(cerr), getNodeId(), getName()) << " start " << mys::nl;

    MarsBlackboard& mbb = MarsBlackboard::getMarsBB(bb);

    PointCloud2Sub& pc2 = mbb.point_cloud();

    if (pc2.isValid()) {
        ImageVision img(pc2.topic());
        updateWindow(img.flipped(), "original");
    }

    setState(Success);
    mys::display(indent(cerr), getNodeText()) << mys::nl;
    return getState();
}
