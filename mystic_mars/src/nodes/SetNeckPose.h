/*
 * SetNeckPose.h
 *
 *  Created on: Mar 16, 2017
 *      Author: rmerriam
 */

#ifndef SetNeckPose_H_
#define SetNeckPose_H_

#include "RPYPoseAbstract.h"
#include <NeckTraj.h>
#include <RPY.h>
#include "../MarsBlackboard.h"
using namespace BT;

using namespace BT;
/*---------------------------------------------------------------------------------------------------------------------
 *  Roll    Forward max 42
 *          Back    max -12
 *  Pitch   Right   max 17
 *  Yaw     Left    max 70
 *
 *---------------------------------------------------------------------------------------------------------------------*/

class SetNeckPose : public RPYPoseAbstract<NeckTraj> {
public:

    SetNeckPose(const string& name, const double& delay);      //  Constructor to execute move created by another node
    SetNeckPose(const string& name, const RPY& rpy = rpy_no_value);    // constructor to move with no delay
    SetNeckPose(const string& name, const double& delay, const RPY& rpy = rpy_no_value);   // constructor with delay
    virtual ~SetNeckPose() = default;

private:
    virtual NeckTraj& getTraj(const MarsBlackboard& mbb) const override;
    virtual const RPY getBlackBoardPose(const MarsBlackboard& mbb) override;
};
//---------------------------------------------------------------------------------------------------------------------
inline SetNeckPose::SetNeckPose(const string& name, const double& delay, const RPY& rpy) :
    RPYPoseAbstract(name, delay, rpy) {
}
//---------------------------------------------------------------------------------------------------------------------
inline SetNeckPose::SetNeckPose(const string& name, const RPY& rpy) :
    SetNeckPose(name, 0, rpy) {
}
//---------------------------------------------------------------------------------------------------------------------
inline SetNeckPose::SetNeckPose(const string& name, const double& delay) :
    SetNeckPose(name, delay, rpy_no_value) {
}
//---------------------------------------------------------------------------------------------------------------------
inline NeckTraj& SetNeckPose::getTraj(const MarsBlackboard& mbb) const {
    return mbb.neck_traj();
}
//---------------------------------------------------------------------------------------------------------------------
inline const RPY SetNeckPose::getBlackBoardPose(const MarsBlackboard& mbb) {
    return mbb.mNeckPose;
}

#endif /* SetNeckPose_H_ */
