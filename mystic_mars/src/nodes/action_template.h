#ifndef action_template_H
#define action_template_H

#include <ActionNode.h>
#include <Blackboard.h>

//#include "MarsBlackboard.h"
//---------------------------------------------------------------------------------------------------------------------
class action_template : public BT::ActionNode {
public:
    //---------------------------------------------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------------------------------
    action_template(const string& name) :
            ActionNode::ActionNode(name) {
    }
    virtual ~action_template() = default;

    //---------------------------------------------------------------------------------------------------------------------
    const virtual BT::TreeNode::NodeState eval(BT::Blackboard& bb) override
    {
        //        MarsBlackboard& mbb = MarsBlackboard::getMarsBB(bb);
        setState(Failure);
        setState(Success);
        return getState();
    }

private:

};
#endif
