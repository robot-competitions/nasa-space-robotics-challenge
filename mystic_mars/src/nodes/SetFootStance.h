/*
 * StartupStance.h
 *
 *  Created on: Mar 16, 2017
 *      Author: rmerriam
 */

#ifndef SetFootStance_H_
#define SetFootStance_H_

#include "RunningNodeAbstract.h"

#include "../MarsBlackboard.h"
using namespace BT;

//---------------------------------------------------------------------------------------------------------------------
class SetFootStance : public RunningNodeAbstract {
public:
    SetFootStance(const string& name);
    virtual ~SetFootStance() = default;


protected:
    virtual void nodeRunning(Blackboard& bb) override;
    virtual void nodeNotRunning(Blackboard& bb) override;

};
#endif /* SetFootStance_H_ */
