#include <mystic.h>

#include "../MarsBlackboard.h"
using namespace mys;

#include "CalcSolarPanelPose.h"
//---------------------------------------------------------------------------------------------------------------------
void CalcSolarPanelPose::calcConsoleAngle(double dx, double dy, MarsBlackboard& mbb) {

    double angle = atan2(dy, dx);

    mbb.mTurnAngle = angle; // (angle > 0) ? (M_PI - angle) : -(M_PI + angle);
}
//---------------------------------------------------------------------------------------------------------------------
const BT::TreeNode::NodeState CalcSolarPanelPose::eval(BT::Blackboard& bb)
    {
    MarsBlackboard& mbb = MarsBlackboard::getMarsBB(bb);

    RPY red_button { mbb.mVisonLocations[eButton] };
    RPY box_panel { mbb.mVisonLocations[eSolarPanel] };

    if (red_button.isValid() && box_panel.isValid()) {

        double dx = red_button.mRoll;
        double dy = red_button.mPitch;

//        double slope = -dy / dx;
//
//        double avg_x = (red_button.mRoll + box_panel.mRoll) / 2.0;
//        double avg_y = (red_button.mPitch + box_panel.mPitch) / 2.0;

//        mbb.mDistanceToTurn = slope * (0 - avg_y) + avg_x;
//
//        if ( !std::isnan(mAdjust)) {
//            mbb.mDistanceToTurn = mbb.mDistanceToTurn + mAdjust;
//        }
//
//        mbb.mDistanceToObject = (red_button.mRoll + box_panel.mRoll) / 2.0;
//
//        if ( !std::isnan(mAdjust)) {
//            mbb.mDistanceToObject = mbb.mDistanceToObject + mAdjust;
//        }
//
//        if (std::isnan(mbb.mDistanceToObject) || std::isnan(mbb.mDistanceToTurn)) {
//            setState(Running);
//            return getState();
//        }
        calcConsoleAngle(dx, dy, mbb);

        cerr << "solar panel ";
        display(cerr, box_panel, red_button) << tab;
        display(cerr, mbb.mTurnAngle.load(), rad2deg(mbb.mTurnAngle.load())) << tab;
        cerr << nl;

        setState(Success);
        setState(Failure);
    }
    else {
        setState(Failure);
    }
    return getState();
}
