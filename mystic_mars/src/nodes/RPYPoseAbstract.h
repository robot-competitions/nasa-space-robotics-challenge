/*
 * StartupStance.h
 *
 *  Created on: Mar 16, 2017
 *      Author: rmerriam
 */

#ifndef RPYPoseAbstract_H_
#define RPYPoseAbstract_H_

#include <RPY.h>
#include "RunningNodeAbstract.h"

#include "../MarsBlackboard.h"
using namespace BT;
//---------------------------------------------------------------------------------------------------------------------
template <typename T>
class RPYPoseAbstract : public RunningNodeAbstract {
public:

    RPYPoseAbstract(const string& name, const double& delay, const RPY& rpy);
    virtual ~RPYPoseAbstract() = default;

    const virtual TreeNode::NodeState eval(Blackboard& bb) override;

protected:
    virtual void nodeNotRunning(Blackboard& bb) override;

    virtual T& getTraj(const MarsBlackboard& mbb) const = 0;
    virtual const RPY getBlackBoardPose(const MarsBlackboard& mbb) = 0;

    const RPY mRPY;
};
//---------------------------------------------------------------------------------------------------------------------
template <typename T>
inline RPYPoseAbstract<T>::RPYPoseAbstract(const string& name, const double& delay, const RPY& rpy) :
    RunningNodeAbstract(name, delay), mRPY(rpy) {
}
//---------------------------------------------------------------------------------------------------------------------
template <typename T>
inline const BT::TreeNode::NodeState RPYPoseAbstract<T>::eval(Blackboard& bb) {
    Depth depth;
    mys::display(indent(cerr), getNodeId(), getName()) << " start " << mys::nl;
    return RunningNodeAbstract::eval(bb);
}
//---------------------------------------------------------------------------------------------------------------------
template <typename T>
inline void RPYPoseAbstract<T>::nodeNotRunning(Blackboard& bb) {
    MarsBlackboard& mbb = MarsBlackboard::getMarsBB(bb);
    T& rpy_pose = getTraj(mbb);

    if (std::isnan(mRPY.mRoll)) {
        rpy_pose.incPose(getBlackBoardPose(mbb));
    }
    else if (std::isinf(mRPY.mRoll)) {
        rpy_pose.resetPose();
    }
    else {
        rpy_pose.incPose(mRPY);
    }
    rpy_pose.moveTime(mDelayTime);
    rpy_pose.delayTime(0);
    rpy_pose.send();

    setTimer();
    setState(Running);

    ROS_DEBUG_STREAM(ros::this_node::getName() << " " << typeid( *this).name() << " done");
}

#endif /* RPYPoseAbstract_H_ */
