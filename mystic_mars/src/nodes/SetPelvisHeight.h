/*
 * StartupStance.h
 *
 *  Created on: Mar 16, 2017
 *      Author: rmerriam
 */

#ifndef SETPELVISHEIGHT_H_
#define SETPELVISHEIGHT_H_

#include "RunningNodeAbstract.h"
#include <PelvisHeightTraj.h>

#include "../MarsBlackboard.h"
using namespace BT;

//---------------------------------------------------------------------------------------------------------------------
class SetPelvisHeight : public RunningNodeAbstract {
public:
    SetPelvisHeight(const string& name, const double& height = 1.05, const double& delay = 0.0);
    virtual ~SetPelvisHeight() = default;

protected:
    virtual void nodeNotRunning(Blackboard& bb) override;

    double mHeight;

};
#endif /* SETPELVISHEIGHT_H_ */
