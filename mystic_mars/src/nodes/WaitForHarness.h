#ifndef WaitForHarness_H
#define WaitForHarness_H

#include "RunningNodeAbstract.h"
#include <HarnessStatus.h>
#include "../MarsBlackboard.h"

using namespace BT;
//---------------------------------------------------------------------------------------------------------------------
class WaitForHarness : public RunningNodeAbstract {
public:
    WaitForHarness(const string& name, const double& delay = 10.0) :
        RunningNodeAbstract(name, 10) {
    }
    ~WaitForHarness() = default;

protected:
    //---------------------------------------------------------------------------------------------------------------------
    virtual void nodeNotRunning(Blackboard& bb) override {
        Depth depth;
        mys::display(indent(cerr), getNodeId(), getName()) << " start " << mys::nl;

        MarsBlackboard& mbb = MarsBlackboard::getMarsBB(bb);
        HarnessStatus& harness = mbb.harness_status();

        if (harness.status() == HarnessStatus::eNone) {
            setState(Success);
        }
        else {
            setTimer();
            setState(Running);
        }
    }
    //---------------------------------------------------------------------------------------------------------------------
    virtual void nodeRunning(Blackboard& bb) override {
        Depth depth;
        mys::display(indent(cerr), getNodeId(), getName()) << " running " << mys::nl;

        MarsBlackboard& mbb = MarsBlackboard::getMarsBB(bb);
        HarnessStatus& harness = mbb.harness_status();

        if (harness.status() == HarnessStatus::eNone) {
            setState(Success);
        }
        else if (isTimerExpired()) {
            setState(Failure);
        }
    }
};

#endif
