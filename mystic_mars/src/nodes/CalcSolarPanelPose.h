#ifndef CalcMoveDistance_H
#define CalcMoveDistance_H

#include <ActionNode.h>
//---------------------------------------------------------------------------------------------------------------------
class CalcSolarPanelPose : public BT::ActionNode {
public:
    CalcSolarPanelPose(const string& name, const double& adjust_by = mys::no_value);
    virtual ~CalcSolarPanelPose() = default;

    const virtual BT::TreeNode::NodeState eval(BT::Blackboard& bb) override;

protected:
    void calcConsoleAngle(double dx, double dy, MarsBlackboard& mbb);

private:
    const double mAdjust;
};
//---------------------------------------------------------------------------------------------------------------------
inline CalcSolarPanelPose::CalcSolarPanelPose(const string& name, const double& adjust_by) :
    ActionNode::ActionNode { name }, mAdjust { adjust_by } {
}

#endif
