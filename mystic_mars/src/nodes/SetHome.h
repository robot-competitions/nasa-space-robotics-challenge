#ifndef SetHome_H
#define SetHome_H

#include <mystic.h>
#include "RunningNodeAbstract.h"
#include <GoHome.h>

using namespace BT;
//---------------------------------------------------------------------------------------------------------------------
class SetHome : public RunningNodeAbstract {
public:
    SetHome(const string& name, const mys::Side side, const GoHome::GoPart part, const double& delay) :
        RunningNodeAbstract(name, delay), mSide(side), mPart(part), mDelay(delay) {
    }
    virtual ~SetHome() = default;

protected:
    virtual void nodeNotRunning(Blackboard& bb) override;

    const mys::Side mSide;
    const GoHome::GoPart mPart;
    const double mDelay;
};
#endif
