/*
 * IsCheckpointDone.cpp
 *
 *  Created on: Apr 28, 2017
 *      Author: rmerriam
 */

#include <mystic.h>
#include "../MarsBlackboard.h"

#include "IsCheckpointDone.h"
//---------------------------------------------------------------------------------------------------------------------
IsCheckpointDone::IsCheckpointDone(const string& name, const uint8_t task, const uint8_t checkpoint) :
    ConditionNode { name }, mTask { task }, mCheckPt { checkpoint } {
}
//---------------------------------------------------------------------------------------------------------------------
const BT::TreeNode::NodeState IsCheckpointDone::eval(Blackboard& bb) {
    Depth depth;
    mys::display(indent(cerr), getNodeId(), getName()) << " start " << mys::nl;

    MarsBlackboard& mbb = MarsBlackboard::getMarsBB(bb);
    TaskStatus& status = mbb.task_status();

//    mys::display(cerr, status.task(), status.checkPt(), (uint32_t)status.isFinished()) << mys::nl;
    if ( !status.isValid()) {
        setState(Failure);
        cerr << "task status task not valid" << mys::nl;
    }
    else if ((status.task() <= mTask) && (status.isFinished())) {
        setState(Success);
    }
    else if ((status.task() <= mTask) && (status.checkPt() <= mCheckPt)) {
        // still on task and checkpoint so not finished
        setState(Failure);
    }
    else {
        setState(Success);
    }

    mys::display(indent(cerr), getNodeText()) << mys::nl;
    return getState();
}
