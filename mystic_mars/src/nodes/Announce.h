#ifndef Announce_H
#define Announce_H

#include <ActionNode.h>
#include <Blackboard.h>

class Announce : public BT::ActionNode {
public:
    //---------------------------------------------------------------------------------------------------------------------
    Announce(const string& name, const string& text, const TreeNode::NodeState response = Success) :
    ActionNode::ActionNode(name), mText {text}, mResponse {response} {
    }
    virtual ~Announce() = default;

    //---------------------------------------------------------------------------------------------------------------------
    const virtual BT::TreeNode::NodeState eval(BT::Blackboard& bb) override
    {
        ROS_INFO_STREAM(ros::this_node::getName() << ' ' << mName << ' ' << mText);

        setState(mResponse);
        return getState();
    }

private:
    const string mText;
    const TreeNode::NodeState mResponse;
};
#endif
