/*
 * StartupStance.cpp
 *
 *  Created on: Mar 16, 2017
 *      Author: rmerriam
 */
#include <mystic.h>
#include <RobotPose.h>
#include <Walk.h>
#include <WalkMotion.h>

#include "walk_params.h"
#include "SetFootStance.h"
//---------------------------------------------------------------------------------------------------------------------
SetFootStance::SetFootStance(const string& name) :
    RunningNodeAbstract(name, 0.0) {
}
//---------------------------------------------------------------------------------------------------------------------
void SetFootStance::nodeRunning(Blackboard& bb) {
    MarsBlackboard& mbb = MarsBlackboard::getMarsBB(bb);
    WalkMotion& move = mbb.walk_motion();

    if (move.isStanding()) {
        setState(Success);
    }
    else {
        setState(Running);
    }
}
//---------------------------------------------------------------------------------------------------------------------
void SetFootStance::nodeNotRunning(Blackboard& bb) {
    using namespace mys;

    MarsBlackboard& mbb = MarsBlackboard::getMarsBB(bb);
//    RobotPose& robot_pose = mbb.robot_pose();

//    if (robot_pose.poseX() < 0.3)
    {
        Walk& walk = mbb.walk();
        WalkParams::walkSetup(walk);

        walk.startSteps();
        walk.alignFeet();

        Vec3 r_base = walk.getLocation(mys::eRight);
        Vec3 l_base = walk.getLocation(mys::eLeft);

        display(cerr, l_base, r_base);

        if (abs(r_base.getY()) < abs(l_base.getY())) {
            walk.setStep(mys::eRight, Vec3 { 0, 0, r_base.getZ() });
            walk.setStep(mys::eLeft, Vec3 { 0, 0, l_base.getZ() });
        }
        else {
            walk.setStep(mys::eLeft, Vec3 { 0, 0, l_base.getZ() });
            walk.setStep(mys::eRight, Vec3 { 0, 0, r_base.getZ() });
        }

//        walk.doSteps();
        setState(Running);
    }
//    else {
//        setState(Success);
//    }
    ROS_DEBUG_STREAM(ros::this_node::getName() << getName() << " done");
}

//---------------------------------------------------------------------------------------------------------------------
//const BT::TreeNode::NodeState SetFootStance::eval(Blackboard& bb) {
//    ROS_DEBUG_STREAM(ros::this_node::getName() << getName());
//
//    if (getState() == Running) {
//        nodeRunning();
//    }
//    else {
//        nodeNotRunning(bb);
//    }
//    return getState();
//}
