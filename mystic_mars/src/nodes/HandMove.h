/*
 * WarmUp.h
 *
 *  Created on: Dec 8, 2016
 *      Author: rmerriam
 */

#ifndef SRC_WARMUP_H_
#define SRC_WARMUP_H_
#include <mystic.h>
using namespace mys;

#include <HandTraj.h>
#include "../MarsBlackboard.h"
using namespace mys;
using namespace BT;
#include "RunningNodeAbstract.h"

/*---------------------------------------------------------------------------------------------------------------------
 *
 *  The move is relative to the current hand position
 ---------------------------------------------------------------------------------------------------------------------*/

//---------------------------------------------------------------------------------------------------------------------
class HandMove : public RunningNodeAbstract {
public:
    HandMove(const string& name, const mys::Side side, const RPY& position, const RPY& orient = rpy_zero, const double& delay = 1.0) :
        RunningNodeAbstract { name, delay }, mHand { side }, mPosition { toVec3(position) }, mOrient { toVec3(orient) }
    {
    }
    virtual ~HandMove() = default;

protected:

    Vec3 toVec3(const RPY& rpy) {
        return Vec3 { rpy.mRoll, rpy.mPitch, rpy.mYaw };

    }
    virtual void nodeRunning(Blackboard& bb) override;
    virtual void nodeNotRunning(Blackboard& bb) override;

private:
    mys::Side mHand;
    const Vec3 mPosition;
    const Vec3 mOrient;
};
#endif /* SRC_WARMUP_H_ */

/*
 *
 *     double robo_x { RobotPose::poseX() };
 double robo_y { RobotPose::poseY() };
 double robo_z { RobotPose::poseZ() };
 *         tf2::Vector3 rot { 0.0, 0.0, 0.0 };
 tf2::Vector3 pos = tf2::Vector3(robo_x + 0.2, robo_y - 0.12, robo_z + 0.2);
 mMoveHand(mystic::eRight, pos, rot, true);
 */
