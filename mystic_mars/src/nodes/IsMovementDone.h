#ifndef IsMovementDone_H
#define IsMovementDone_H

#include <ConditionNode.h>
#include <WalkMotion.h>
#include "../MarsBlackboard.h"

using namespace BT;
//---------------------------------------------------------------------------------------------------------------------
class IsMovementDone : public ConditionNode {
public:
    IsMovementDone(const string& name) :
            ConditionNode(name) {
    }
    ~IsMovementDone() = default;

    //---------------------------------------------------------------------------------------------------------------------
    const virtual TreeNode::NodeState eval(Blackboard& bb) override {
        ROS_DEBUG_STREAM(ros::this_node::getName() << getName());

        MarsBlackboard& mbb = MarsBlackboard::getMarsBB(bb);
        WalkMotion& move = mbb.walk_motion();

        if (move.isStanding()) {
            setState(Success);
        }
        else {
            setState(Running);
        }
        return getState();
    }

private:

};

#endif
