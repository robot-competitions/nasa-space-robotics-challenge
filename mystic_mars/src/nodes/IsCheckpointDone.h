/*
 * IsCheckpointDone.h
 *
 *  Created on: Apr 28, 2017
 *      Author: rmerriam
 */

#ifndef IsCheckpointDone_H
#define IsCheckpointDone_H

#include <ConditionNode.h>
#include <TaskStatus.h>

using namespace BT;
//---------------------------------------------------------------------------------------------------------------------
class IsCheckpointDone : public ConditionNode {
public:
    IsCheckpointDone(const string& name, const uint8_t task, const uint8_t checkpoint);
    ~IsCheckpointDone() = default;
    const virtual TreeNode::NodeState eval(Blackboard& bb) override;

private:
    uint8_t mTask;
    uint8_t mCheckPt;

};
#endif
