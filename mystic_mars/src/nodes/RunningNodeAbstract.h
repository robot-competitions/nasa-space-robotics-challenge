/*
 * RunningNodeAbstract.h
 *
 *  Created on: Apr 28, 2017
 *      Author: rmerriam
 */

#ifndef RUNNINGNODEABSTRACT_H_
#define RUNNINGNODEABSTRACT_H_
#include <mystic.h>
#include <ActionNode.h>
//---------------------------------------------------------------------------------------------------------------------
namespace BT {

    class RunningNodeAbstract : public ActionNode {
    public:
        RunningNodeAbstract(const string& name, const double& delay = 0.0) :
            ActionNode(name), mDelayTime(delay) {
            if (mDelayTime != 0.0) {
                setTimerCallback();
            }
        }
        ~RunningNodeAbstract() = default;

        virtual const NodeState eval(Blackboard& bb) override;

    protected:
        virtual void nodeRunning(Blackboard& bb);
        virtual void nodeNotRunning(Blackboard& bb) = 0;

        void setTimerCallback();
        void timerCallback(const TimerEvent& event);
        void setTimer();
        bool isTimerExpired() const {
            return mTimerExpired;
        }

        NodeHandle mNodeHandle;
        Timer mTimer;
        TimerEvent mTimerEvent;
        atomic_bool mTimerExpired { true };
        double mDelayTime;

    };
    //---------------------------------------------------------------------------------------------------------------------
    inline const TreeNode::NodeState RunningNodeAbstract::eval(Blackboard& bb) {
        Depth depth;
        mys::display(indent(cerr), getNodeId(), getName()) << " start " << mys::nl;

        if (getState() == Running) {
            nodeRunning(bb);
        }
        else {
            nodeNotRunning(bb);
        }
        mys::display(indent(cerr), getNodeText()) << mys::nl;
        return getState();
    }
    //---------------------------------------------------------------------------------------------------------------------
    inline void RunningNodeAbstract::nodeRunning(Blackboard& bb) {
        if (isTimerExpired()) {
            setState(Success);
        }
    }
    //---------------------------------------------------------------------------------------------------------------------
    inline void RunningNodeAbstract::setTimerCallback() {
        mTimer = mNodeHandle.createTimer(ros::Duration(0.0), &RunningNodeAbstract::timerCallback, this, true, false);
    }
    //---------------------------------------------------------------------------------------------------------------------
    inline void RunningNodeAbstract::timerCallback(const TimerEvent& event) {
        mTimerEvent = event;
        mTimerExpired = true;
    }
    //---------------------------------------------------------------------------------------------------------------------
    inline void RunningNodeAbstract::setTimer() {
        mTimer.stop();
        if (mDelayTime != 0.0) {
            mTimer.setPeriod(Duration(mDelayTime + 0.1));
            mTimerExpired = false;
        }
        mTimer.start();
    }
}
#endif /* RUNNINGNODEABSTRACT_H_ */
