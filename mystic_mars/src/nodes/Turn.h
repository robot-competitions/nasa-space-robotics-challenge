/*
 * StartupStance.h
 *
 *  Created on: Mar 16, 2017
 *      Author: rmerriam
 */

#ifndef Turn_H_
#define Turn_H_

#include "RunningNodeAbstract.h"

#include "../MarsBlackboard.h"
using namespace BT;

//---------------------------------------------------------------------------------------------------------------------
class Turn : public RunningNodeAbstract {
public:
    enum eType {
        eZero, eAlign, eBlackboard, eValue, eStance
    };
    Turn(const string& name, const eType type);
    Turn(const string& name, const double& yaw);

    virtual ~Turn() = default;

protected:
    virtual void nodeRunning(Blackboard& bb) override;
    virtual void nodeNotRunning(Blackboard& bb) override;
    void setStance(Walk& walk);
    void doTurn(double yaw, Walk& walk);
    const double hypotenuse(const Vec3& new_spread) const;

private:
    double mYaw;
    const eType mType { eZero };
    static constexpr double mMaxTurn { M_PI_4 };
    const double mWidth { 0.30 };
};
#endif /* Turn_H_ */
