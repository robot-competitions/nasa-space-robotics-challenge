/*
 * StartupStance.h
 *
 *  Created on: Mar 16, 2017
 *      Author: rmerriam
 */

#ifndef Move_H_
#define Move_H_

#include "RunningNodeAbstract.h"

#include "../MarsBlackboard.h"
using namespace BT;

//---------------------------------------------------------------------------------------------------------------------
class Move : public RunningNodeAbstract {
public:
    Move(const bool climb, const string& name, const int& num_steps);
    Move(const string& name, const double& distance);
    Move(const string& name, const bool use_turn_distance = false, const bool backup = false);
    virtual ~Move() = default;

protected:
    virtual void nodeRunning(Blackboard& bb) override;
    virtual void nodeNotRunning(Blackboard& bb) override;
    double determineDistance(double distance, MarsBlackboard& mbb);

private:
    void doSteps(MarsBlackboard& bb);
    void doStairsRunning(MarsBlackboard& mbb);

    void move(double distance, Walk& walk);
    bool absLessThan(const double& distance, const double& step) const;

    const double mDistance { };
    const bool mUseTurnDistance { false };
    const bool mBackup { false };

    const int mNumStairs { };
    int mStairCnt { };
    const bool mClimbStairs { false };
    bool mIsFirstStair { true };

    double mHalfStep { };
    double mStep { };

    static constexpr double stair_depth { 0.24 };
    static constexpr double mStrideForward { 0.8 };
    static constexpr double mStrideReverse { -0.5 };
};
#endif /* Move_H_ */
