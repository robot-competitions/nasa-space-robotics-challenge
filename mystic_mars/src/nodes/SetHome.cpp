#include <mystic.h>

#include <BehaviorTree.h>
#include "SetHome.h"
#include "../MarsBlackboard.h"

using namespace BT;
//--------------------------------------------------------------------------------------------------------------------------
void SetHome::nodeNotRunning(Blackboard& bb) {
    MarsBlackboard& mbb = MarsBlackboard::getMarsBB(bb);
    GoHome& go_home = mbb.go_home();

    go_home.setPose(mSide, mPart, mDelay);
    go_home.send();

    setTimer();
    setState(Running);
}

#include "../MarsBlackboard.h"
//#include "SetHome.h"
