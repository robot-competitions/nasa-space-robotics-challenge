/*
 * StartupStance.cpp
 *
 *  Created on: Mar 16, 2017
 *      Author: rmerriam
 */
#include <mystic.h>
using namespace mys;
#include <RobotPose.h>
#include <Walk.h>
#include <WalkMotion.h>

#include "walk_params.h"
#include "Turn.h"
using namespace BT;

//---------------------------------------------------------------------------------------------------------------------
Turn::Turn(const string& name, const double& yaw) :
    RunningNodeAbstract { name }, mYaw { yaw }, mType { Turn::eValue } {
}
//---------------------------------------------------------------------------------------------------------------------
Turn::Turn(const string& name, const eType type) :
    RunningNodeAbstract { name }, mYaw { }, mType { type } {
}
//---------------------------------------------------------------------------------------------------------------------
void Turn::nodeRunning(Blackboard& bb) {
    MarsBlackboard& mbb = MarsBlackboard::getMarsBB(bb);
    WalkMotion& move = mbb.walk_motion();

    if (move.isStanding()) {
        setState(Success);
    }
    else {
        setState(Running);
    }
}
//---------------------------------------------------------------------------------------------------------------------
void Turn::doTurn(double yaw, Walk& walk) {

    mYaw = yaw;

    if (abs(yaw) > .02) {
        double max_yaw = (yaw >= 0.0) ? mMaxTurn : -mMaxTurn;

        walk.startSteps();

        while (abs(yaw) > mMaxTurn) {
            walk.turn(max_yaw);
            yaw -= max_yaw;
        }
        walk.turn(yaw);

        walk.doSteps();
        setState(Running);
    }
    else {
        setState(Success);
    }
}
//---------------------------------------------------------------------------------------------------------------------
void Turn::nodeNotRunning(Blackboard& bb) {

    MarsBlackboard& mbb = MarsBlackboard::getMarsBB(bb);
    Walk& walk = mbb.walk();

    switch (mType) {
        case eZero:
            doTurn( -mbb.robot_pose().yaw(), walk);
            break;

        case eAlign: {
            double angle { rad2deg(mbb.robot_pose().yaw()) };
            double new_angle = lround(angle / 45.0) * 45.0;
            doTurn(deg2rad(new_angle - angle), walk);
            break;
        }

        case eBlackboard:
            doTurn(mbb.mTurnAngle, walk);
            break;

        case eValue:
            doTurn(mYaw, walk);
            break;

        case eStance:
            setStance(walk);
            break;
    }
}
//---------------------------------------------------------------------------------------------------------------------
inline const double Turn::hypotenuse(const Vec3& value) const {
    return sqrt(pow(value.getX(), 2) + pow(value.getY(), 2));
}
//---------------------------------------------------------------------------------------------------------------------
void Turn::setStance(Walk& walk) {

    walk.updateByFrame("torso");
    // get location in world frame
    Vec3 l_base { walk.getLocation(eLeft) };
    Vec3 r_base { walk.getLocation(eRight) };

    // this is now foot frame
    Vec3 spread { l_base - r_base };

    double angle { atan2( -spread.getX(), spread.getY()) };
    double avg_yaw { (walk.getRPY(eLeft).mYaw + walk.getRPY(eRight).mYaw) / 2.0 };
    double shr { angle - avg_yaw };

//    cerr << "shr ";
//    display(cerr, shr, (spread.getY() - mWidth));
//    display(cerr, spread) << nl;

    bool do_move { false };
    if (abs(shr) > 0.03) {    //  0.03 - 2 degress
        do_move = true;
    }
    else {
        spread.setX(0);
    }

    if (abs(spread.getY() - mWidth) > 0.05) {   // now adjust the y pos of the left foot
        spread.setY(spread.getY() - mWidth);
        do_move = true;
    }
    else {
        spread.setY(0);
    }

    if (spread.getZ() < 0.0) {      // right is higher than left - give a boost to left Z
        spread.setZ(spread.getZ() + 0.1);
        do_move = true;
    }

    if (do_move) {
//        cerr << "pos ";
//        display(cerr, l_base, r_base);
//        display(cerr, l_base - spread) << nl;

        walk.startSteps();
        walk.incStep(mys::eLeft, -spread);
        walk.doSteps();
        setState(Running);
    }
    else {
        setState(Success);
    }
}
