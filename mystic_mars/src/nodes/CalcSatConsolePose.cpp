#include <mystic.h>

#include "../MarsBlackboard.h"
using namespace mys;

#include "CalcSatConsolePose.h"
//---------------------------------------------------------------------------------------------------------------------
void CalcSatConsolePose::calcConsoleAngle(double dx, double dy, MarsBlackboard& mbb) {

    double angle = atan2(dx, dy);

    mbb.mTurnAngle = (angle > 0) ? (M_PI - angle) : -(M_PI + angle);
}
//---------------------------------------------------------------------------------------------------------------------
const BT::TreeNode::NodeState CalcSatConsolePose::eval(BT::Blackboard& bb) {
    Depth depth;
    mys::display(BT::indent(cerr), getNodeId(), getName()) << " start " << mys::nl;

    MarsBlackboard& mbb = MarsBlackboard::getMarsBB(bb);

    RPY red_wheel { mbb.mObjectDistances[eRedWheel] };
    RPY blue_wheel { mbb.mObjectDistances[eBlueWheel] };

    if (red_wheel.isValid() && blue_wheel.isValid()) {

        double dx = blue_wheel.mRoll - red_wheel.mRoll;
        double dy = blue_wheel.mPitch - red_wheel.mPitch;

        double slope = -dy / dx;

        double avg_x = (red_wheel.mRoll + blue_wheel.mRoll) / 2.0;
        double avg_y = (red_wheel.mPitch + blue_wheel.mPitch) / 2.0;

        mbb.mDistanceToTurn = slope * (0 - avg_y) + avg_x;

        if ( !std::isnan(mAdjust)) {
            mbb.mDistanceToTurn = mbb.mDistanceToTurn + mAdjust;
        }

        mbb.mDistanceToObject = (red_wheel.mRoll + blue_wheel.mRoll) / 2.0;

        if ( !std::isnan(mAdjust)) {
            mbb.mDistanceToObject = mbb.mDistanceToObject + mAdjust;
        }

        if (std::isnan(mbb.mDistanceToObject) || std::isnan(mbb.mDistanceToTurn)) {
            setState(Running);
            return getState();
        }
        calcConsoleAngle(dx, dy, mbb);

        cerr << "sat console ";
        display(cerr, blue_wheel, red_wheel) << tab;
        display(cerr, avg_x, avg_y) << tab;
        display(cerr, mbb.mDistanceToTurn) << tab;
        display(cerr, mbb.mDistanceToObject.load(), mAdjust) << tab;
        display(cerr, dx, dy, mbb.mTurnAngle.load(), rad2deg(mbb.mTurnAngle.load())) << nl;

        setState(Success);
    }
    else {
        setState(Failure);
    }
    return getState();
}
