#include <mystic.h>

#include "../publishers.h"
#include "../subscribers.h"

#include <BehaviorTree.h>
using namespace BT;

#include "../MarsBlackboard.h"
#include "BlackboardStart.h"
//---------------------------------------------------------------------------------------------------------------------
BlackboardStart::BlackboardStart(const string& name) :
    ActionNode::ActionNode(name) {
}
//---------------------------------------------------------------------------------------------------------------------
const TreeNode::NodeState BlackboardStart::eval(Blackboard& bb) {
    Depth depth;
    mys::display(indent(cerr), getNodeId(), getName()) << " start " << mys::nl;

    MarsBlackboard& mbb = MarsBlackboard::getMarsBB(bb);
    setState(Failure);

    bool status =
        //    mbb.foot_status().isValid() && // not operational until footsteps happen???
        mbb.double_support().isValid() &&     //
            mbb.harness_status().isValid() &&     //
            mbb.robot_pose().isValid() &&     //
            mbb.joint_status().isValid() &&         //
            mbb.point_cloud().isValid() &&          //
            mbb.walk_motion().isValid() &&          //
            //
            mbb.go_home().isSubscribed() &&  //
            mbb.raise_pelvis().isSubscribed() &&    //
            mbb.walk().isSubscribed() &&    //
            true;
    if (status) {
        setState(Success);
    }

    mys::display(indent(cerr), getNodeText()) << mys::nl;
    return getState();
}

