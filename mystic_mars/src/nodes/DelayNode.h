#ifndef DelayNode_H
#define DelayNode_H

//  Utility node for debugging - delays a specified amount of time specified by mWaitTime on the Blackboard

#include <ActionNode.h>

//---------------------------------------------------------------------------------------------------------------------
class DelayNode : public BT::ActionNode {
public:
    //---------------------------------------------------------------------------------------------------------------------
    DelayNode(const string& name, const double delay = 0.0) :
            ActionNode::ActionNode { name }, mDelay { delay } {
    }
    virtual ~DelayNode() = default;

    //---------------------------------------------------------------------------------------------------------------------
    const virtual BT::TreeNode::NodeState eval(BT::Blackboard& bb) override {
        MarsBlackboard& mbb = MarsBlackboard::getMarsBB(bb);
        Depth depth;
        mys::display(indent(cerr), getNodeId(), getName()) << " start " << mys::nl;

        if (mDelay > 0.0) {
            mbb.mWaitTime = mDelay;
        }
        if (mbb.mWaitTime > 0.0) {
            Duration(mbb.mWaitTime).sleep();
            mbb.mWaitTime = 0;
        }
        setState(Success);
        return getState();
    }

private:
    const double mDelay;

};
#endif
