/*
 * StartupStance.cpp
 *
 *  Created on: Mar 16, 2017
 *      Author: rmerriam
 */

#include <cmath>

#include <mystic.h>
#include <Walk.h>
#include <WalkMotion.h>

//#include "walk_params.h"
#include "Move.h"
//---------------------------------------------------------------------------------------------------------------------
Move::Move(const bool climb, const string& name, const int& num_steps) :
		RunningNodeAbstract { name, 0.0 }, mNumStairs { num_steps }, mClimbStairs { climb } {
}
//---------------------------------------------------------------------------------------------------------------------
Move::Move(const string& name, const double& distance) :
		RunningNodeAbstract { name, 0.0 }, mDistance { distance } {
}
//---------------------------------------------------------------------------------------------------------------------
Move::Move(const string& name, const bool use_turn_distance, const bool backup) :
		RunningNodeAbstract { name, 0.0 }, mDistance { mys::no_value }, mUseTurnDistance { use_turn_distance }, mBackup(backup) {
}
//---------------------------------------------------------------------------------------------------------------------
inline bool Move::absLessThan(const double& distance, const double& step) const {
	return abs(distance) < abs(step);
}
//---------------------------------------------------------------------------------------------------------------------
double Move::determineDistance(double distance, MarsBlackboard& mbb) {

	if (std::isnan(mDistance)) {
		distance = mUseTurnDistance ? mbb.mDistanceToTurn.load() : mbb.mDistanceToObject.load();
	} else {
		distance = mDistance;
	}

	if (mBackup) {
		distance = -distance;
	}

	return distance;
}
//---------------------------------------------------------------------------------------------------------------------
void Move::move(double distance, Walk& walk) {
	// right foot is half-step in front

	if (absLessThan(distance, mStep)) {     //  can finish both feet
		walk.incStep(mys::eLeft, Vec3 { distance, 0, 0 });
		walk.incStep(mys::eRight, Vec3 { distance - mHalfStep, 0, 0 });
	} else if (absLessThan(distance, mHalfStep + mStep)) {    // beyond right foot but not left
		walk.incStep(mys::eLeft, Vec3 { mStep, 0, 0 });
		walk.incStep(mys::eRight, Vec3 { distance - mHalfStep, 0, 0 });
		if (distance != mStep) {
			walk.incStep(mys::eLeft, Vec3 { distance - mStep, 0, 0 });
		}
	} else {      // distance more than a step
		walk.incStep(mys::eLeft, Vec3 { mStep, 0, 0 });
		walk.incStep(mys::eRight, Vec3 { mStep, 0, 0 });
		move(distance - mStep, walk);
	}
}
//---------------------------------------------------------------------------------------------------------------------
void Move::nodeRunning(Blackboard& bb) {
	MarsBlackboard& mbb = MarsBlackboard::getMarsBB(bb);

	WalkMotion& move = mbb.walk_motion();

	if (move.isStanding()) {
		if (mClimbStairs) {
			doStairsRunning(mbb);
		} else {
			setState(Success);
		}
	} else {
		setState(Running);
	}
}
//---------------------------------------------------------------------------------------------------------------------
void Move::nodeNotRunning(Blackboard& bb) {
	MarsBlackboard& mbb = MarsBlackboard::getMarsBB(bb);

	if (mClimbStairs) {
		doSteps(mbb);
	} else {
		double distance;

		distance = determineDistance(distance, mbb);

		if (abs(distance) < 0.01) {
			setState(Success);
		} else {

			mStep = {(distance < 0.0) ? mStrideReverse : mStrideForward};
			mHalfStep = mStep / 2;

			Walk& walk = mbb.walk();

			walk.startSteps();

			// always start with feet together
			if (absLessThan(distance, mHalfStep)) {    // can step this distance with both feet
				walk.incStep(mys::eRight, Vec3 {distance, 0, 0});
				walk.incStep(mys::eLeft, Vec3 {distance, 0, 0});
			}
			else if (absLessThan(distance, mStep)) {     // less than one step - half step, distance step, distance - half

				walk.incStep(mys::eRight, Vec3 {mHalfStep, 0, 0});
				walk.incStep(mys::eLeft, Vec3 {distance, 0, 0});

				if (distance != mHalfStep) {
					walk.incStep(mys::eRight, Vec3 {distance - mHalfStep, 0, 0});
				}
			}
			else {  // distance more than a step - move right half-step and use recursion to walk distance
				walk.incStep(mys::eRight, Vec3 {mHalfStep, 0, 0});
				move(distance, walk);
			}

			walk.doSteps();
			setState(Running);
		}
	}
}
//---------------------------------------------------------------------------------------------------------------------
void Move::doStairsRunning(MarsBlackboard& mbb) {
	Walk& walk = mbb.walk();

	walk.startSteps();

	double r_x { walk.getLocation(mys::eRight).getX() };
	double l_x { walk.getLocation(mys::eLeft).getX() };

	cerr << " offset ";
	mys::display(cerr, r_x - l_x) << mys::nl;

	walk.incStep(mys::eLeft, (r_x - l_x) * .99, 0, 0.15);
	walk.incStep(mys::eRight, stair_depth * 1.2, 0, 0.15);
	//        walk.incStep(mys::eLeft, stair_depth * 0.2, 0, -0.15);
	walk.doSteps();

	--mStairCnt;
	if (mStairCnt < 1) {
		walk.incStep(mys::eLeft, stair_depth * 1.2, 0, 0.15);
		setState(Success);
	} else {
		setState(Running);
	}
}
/*---------------------------------------------------------------------------------------------------------------------
 *
 *  Specialized version of move just for doing the steps:
 *      Steps are 0.21 m high, 0.24 m deep
 *      Bottom step is 3/8" higher
 *      Top step is 1" shorter
 *      There are 8 steps plus short 1st step of about .10m high
 *
 *  Assumption is R5 is within a half-step of the base of stairs
 *
 */
void Move::doSteps(MarsBlackboard& mbb) {
	Walk& walk = mbb.walk();

	walk.startSteps();

	walk.incStep(mys::eRight, stair_depth * 1.5, 0, 0.1);
	walk.incStep(mys::eLeft, stair_depth * 1.1, 0, 0.15);
	walk.incStep(mys::eLeft, stair_depth * 0.2, 0, -0.15);
	walk.incStep(mys::eRight, stair_depth * 1.1, 0, 0.15);

	walk.doSteps();
	mIsFirstStair = false;
	mStairCnt = mNumStairs - 2;
	setState(Running);
}
