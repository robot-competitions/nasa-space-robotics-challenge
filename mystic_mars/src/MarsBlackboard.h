/*
 * MarsBlackboard.h
 *
 *  Created on: Mar 16, 2017
 *      Author: rmerriam
 */

#ifndef MARSBLACKBOARD_H_
#define MARSBLACKBOARD_H_

#include <Blackboard.h>
#include <atomic>

#include <RPY.h>

class FootstepStatus;
class GoHome;
class HarnessStatus;
class IsDoubleSupport;
class ImageVision;
class JointStatus;
class PointCloud2Sub;
class RobotPose;
class TaskStatus;
class WalkMotion;

class ArmTraj;
class ChestTraj;
class HandTraj;
class NeckTraj;
class PelvisHeightTraj;
class Walk;
class WalkStop;
using atomic_double = atomic<double>;

using atomic_rpy = atomic<RPY>;
//--------------------------------------------------------------------------------------------------------------------------
enum ObjectId {
    eConsole, eBlueWheel, eRedWheel,
    eRedButton, ePath,
    eButton, eSolarBox, eSolarPanel, eBlack, eWhite,
    eSteps, eDoor, eLeftRail, eRightRail,
    eBorder, eScanDistance,
    eMaxObjId = eBorder
};
//--------------------------------------------------------------------------------------------------------------------------
class MarsBlackboard : public BT::Blackboard {
public:
    MarsBlackboard();
    virtual ~MarsBlackboard() = default;

    static MarsBlackboard& getMarsBB(BT::Blackboard& bb);

    // subscribers - update BlackboardStart node
    FootstepStatus& foot_status() const;
    HarnessStatus& harness_status() const;
    IsDoubleSupport& double_support() const;
    JointStatus& joint_status() const;
    PointCloud2Sub& point_cloud() const;
    RobotPose& robot_pose() const;
    TaskStatus& task_status() const;
    WalkMotion& walk_motion() const;

    // publishers
    ArmTraj& larm_traj() const;
    ArmTraj& rarm_traj() const;
    ChestTraj& chest_traj() const;
    GoHome& go_home() const;
    HandTraj& lhand_traj() const;
    HandTraj& rhand_traj() const;
    NeckTraj& neck_traj() const;
    PelvisHeightTraj& raise_pelvis() const;
    Walk& walk() const;
    WalkStop& walk_stop() const;

    // working data
    atomic_double mWaitTime { };

    // walking information
    atomic_double mDistanceToTurn { };
    atomic_double mDistanceToObject { };
    atomic_double mTurnAngle { };

    // pose information
    atomic_rpy mChestPose;
    atomic_rpy mNeckPose;

// path scan distance
    atomic_double mScanDistance { };
    atomic_double mScanAngle { };

    //  world information
    atomic_rpy mObjectDistances[eMaxObjId];
    atomic_rpy mVisonLocations[eMaxObjId];

    // constants
    double mPelvisHeight { 1.05 };
    ros::Rate mRate { 10 };

private:
    FootstepStatus& mFootstepStatus;
    IsDoubleSupport& mIsDoubleSupport;
    HarnessStatus& mHarnessStatus;
    JointStatus& mJointStatus;
    PointCloud2Sub& mPointCloud2Sub;
    RobotPose& mRobotPose;
    TaskStatus& mTaskStatus;
    WalkMotion& mWalkMotion;

    ArmTraj& mLArmTraj;
    ArmTraj& mRArmTraj;
    ChestTraj& mChestTraj;
    HandTraj& mLHandTraj;
    HandTraj& mRHandTraj;
    GoHome& mGoHome;
    NeckTraj& mNeckTraj;
    PelvisHeightTraj& mRaisePelvis;
    Walk& mWalk;
    WalkStop& mWalkStop;

};
//==========================================================================================================================
inline MarsBlackboard& MarsBlackboard::getMarsBB(BT::Blackboard& bb) {
    return dynamic_cast<MarsBlackboard&>(bb);
}
//--------------------------------------------------------------------------------------------------------------------------
inline FootstepStatus& MarsBlackboard::foot_status() const {
    return mFootstepStatus;
}
//--------------------------------------------------------------------------------------------------------------------------
inline IsDoubleSupport& MarsBlackboard::double_support() const {
    return mIsDoubleSupport;
}
//--------------------------------------------------------------------------------------------------------------------------
inline JointStatus& MarsBlackboard::joint_status() const {
    return mJointStatus;
}
//--------------------------------------------------------------------------------------------------------------------------
inline PointCloud2Sub& MarsBlackboard::point_cloud() const {
    return mPointCloud2Sub;
}
//--------------------------------------------------------------------------------------------------------------------------
inline RobotPose& MarsBlackboard::robot_pose() const {
    return mRobotPose;
}
//--------------------------------------------------------------------------------------------------------------------------
inline TaskStatus& MarsBlackboard::task_status() const {
    return mTaskStatus;
}
//--------------------------------------------------------------------------------------------------------------------------
inline WalkMotion& MarsBlackboard::walk_motion() const {
    return mWalkMotion;
}
//==========================================================================================================================
inline ChestTraj& MarsBlackboard::chest_traj() const {
    return mChestTraj;
}
//--------------------------------------------------------------------------------------------------------------------------
inline ArmTraj& MarsBlackboard::larm_traj() const {
    return mLArmTraj;
}
//--------------------------------------------------------------------------------------------------------------------------
inline ArmTraj& MarsBlackboard::rarm_traj() const {
    return mRArmTraj;
}
//--------------------------------------------------------------------------------------------------------------------------
inline HandTraj& MarsBlackboard::lhand_traj() const {
    return mLHandTraj;
}
//--------------------------------------------------------------------------------------------------------------------------
inline HandTraj& MarsBlackboard::rhand_traj() const {
    return mRHandTraj;
}
//--------------------------------------------------------------------------------------------------------------------------
inline GoHome& MarsBlackboard::go_home() const {
    return mGoHome;;
}
//--------------------------------------------------------------------------------------------------------------------------
inline HarnessStatus& MarsBlackboard::harness_status() const {
    return mHarnessStatus;
}
//--------------------------------------------------------------------------------------------------------------------------
inline NeckTraj& MarsBlackboard::neck_traj() const {
    return mNeckTraj;
}
//--------------------------------------------------------------------------------------------------------------------------
inline PelvisHeightTraj& MarsBlackboard::raise_pelvis() const {
    return mRaisePelvis;
}
//--------------------------------------------------------------------------------------------------------------------------
inline Walk& MarsBlackboard::walk() const {
    return mWalk;
}
//--------------------------------------------------------------------------------------------------------------------------
inline WalkStop& MarsBlackboard::walk_stop() const {
    return mWalkStop;
}
#endif /* MARSBLACKBOARD_H_ */
