/*
 * MarsBlackboard.cpp
 *
 *  Created on: Mar 16, 2017
 *      Author: rmerriam
 */
#include <mystic.h>

#include "publishers.h"
#include "subscribers.h"

#include "MarsBlackboard.h"
//--------------------------------------------------------------------------------------------------------------------------
MarsBlackboard::MarsBlackboard() :
    Blackboard(),
        mFootstepStatus( *new FootstepStatus),
        mIsDoubleSupport( *new IsDoubleSupport),
        mHarnessStatus( *new HarnessStatus),
        mJointStatus( *new JointStatus),
        mPointCloud2Sub( *new PointCloud2Sub),
        mRobotPose( *new RobotPose),
        mTaskStatus( *new TaskStatus),
        mWalkMotion( *new WalkMotion),
        //
        mLArmTraj( *new ArmTraj(mys::eLeft)),
        mRArmTraj( *new ArmTraj(mys::eRight)),
        mChestTraj( *new ChestTraj),
        mLHandTraj( *new HandTraj(mys::eLeft)),
        mRHandTraj( *new HandTraj(mys::eRight)),
        mGoHome( *new GoHome),
        mNeckTraj( *new NeckTraj),
        mRaisePelvis( *new PelvisHeightTraj),
        mWalk( *new Walk),
        mWalkStop( *new WalkStop)
//
{
    ROS_DEBUG_STREAM(ros::this_node::getName() << " MarsBlackboard ready");
}

