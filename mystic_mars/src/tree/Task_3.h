/*

 * Task_3_1.h
 *
 *  Created on: Apr 30, 2017
 *      Author: rmerriam
 */

#ifndef Task_3_H_
#define Task_3_H_

#include "Task_3/Task_3_1.h"
#include "Task_3/Task_3_2.h"
#include "Task_3/Task_3_3.h"
#include "Task_3/Task_3_4.h"
#include "Task_3/Task_3_5.h"
#include "Task_3/Task_3_6.h"
#include "Task_3/Task_3_7.h"
#include "Task_3/Task_3_8.h"

//--------------------------------------------------------------------------------------------------------------------------
struct Task_3 {

    MemSequenceNode node { "Task_3",
        {
            *new SelectorNode { "task_skipper",
                {
                    harness_active,
                    check_pt_3_2_done,
                    *new MemSequenceNode { "task startup",
                        {
                            start_task_3_3,
                            wait_harness_active
                        }
                    },
                }
            },
            //            task_3_1.node,      //   3   1   Climb stairs       Robot is teleported to be on top of the stairs, in front of the door
//            task_3_2.node,      //   3   2   Open door          Robot is teleported to be in front of the door, door opens
            task_3_3.node, //   3   3   Pass through door  Robot is teleported to be after the door
//            task_3_4.node,      //   3   4   Lift detector      This checkpoint can't be skipped by itself
//            task_3_5.node,      //   3   5   Find the leak      Robot is teleported to be after the door
//            task_3_6.node,      //   3   6   Lift repair tool   This checkpoint can't be skipped by itself
//            task_3_7.node,      //   3   7   Repair leak        Robot is teleported to be after the door
//            task_3_8.node,      //   3   8   Go to 3rd finish box  Program ends
        }
    };

private:

//    t3_1::Task_3_1 task_3_1;
//    t3_2::Task_3_2 task_3_2;
    t3_3::Task_3_3 task_3_3;
    //    t3_4::Task_3_4 task_3_4;
//    t3_5::Task_3_5 task_3_5;
//    t3_6::Task_3_6 task_3_6;
//    t3_7::Task_3_7 task_3_7;
    t3_8::Task_3_8 task_3_8;

    IsHarnessActive harness_active { "harness_active_" };
    IsHarnessActive harness_active2 { "harness_active_" };
    IsCheckpointDone check_pt_3_2_done { "check_3_2", 3, 2 };
    SetTaskNode start_task_3_3 { "start_task_3_3", 3, 3 };
    Inverter wait_harness_active { "wait_harness_active", *new IsHarnessActive { "harness_started after skip" } };
};

#endif /* Task_3_H_ */
