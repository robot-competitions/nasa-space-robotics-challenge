/*
 * Testing.h
 *
 *  Created on: Apr 30, 2017
 *      Author: rmerriam
 */

#ifndef Testing_H_
#define Testing_H_

//--------------------------------------------------------------------------------------------------------------------------
struct Testing {

    MemSequenceNode node { "testing details",
        {
            initialize.node,
            //            walk_some,
            face_90,
            //            face_45,

            move1,

            move2,
            move3,
            move4,
            move5,
        }
    };

private:
    RPY pos { 0.0, -0.2, -0.0 };
    RPY pos2 { 0.05, -0.0, -0.0 };
    RPY pos3 { -0.0, -0.05, -0.0 };


    RPY rot { 0.0, -0.0, 0.0 };

    HandMove move0 { "move0", mys::eRight, RPY { 0, 0, 0 }, rot };
    HandMove move1 { "move1", mys::eRight, pos, rot };

    HandMove move2 { "move2", mys::eRight, pos2, rot };
    HandMove move3 { "move3", mys::eRight, pos2, rot };

    HandMove move4 { "move4", mys::eRight, pos3, rot };
    HandMove move5 { "move5", mys::eRight, pos3, rot };


    Move walk_some { "walk_some", 0.15 };
    Move walk_more { "walk_more", 0.15 };

    Turn face_45 { "turn to 45", deg2rad(45.0) };
    Turn face_90 { "turn to 90", deg2rad( -90.0) };

    Initialize initialize;
    LookDown look_down { 20, 15 };

};

#endif /* Testing_H_ */
