/*
 * Task_1_1.h
 *
 *  Created on: Apr 30, 2017
 *      Author: rmerriam
 */

#ifndef Task_1_H_
#define Task_1_H_

#include "Task_1/Task_1_1.h"
#include "Task_1/Task_1_2.h"
#include "Task_1/Task_1_3.h"
#include "Task_1/Task_1_4.h"
//--------------------------------------------------------------------------------------------------------------------------
struct Task_1 {

    MemSequenceNode node { "task_1",
        {
            task_1_1.node,

            *new SelectorNode { "task_skipper",
                {
                    harness_active,
                    check_pt_1_3_done,
                    *new MemSequenceNode { "task startup",
                        {
                            start_task_1_4,
                            wait_harness_active
                        }
                    },
                }
            },
            //            task_1_2.node,
//            task_1_3.node,
            task_1_4.node,
        }
    };
    private:
    t1_1::Task_1_1 task_1_1;
    //    t1_2::Task_1_2 task_1_2;
//    t1_3::Task_1_3 task_1_3;
    t1_4::Task_1_4 task_1_4;

    IsHarnessActive harness_active { "harness_active_" };
    IsCheckpointDone check_pt_1_3_done { "check_1_3", 1, 3 };
    SetTaskNode start_task_1_4 { "start_task_1_4", 1, 4 };
    Inverter wait_harness_active { "wait_harness_active", *new IsHarnessActive { "harness_started after skip to 1_4" } };

};

#endif /* Task_1_H_ */
