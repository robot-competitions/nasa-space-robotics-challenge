/*
 * TreeRoot.h
 *
 *  Created on: Apr 30, 2017
 *      Author: rmerriam
 */

#ifndef TREEROOT_H_
#define TREEROOT_H_

#include <mystic.h>

#include "../nodes.h"

#include "UtilNodes.h"
#include "Task_1.h"
#include "Task_2.h"
#include "Task_3.h"
#include "TreeRoot.h"
#include "Testing.h"
using namespace BT;
//--------------------------------------------------------------------------------------------------------------------------
struct TreeRoot {

#if 0

    MemSequenceNode node {"===== root =====",
        {
            task_1.node,
            task_2.node,
            task_3.node,
        }
    };
private:

    Task_1 task_1;
    Task_2 task_2;
    Task_3 task_3;

#else

    MemSequenceNode node { "===== testing root =====",
        {
            //            initialize.node,
            //            home_all.node,
            testing.node,
        },
    };
    private:
    HomeAll home_all;
    Testing testing;

#endif
private:

    Initialize initialize;
    IsHarnessActive harness_active { "harness_active" };
};

#endif /* TREEROOT_H_ */
