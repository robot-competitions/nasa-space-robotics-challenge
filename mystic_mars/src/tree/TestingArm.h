/*
 * Testing.h
 *
 *  Created on: Apr 30, 2017
 *      Author: rmerriam
 */

#ifndef Testing_H_
#define Testing_H_
//         eShoulderRotate, eShoulderJoint, eUpperArmRotate, eElbowJointRotate, eLowerArmRotate, eWristX, eWristY

const double ShoulderRotHome { deg2rad( -11.0) };   // -163:115
const double ShoulderRotNeutral { deg2rad(0.0) };
const double ShoulderRot90Down { deg2rad(90.0) };
const double ShoulderRot114Down { deg2rad(114.0) };
const double ShoulderRot90Up { deg2rad( -90.0) };
const double ShoulderRot45Up { deg2rad( -45.0) };

const double ShoulderJointHome { deg2rad(69.0) };   // -73:87
const double ShoulderJointNeutral { deg2rad(0.0) }; // mid with rotate joint
const double ShoulderJointCloseIn { deg2rad(87.0) };
const double ShoulderJointRaised { deg2rad(0.0) };
const double ShoulderJointBackward { deg2rad(85.0) };       // with rotate joint '90down"
const double ShoulderJointForeward { deg2rad( -72.0) };     // with rotate joint '90down"

const double UpperRotHome { deg2rad(40.0) };        // -177:124.9
const double UpperRotNeutral { deg2rad(30.0) };
const double UpperRot90 { deg2rad(0.0) };
const double UpperRot90CCW { deg2rad( -90.0) };
const double UpperRot90CW { deg2rad(90.0) };
const double UpperRotMax { deg2rad(124.0) };

const double ElbowRotHome { deg2rad(86.0) };    //  -6.88:125
const double ElbowRot90 { deg2rad(90.0) };      // 90 degrees to upper arm
const double ElbowRot45 { deg2rad(45.0) };      // 90 degrees to upper arm
const double ElbowRot0 { deg2rad(0.0) };      // 0 degrees to upper arm
const double ElbowRotBack { deg2rad( -6.0) };      // 0 degrees to upper arm

const double LowerRotHome { deg2rad(75.0) };    //  -115:180
const double LowerRotNeutral { deg2rad(0.0) };
const double LowerRot90 { deg2rad( -110.0) };
const double LowerRot90Rev { deg2rad(170.0) };

const double WristFlexHome { deg2rad(0.0) };    // -36:36
const double WristFlexNeutral { deg2rad(0.0) };

const double WristFlapHome { deg2rad(0.0) };    // -28:21
const double WristFlapNeutral { deg2rad(0.0) };

//--------------------------------------------------------------------------------------------------------------------------
struct Testing {

    MemSequenceNode node { "testing details",
        {
            initialize.node,
            look_down.node,
            pelvis_init_height,
            r_arm,
            pelvis_wheel_height,
            r_spin,
        }
    };

private:

    ArmPositions test_pos {
        {
            ShoulderRot45Up + deg2rad( -25.0),
            ShoulderJointCloseIn + deg2rad( -7.00),
            UpperRot90CW + deg2rad(0.0),
            ElbowRot0 + deg2rad(0.0),
            LowerRotNeutral,
            WristFlexNeutral,
            WristFlapNeutral
        }
    };

    ArmPositions test_spin {
        {
            ShoulderRot45Up + deg2rad( -25.0),
            ShoulderJointCloseIn + deg2rad( -9.0),
            UpperRot90CCW + deg2rad(0.0),
            ElbowRot0 + deg2rad(0.0),
            LowerRotNeutral,
            WristFlexNeutral,
            WristFlapNeutral
        }
    };

    ArmMove r_arm { "testing r arm", mys::eRight, test_pos, 1.0 };
    ArmMove r_spin { "testing r spin", mys::eRight, test_spin, 1.0 };
    SetPelvisHeight pelvis_init_height { "pelvis init height", 1.05, 1.5 };
    SetPelvisHeight pelvis_wheel_height { "pelvis wheel height", 0.95, 1.5 };

    Initialize initialize;
    LookDown look_down { 20, 15 };

    ArmPositions home_pos = {
        {
            ShoulderRotHome,
            ShoulderJointHome,
            UpperRotHome,
            ElbowRotHome,
            LowerRotHome,
            WristFlexHome,
            WristFlapHome
        }
    };
};

#endif /* Testing_H_ */
