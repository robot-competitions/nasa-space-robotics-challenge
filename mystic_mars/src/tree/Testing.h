/*
 * Testing.h
 *
 *  Created on: Apr 30, 2017
 *      Author: rmerriam
 */

#ifndef Testing_H_
#define Testing_H_
/*
 *  Steps are 0.21 m high, 0.24 m deep
 *  Bottom step is 3/8" higher
 *  Top step is 1" shorter
 *  There are 8 steps plus short 1st step of about .10m high
 */
//--------------------------------------------------------------------------------------------------------------------------
struct Testing {

    MemSequenceNode node { "testing details",
        {
            initialize.node,
            chest_pose_15,
            align45,

            *new MemSequenceNode { "inner_climb",
                {
                    climb_stairs,
                }
            },
            stance,
            walk_some,
        }
    };

    Move climb_stairs { true, "climb_stairs", 9 };
    SetChestPose chest_pose_15 { "chest init pose", 0.25, RPY { 0, deg2rad(15.0), 0 } };
    Turn stance { "even up", Turn::eStance };
    SetPelvisHeight pelvis_stair_height { "pelvis_stair_height", 1.1, 0.25 };

    Move walk_some { "walk_some", 1.0 };
    Move walk_more { "walk_more", 0.15 };

    Initialize initialize;
    LookDown look_down { 20, 15 };
    Turn align45 { "align45", Turn::eAlign };

};

#endif /* Testing_H_ */
