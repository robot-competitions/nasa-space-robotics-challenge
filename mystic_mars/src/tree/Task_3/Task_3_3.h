/*
 * Task_3_3.h
 *
 *  Created on: Apr 30, 2017
 *      Author: rmerriam
 */

#ifndef Task_3_3_H_
#define Task_3_3_H_

namespace t3_3 {
    const string task_name { "task_3_3" };
//--------------------------------------------------------------------------------------------------------------------------
    struct Task_3_3 {

        SelectorNode node { task_name,
            {
                harness_active,
                check_pt_3_3_done,
                //                stop_checkpoint_done,
                *new MemSequenceNode { "memsequence_" + task_name,
                    {
                        ready,
                        stabilize,
                        initialize.node,
                        *new Announce { task_name, " walk through door" },
                        stance,
                        align45,

                        *new Announce { task_name, " walk to door " },
                        lean_in,
                        walk_to_door,
                        to_25,
                        walk_through_door,
                        //                        walk_through_door2,
                        to_65,
                        walk_into_hab,
                        start_task_3_8,
                        finished,
                    }
                },
            }
        };

    private:
        Move walk_to_door { "walk_to_door", 1.6 };
        SetChestPose lean_in { "lean_in", 0, RPY { 0, deg2rad(15.0), 0 } };
        Turn to_25 { "to 25", deg2rad( -20.0) };
        Move walk_through_door { "walk_through_door", 1.5 };
        Turn stabilize { "Stabilize", Turn::eStance };
        Turn align45 { "align45", Turn::eAlign };

//        Move walk_through_door2 { "walk_through_door", .7 };
        Turn to_65 { "to 65", deg2rad( -70.0) };
        Move walk_into_hab { "walk_into_hab", 1.75 };

        Turn stance { "even up", Turn::eStance };

        IsHarnessActive harness_active { "harness_active_" + task_name };
        IsCheckpointDone check_pt_3_3_done { "check_" + task_name, 3, 3 };

        Initialize initialize;

        LookDown look_down { 20, 20 };
        DoorDistance door_distance { 0.6 };
        //        Move walk_to_door { "walk_to_door" };

        Announce finished { task_name, "<<=== finished " };
        Announce ready { task_name, "<<=== ready " };

        WalkingStop stop_checkpoint_done { "stop_checkpoint_done " + task_name, check_pt_3_3_done };

        SetTaskNode start_task_3_8 { "start task 3/8 ", 3, 8 };
//        SkipPrep skip_prep;
//        Inverter wait_harness_active { "wait_harness_active", *new IsHarnessActive { "harness_started after skip to 1_4" } };
    };
}   // end namespace
#endif /* Task_3_3_H_ */
