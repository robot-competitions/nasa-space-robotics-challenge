/*
 * Task_3_1.h
 *
 *  Created on: Apr 30, 2017
 *      Author: rmerriam
 */

#ifndef Task_3_1_H_
#define Task_3_1_H_

namespace t3_1 {
    const string task_name = { "task_3_1" };

    //--------------------------------------------------------------------------------------------------------------------------
    struct Task_3_1 {

        SelectorNode node { task_name,
            {
                harness_active,
                check_pt_3_1_done,
                *new MemSequenceNode { "memsequence_" + task_name,
                    {
                        ready,
                        initialize.node,
                        //                        console_distance.node,
                        *new Announce { task_name, " start leave start box " },
                        leave_start,
                        finished,
                    }
                },
            }
        };
        private:

        IsHarnessActive harness_active { "harness_active_" + task_name };
        IsCheckpointDone check_pt_3_1_done { "check_" + task_name, 3, 1 };
        Initialize initialize;
        Move leave_start { "leave_start", true };
        //        ConsoleDistance console_distance;
        Announce finished { task_name, "<<=== finished " };
        Announce ready { task_name, "<<=== ready " };
    };
}
#endif /* Task_3_1_H_ */
