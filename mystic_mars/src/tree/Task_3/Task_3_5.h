/*
 * task_3_5.h
 *
 *  Created on: Apr 30, 2017
 *      Author: rmerriam
 */

#ifndef task_3_5_H_
#define task_3_5_H_

namespace t3_5 {
    const string task_name = { "task_3_5" };
//--------------------------------------------------------------------------------------------------------------------------
    struct Task_3_5 {
        SelectorNode node { task_name,
            {
                harness_active,
                check_pt_3_5_done,
                *new MemSequenceNode { "memsequence_" + task_name,
                    {
                        ready,
                        //                        initialize.node,
                        *new Announce { task_name, " open door " },

                        *new Announce { task_name, " jump to 3.6 " },
                        skip_prep.node,
                        start_task,
                        wait_harness_active,
                        finished,
                    }
                },
            }
        };

    private:
        IsHarnessActive harness_active { "harness_active_" + task_name };
        Initialize initialize;

        IsCheckpointDone check_pt_3_5_done { "check_" + task_name, 3, 5 };

        Announce finished { task_name, "<<=== finished " };
        Announce ready { task_name, "<<=== ready " };

        SkipPrep skip_prep;
        SetTaskNode start_task { "start task 3/6 ", 3, 6 };
        Inverter wait_harness_active { "wait_harness_active", *new IsHarnessActive { "harness_started after skip to 3_6" } };
    };
}

#endif /* task_3_5_H_ */
