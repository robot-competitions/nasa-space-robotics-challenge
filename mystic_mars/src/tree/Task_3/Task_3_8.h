/*
 * task_3_8.h
 *
 *  Created on: Apr 30, 2017
 *      Author: rmerriam
 */

#ifndef task_3_8_H_
#define task_3_8_H_

namespace t3_8 {
    const string task_name = { "task_3_8" };
//--------------------------------------------------------------------------------------------------------------------------
    struct Task_3_8 {
        SelectorNode node { task_name,
            {
                //                harness_active,
                //                check_pt_3_8_done,
                *new MemSequenceNode { "memsequence_" + task_name,
                    {
                    //                        ready,
//                        //                        initialize.node,
//                        *new Announce { task_name, " open door " },
//
//                        *new Announce { task_name, " jump to 3.3 " },
//                        skip_prep.node,
//                        start_task_3_3,
//                        wait_harness_active,
//                        finished,
                    }
                },
            }
        };

    private:
        IsHarnessActive harness_active { "harness_active_" + task_name };
        Initialize initialize;

        IsCheckpointDone check_pt_3_8_done { "check_" + task_name, 3, 2 };

        Announce finished { task_name, "<<=== finished " };
        Announce ready { task_name, "<<=== ready " };

        SkipPrep skip_prep;
        SetTaskNode start_task_3_3 { "start task 2/3 ", 3, 3 };
        Inverter wait_harness_active { "wait_harness_active", *new IsHarnessActive { "harness_started after skip to 3_3" } };
    };
}

#endif /* task_3_8_H_ */
