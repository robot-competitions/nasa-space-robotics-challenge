/*
 * Initialize.h
 *
 *  Created on: Apr 30, 2017
 *      Author: rmerriam
 */

#ifndef INITIALIZE_H_
#define INITIALIZE_H_
//--------------------------------------------------------------------------------------------------------------------------
struct Initialize {
    MemSequenceNode node { "initialize",
        {
            move_done,
            home_all.node,
            pelvis_init_height,
            stance,
            chest_pose_10,
            stance2,
        }
    };
    private:

    IsMovementDone move_done { "move_done" };
    HomeAll home_all;
    SetPelvisHeight pelvis_init_height { "pelvis init height" };
    Turn stance { "even up", Turn::eStance };
    Turn stance2 { "even up", Turn::eStance };
    SetChestPose chest_pose_10 { "chest init pose", 0.25, RPY { 0, deg2rad(10.0), 0 } };
};

#endif /* INITIALIZE_H_ */
