/*
 * LookDown.h
 *
 *  Created on: May 11, 2017
 *      Author: rmerriam
 */

#ifndef LookDown_H_
#define LookDown_H_

struct LookDown {
    LookDown(const double& neck_pitch, const double& chest_pitch, const double& delay = 0.1) :
        mNeckPose { "neck look down", RPY { deg2rad(neck_pitch), 0, 0 } },
            mChestPose { "chest look down", delay, RPY { 0, deg2rad(chest_pitch), 0 } }
    {

    }

    SequenceNode node { "look down",
        {
            mNeckPose,
            mChestPose,
        }
    };

private:
    SetNeckPose mNeckPose;
    SetChestPose mChestPose;
};

#endif /* LookDown_H_ */
