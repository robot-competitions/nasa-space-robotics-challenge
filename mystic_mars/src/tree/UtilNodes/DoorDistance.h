/*
 * DoorDistance.h
 *
 *  Created on: May 11, 2017
 *      Author: rmerriam
 */

#ifndef DoorDistance_H_
#define DoorDistance_H_

struct DoorDistance {
    DoorDistance(const double& adjust = mys::no_value) :
        mAdjust { adjust },
            calc_move_distance { "calc door distance", mAdjust } {
    }

    SequenceNode node { "get distance",
        {
            get_hab_door,
            calc_move_distance,
        }
    };

private:
    const double mAdjust;
    GetObjectByColor get_hab_door { "get red wheel", eDoor, gimp2cv(40, 0.03, 0.18), gimp2cv(60, 0.8, 0.24) };
    GetObjectByColor get_hab_thresh { "door jam", eDoor, gimp2cv(40, 0.03, 0.18), gimp2cv(60, 0.8, 0.24) };
    CalcSatConsolePose calc_move_distance;
};

#endif /* DoorDistance_H_ */
