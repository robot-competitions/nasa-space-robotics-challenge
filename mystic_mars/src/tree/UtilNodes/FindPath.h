/*
 * FindPath.h
 *
 *  Created on: May 18, 2017
 *      Author: rmerriam
 */

#ifndef FindPath_H_
#define FindPath_H_

///--------------------------------------------------------------------------------------------------------------------------
struct FindPath {

    MemSequenceNode node { "Neck chest movement",
        {
            home_all.node,
            look_down.node,
            scan_start,
            chest_left,

            *new LoopNode { " path scanning",
                {
                    scan_path,
                    chest_swing,
                }, 9
            },
        //            scan_update,
//            home_all2.node,
//            chest_pose_10,
//            turn,
//            walk_path,
        }
    };

private:
    HomeAll home_all;
    HomeAll home_all2;
    LookDown look_down { 15, 10, 0.5 };
    SetChestPose chest_left { "chest left", 0.5, RPY { 0, 0, deg2rad(60.0) } };
    SetChestPose chest_swing { "chest scan", 0.5, RPY { 0, 0, deg2rad( -15.0) } };
    ScanPathway scan_path { "scan path", ScanPathway::eProcess };
    ScanPathway scan_start { "scan reset", ScanPathway::eReset };
    ScanPathway scan_update { "scan update", ScanPathway::eUpdate };
    //    Turn turn { "turn to path" };
    Move walk_path { "walk_path", false };
    SetChestPose chest_pose_10 { "chest init pose", 0.25, RPY { 0, deg2rad(10.0), 0 } };

};

#endif /* FindPath_H_ */
