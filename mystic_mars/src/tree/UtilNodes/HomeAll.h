/*
 * HomeAll_1.h
 *
 *  Created on: Apr 30, 2017
 *      Author: rmerriam
 */

#ifndef HomeAll_H_
#define HomeAll_H_

//--------------------------------------------------------------------------------------------------------------------------
struct HomeAll {

    MemSequenceNode node { "reset / home all",
        {
            home_pelvis,
            home_chest,
            reset_neck,
            home_left_arm,
            home_right_arm,
        }
    };

private:
    SetHome home_chest { "home chest", mys::eLeft, GoHome::GoPart::eCHEST, 0 };
    SetHome home_left_arm { "home left arm", mys::eLeft, GoHome::GoPart::eARM, 0 };
    SetHome home_right_arm { "home right arm", mys::eRight, GoHome::GoPart::eARM, 0.25 };
    SetNeckPose reset_neck { "reset neck", 0, rpy_reset_pose };

    SetHome home_pelvis { "home pelvis", mys::eLeft, GoHome::GoPart::ePELVIS, 0.0 };
    SetPelvisHeight pelvis_init_height { "pelvis init height" };
};

#endif /* HomeAll_H_ */
