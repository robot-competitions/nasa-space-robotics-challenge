/*
 * WalkStopper.h
 *
 *  Created on: Apr 30, 2017
 *      Author: rmerriam
 */

#ifndef WalkStopper_H_
#define WalkStopper_H_

//--------------------------------------------------------------------------------------------------------------------------
struct WalkStopper {

    MemSequenceNode node { "WalkStopper details",
        {
            *new SucceedAlways { "walk succeed", leave_start },
            *new SequenceNode { "test stop",
                {
                    check_pt_1_1_done,
                //                    stop_walk,
                }
            },
        }
    };

private:

//    WalkingStop stop_walk { "stop_walk " };
    IsCheckpointDone check_pt_1_1_done { "check_", 1, 1 };
    Move leave_start { "leave_start", 2.5 };
};

#endif /* WalkStopper_H_ */
