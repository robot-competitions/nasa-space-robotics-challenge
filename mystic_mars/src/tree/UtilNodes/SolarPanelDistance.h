/*
 * SolarPanelDistance.h
 *
 *  Created on: May 11, 2017
 *      Author: rmerriam
 */

#ifndef SolarPanelDistance_H_
#define SolarPanelDistance_H_

struct SolarPanelDistance {
    SolarPanelDistance(const double& adjust = mys::no_value) :
        mAdjust { adjust },
            calc_move_distance { "calc solar panel distance", mAdjust } {
    }

    SequenceNode node { "get distance",
        {
            get_solar_panel,
            get_button_red,
            calc_move_distance,
        }
    };

private:
    const double mAdjust;

    GetObjectByColor get_button_red { "get_button_red", eButton, gimp2cv(0, 0.70, 0.25), gimp2cv(10, 1.0, 0.60) };
    GetObjectByColor get_solar_panel { "get_solar_panel", eSolarPanel, gimp2cv(200, 0.70, 0.10), gimp2cv(210, 0.9, 0.25) };
    //    GetObjectByColor get_solar_box { "get_solar_box", eSolarBox, gimp2cv(0, 0.0, 0.22), gimp2cv(0, 0.01, 0.24) };

    CalcSolarPanelPose calc_move_distance;
};

#endif /* SolarPanelDistance_H_ */
