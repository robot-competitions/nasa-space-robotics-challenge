/*
 * BackupTurn.h
 *
 *  Created on: May 18, 2017
 *      Author: rmerriam
 */

#ifndef BACKUPTURN_H_
#define BACKUPTURN_H_

//--------------------------------------------------------------------------------------------------------------------------
struct BackupTurn {
    BackupTurn(const double& distance = 0.0) :
    back_to_path {"back_to_path", distance} {}

    MemSequenceNode node { "back from position",
        {
            home_all.node,
            back_to_path,
            face_zero,
            stance
        }
    };

private:
    Move back_to_path;  // {"back_to_path", -0.30};
    Turn face_zero { "turn to zero", Turn::eZero };
    HomeAll home_all;
    Turn stance { "even up", Turn::eStance };

};

#endif /* BACKUPTURN_H_ */
