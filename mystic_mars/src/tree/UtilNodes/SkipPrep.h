/*
 * SkipPrep.h
 *
 *  Created on: Apr 30, 2017
 *      Author: rmerriam
 */

#ifndef SkipPrep_H_
#define SkipPrep_H_
//--------------------------------------------------------------------------------------------------------------------------
struct SkipPrep {
    MemSequenceNode node { "skip_prep",
        {
            move_done,
            home_all.node,
            pelvis_init_height,
            stance,
            chest_pose_10,
        }
    };
    private:

    IsMovementDone move_done { "move_done" };
    HomeAll home_all;
    SetPelvisHeight pelvis_init_height { "pelvis init height" };
    Turn stance { "even up", Turn::eStance };
    SetChestPose chest_pose_10 { "chest init pose", 0.25, RPY { 0, deg2rad(10.0), 0 } };
};

#endif /* SkipPrep_H_ */
