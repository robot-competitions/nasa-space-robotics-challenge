/*
 * TaskSkipper.h
 *
 *  Created on: May 12, 2017
 *      Author: rmerriam
 */

#ifndef TaskSkipper_H_
#define TaskSkipper_H_

namespace t_skip {
//    const string task_name { "TaskSkipper " };

//--------------------------------------------------------------------------------------------------------------------------
    struct TaskSkipper {
        TaskSkipper(ConditionNode& test, ActionNode& action) :
            mTest(test), mAction(action), mTestInvert("skip inverter", test) {
        }

        SelectorNode node { "task_skipper",
            {
                //                harness_active,
                mTest,
                //                mTestInvert,
                mAction,
            }
        };

    private:
        ConditionNode& mTest;
        ActionNode& mAction;
        Inverter mTestInvert;

//        IsHarnessActive harness_active { "harness_active_" + task_name };
//        Announce ready { task_name, "<<=== ready " };
    };
}
#endif /* TaskSkipper_H_ */
