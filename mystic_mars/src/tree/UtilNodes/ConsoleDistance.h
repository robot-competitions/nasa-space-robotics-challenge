/*
 * ConsoleDistance.h
 *
 *  Created on: May 11, 2017
 *      Author: rmerriam
 */

#ifndef CONSOLEDISTANCE_H_
#define CONSOLEDISTANCE_H_

struct ConsoleDistance {
    ConsoleDistance(const double& adjust = mys::no_value) :
        mAdjust { adjust },
        calc_move_distance { "calc wheel distance", mAdjust } {
    }

    SequenceNode node { "get distance",
        {
            get_wheel_red,
            get_wheel_blue,
            calc_move_distance,
        }
    };

private:
    const double mAdjust;
    GetObjectByColor get_wheel_red { "get red wheel", eRedWheel, gimp2cv(0, 0.70, 0.25), gimp2cv(10, 1.0, 0.60) };
    GetObjectByColor get_wheel_blue { "get blue wheel", eBlueWheel, gimp2cv(220, 0.35, 0.3), gimp2cv(255, 1.0, 0.50) };
    CalcSatConsolePose calc_move_distance;
};

#endif /* CONSOLEDISTANCE_H_ */
