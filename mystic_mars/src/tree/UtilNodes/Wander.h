/*
 * Wander_1.h
 *
 *  Created on: Apr 30, 2017
 *      Author: rmerriam
 */

#ifndef Wander_H_
#define Wander_H_

//--------------------------------------------------------------------------------------------------------------------------
struct Wander {
    LoopNode node { "wander loop",
        {
            *new SelectorNode { "wander", true,
                {
                    straight_ahead.node,
                    *new MemSequenceNode { "try 45 ",
                        {
                            *new Announce { "wander", "check 45 degrees " },
                            stance,
                            path_not_clear,
                            face_45,
                            ahead_45.node,
                        }
                    },
                    *new MemSequenceNode { "try 90 ",
                        {
                            *new Announce { "wander", "check 90 degrees " },
                            stance2,
                            path_not_clear,
                            face_90,
                            ahead_90.node,
                        }
                    },
                },
            },
        }, 20
    };

private:

    Turn face_45 { "turn to 45", deg2rad(45.0) };
    Turn face_90 { "turn to 90", deg2rad( -90.0) };

    ScanPathway scan_path { "scan_path", ScanPathway::eProcess, true };
    ScanPathway scan_path_45 { "scan_path_45", ScanPathway::eProcess, true };
    ScanPathway scan_path_90 { "scan_path_90", ScanPathway::eProcess, true };

    IsPathClear is_path_clear { "is_path_clear" };
    Inverter path_not_clear { "path_not_clear", is_path_clear };

    Turn stance { "even up", Turn::eStance };
    Turn stance2 { "even up", Turn::eStance };

    //--------------------------------------------------------------------------------------------------------------------------
    struct StraightAhead {

        MemSequenceNode node { "straight ahead",
            {
                *new Announce { "wander", "straight " },
                scan_path,
                is_path_clear,
                walk,
            }
        };
        private:

        ScanPathway scan_path { "scan path", ScanPathway::eProcess, true };

        IsPathClear is_path_clear { "is_path_clear" };
        Move walk { "walk", no_value };
    };

    StraightAhead straight_ahead;
    StraightAhead ahead_45;
    StraightAhead ahead_90;

};

#endif /* Wander_H_ */
