/*
 * task_2_1.h
 *
 *  Created on: Apr 30, 2017
 *      Author: rmerriam
 */

#ifndef task_2_1_H_
#define task_2_1_H_

namespace t2_1 {
    const string task_name = { "task_2_1" };

//--------------------------------------------------------------------------------------------------------------------------
    struct Task_2_1 {
        SelectorNode node { task_name,
            {
                harness_active,
                check_pt_2_1_done,
                *new MemSequenceNode { "memsequence_" + task_name,
                    {
                        ready,
                        initialize.node,
                        *new Announce { task_name, " start Lift solar panel " },

                        leave_start,
                        *new Announce { task_name, " jump to 2.2 " },
                        skip_prep.node,
                        start_task_2_2,
                        wait_harness_active,
                        finished,
                    }
                },
            }
        };

    private:
        IsHarnessActive harness_active { "harness_active_" + task_name };
        Initialize initialize;

        HomeAll home_all;
        Move leave_start { "leave_start", 1.8 };
        IsCheckpointDone check_pt_2_1_done { "check_" + task_name, 2, 1 };
        SolarPanelDistance panel_distance;

        Announce finished { task_name, "<<=== finished " };
        Announce ready { task_name, "<<=== ready " };

        SkipPrep skip_prep;
        SetTaskNode start_task_2_2 { "start task 2/2 ", 2, 2 };
        Inverter wait_harness_active { "wait_harness_active", *new IsHarnessActive { "harness_started after skip to 2_2" } };
    };
}
#endif /* task_2_1_H_ */
