/*
 * task_2_6.h
 *
 *  Created on: Apr 30, 2017
 *      Author: rmerriam
 */

#ifndef task_2_6_H_
#define task_2_6_H_

namespace t2_6 {
    const string task_name = { "task_2_6" };
//--------------------------------------------------------------------------------------------------------------------------
    struct Task_2_6 {
        SelectorNode node { task_name,
            {
                harness_active,
                //                check_pt_2_6_done,
                stop_checkpoint_done,
                *new MemSequenceNode { "memsequence_" + task_name,
                    {
                        ready,
                        initialize.node,
                        *new Announce { task_name, " Go to 2nd finish box " },

//                        stance,
                        backup_turn.node,
                        //                        stance2,
                        look_down.node,
                        *new Announce { task_name, " wander" },

                        *new SelectorNode { "wander to 2/6",
                            {
                                check_pt_2_6_done,
                                wander.node,
                            },
                        },
                        move_done,
                        finished
                    }
                },
            }
        };

    private:

        IsHarnessActive harness_active { "harness_active_" + task_name };
        Initialize initialize;
        LookDown look_down { 10, 10, 0.5 };

        BackupTurn backup_turn;

        Wander wander;
        IsCheckpointDone check_pt_2_6_done { "check_" + task_name, 2, 6 };
        IsMovementDone move_done { "move_done" };
        Turn stance { "stance", Turn::eStance };
        Turn stance2 { "stance2", Turn::eStance };

        Announce finished { task_name, "<<=== finished " };
        Announce ready { task_name, "<<=== ready " };

        WalkingStop stop_checkpoint_done { "stop_checkpoint_done " + task_name, check_pt_2_6_done };

    };
}
#endif /* task_2_6_H_ */
