/*
 * task_2_4.h
 *
 *  Created on: Apr 30, 2017
 *      Author: rmerriam
 */

#ifndef task_2_4_H_
#define task_2_4_H_

namespace t2_4 {
    const string task_name = { "task_2_4" };
//--------------------------------------------------------------------------------------------------------------------------
    struct Task_2_4 {
        SelectorNode node { task_name,
            {
                harness_active,
                check_pt_2_4_done,
                *new MemSequenceNode { "memsequence_" + task_name,
                    {
                        ready,
                        //                        initialize.node,
                        *new Announce { task_name, " Lift power cable " },

                        *new Announce { task_name, " jump to 2.5 " },
                        skip_prep.node,
                        start_task_2_5,
                        wait_harness_active,
                        finished,
                    }
                },
            }
        };

    private:
        IsHarnessActive harness_active { "harness_active_" + task_name };
        Initialize initialize;

        IsCheckpointDone check_pt_2_4_done { "check_" + task_name, 2, 4 };

        Announce finished { task_name, "<<=== finished " };
        Announce ready { task_name, "<<=== ready " };

        SkipPrep skip_prep;
        SetTaskNode start_task_2_5 { "start task 2/5 ", 2, 5 };
        Inverter wait_harness_active { "wait_harness_active", *new IsHarnessActive { "harness_started after skip to 1_4" } };
    };
}
#endif /* task_2_4_H_ */
