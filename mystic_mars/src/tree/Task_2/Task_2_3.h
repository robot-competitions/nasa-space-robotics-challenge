/*
 * task_2_3.h
 *
 *  Created on: Apr 30, 2017
 *      Author: rmerriam
 */

#ifndef task_2_3_H_
#define task_2_3_H_

namespace t2_3 {
    const string task_name = { "task_2_3" };
//--------------------------------------------------------------------------------------------------------------------------
    struct Task_2_3 {
        SelectorNode node { task_name,
            {
                harness_active,
                check_pt_2_3_done,
                *new MemSequenceNode { "memsequence_" + task_name,
                    {
                        ready,
                        //                        initialize.node,
                        *new Announce { task_name, " Press solar panel button " },

                        *new Announce { task_name, " jump to 2.4 " },
                        skip_prep.node,
                        start_task_2_4,
                        wait_harness_active,
                        finished,
                    }
                },
            }
        };

    private:
        IsHarnessActive harness_active { "harness_active_" + task_name };
        Initialize initialize;

        IsCheckpointDone check_pt_2_3_done { "check_" + task_name, 2, 3 };

        Announce finished { task_name, "<<=== finished " };
        Announce ready { task_name, "<<=== ready " };

        SkipPrep skip_prep;
        SetTaskNode start_task_2_4 { "start task 2/4 ", 2, 4 };
        Inverter wait_harness_active { "wait_harness_active", *new IsHarnessActive { "harness_started after skip to 2_4" } };
    };
}

#endif /* task_2_3_H_ */
