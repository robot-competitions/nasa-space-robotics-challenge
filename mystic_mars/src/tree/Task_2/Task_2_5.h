/*
 * task_2_5.h
 *
 *  Created on: Apr 30, 2017
 *      Author: rmerriam
 */

#ifndef task_2_5_H_
#define task_2_5_H_

namespace t2_5 {
    const string task_name = { "task_2_5" };
//--------------------------------------------------------------------------------------------------------------------------
    struct Task_2_5 {
        SelectorNode node { task_name,
            {
                harness_active,
                check_pt_2_5_done,
                *new MemSequenceNode { "memsequence_" + task_name,
                    {
                        ready,
                        //                        initialize.node,
                        *new Announce { task_name, " Plug power cable " },

                        *new Announce { task_name, " jump to 2.6 " },
                        skip_prep.node,
                        start_task_2_6,
                        wait_harness_active,
                        finished,
                    }
                },
            }
        };

    private:
        IsHarnessActive harness_active { "harness_active_" + task_name };
        Initialize initialize;

        IsCheckpointDone check_pt_2_5_done { "check_" + task_name, 2, 5 };

        Announce finished { task_name, "<<=== finished " };
        Announce ready { task_name, "<<=== ready " };

        SkipPrep skip_prep;
        SetTaskNode start_task_2_6 { "start task 2/5 ", 2, 6 };
        Inverter wait_harness_active { "wait_harness_active", *new IsHarnessActive { "harness_started after skip to 1_4" } };
    };
}
#endif /* task_2_5_H_ */
