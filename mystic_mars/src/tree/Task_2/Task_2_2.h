/*
 * task_2_2.h
 *
 *  Created on: Apr 30, 2017
 *      Author: rmerriam
 */

#ifndef task_2_2_H_
#define task_2_2_H_

namespace t2_2 {
    const string task_name = { "task_2_2" };
//--------------------------------------------------------------------------------------------------------------------------
    struct Task_2_2 {
        SelectorNode node { task_name,
            {
                harness_active,
                check_pt_2_2_done,
                *new MemSequenceNode { "memsequence_" + task_name,
                    {
                        ready,
                        //                        initialize.node,
                        *new Announce { task_name, " Place solar panel on array " },

                        *new Announce { task_name, " jump to 2.3 " },
                        skip_prep.node,
                        start_task_2_3,
                        wait_harness_active,
                        finished,
                    }
                },
            }
        };

    private:
        IsHarnessActive harness_active { "harness_active_" + task_name };
        Initialize initialize;

        IsCheckpointDone check_pt_2_2_done { "check_" + task_name, 2, 2 };

        Announce finished { task_name, "<<=== finished " };
        Announce ready { task_name, "<<=== ready " };

        SkipPrep skip_prep;
        SetTaskNode start_task_2_3 { "start task 2/3 ", 2, 3 };
        Inverter wait_harness_active { "wait_harness_active", *new IsHarnessActive { "harness_started after skip to 2_3" } };
    };
}

#endif /* task_2_2_H_ */
