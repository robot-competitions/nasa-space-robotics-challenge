/*
 * Task_2_1.h
 *
 *  Created on: Apr 30, 2017
 *      Author: rmerriam
 */

#ifndef Task_2_H_
#define Task_2_H_

#include "Task_2/Task_2_1.h"
#include "Task_2/Task_2_2.h"
#include "Task_2/Task_2_3.h"
#include "Task_2/Task_2_4.h"
#include "Task_2/Task_2_5.h"
#include "Task_2/Task_2_6.h"

//--------------------------------------------------------------------------------------------------------------------------
struct Task_2 {

    MemSequenceNode node { "Task_2",
        {
            *new SelectorNode { "task_skipper",
                {
                    harness_active,
                    check_pt_2_5_done,
                    *new MemSequenceNode { "task startup",
                        {
                            start_task_2_6,
                            wait_harness_active
                        }
                    },
                }
            },
            //            task_2_1.node,
//            task_2_2.node,
//            task_2_3.node,
//            task_2_4.node,
//            task_2_5.node,
            task_2_6.node,
        }

    };

private:

//    t2_1::Task_2_1 task_2_1;
//    t2_2::Task_2_2 task_2_2;
//    t2_3::Task_2_3 task_2_3;
//    t2_4::Task_2_4 task_2_4;
//    t2_5::Task_2_5 task_2_5;
    t2_6::Task_2_6 task_2_6;

    IsHarnessActive harness_active { "harness_active_" };
    IsCheckpointDone check_pt_2_5_done { "check_2_5", 2, 5 };
    SetTaskNode start_task_2_6 { "start_task_2_6", 2, 6 };
    Inverter wait_harness_active { "wait_harness_active", *new IsHarnessActive { "harness_started after skip" } };
};

#endif /* Task_2_H_ */
