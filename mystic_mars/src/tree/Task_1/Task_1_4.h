/*
 * Task_1_4.h
 *
 *  Created on: May 12, 2017
 *      Author: rmerriam
 */

#ifndef Task_1_4_H_
#define Task_1_4_H_

namespace t1_4 {
    const string task_name { "task_1_4 " };
//--------------------------------------------------------------------------------------------------------------------------
    struct Task_1_4 {

        SelectorNode node { task_name,
            {
                harness_active,
                stop_checkpoint_done,

                *new MemSequenceNode { "memsequence_" + task_name,
                    {
                        ready,
                        initialize.node,
                        *new Announce { task_name, " backup" },

                        stance,
                        backup_turn.node,
                        look_down.node,
                        *new Announce { task_name, " wander" },

                        *new SelectorNode { "wander to 1/4",
                            {
                                check_pt_1_4_done,
                                wander.node,
                            },
                        },
                        move_done,
                        finished
                    }
                },
            }
        };

    private:

        IsHarnessActive harness_active { "harness_active_" + task_name };
        Initialize initialize;
        LookDown look_down { 10, 10, 0.35 };
        BackupTurn backup_turn { -0.3 };
        Wander wander;
        IsCheckpointDone check_pt_1_4_done { "check_" + task_name, 1, 4 };
        IsMovementDone move_done { "move_done" };
        Turn stance { "stance", Turn::eStance };
        WalkingStop stop_checkpoint_done { "stop_checkpoint_done " + task_name, check_pt_1_4_done };

        Announce finished { task_name, "<<=== finished " };
        Announce ready { task_name, "<<=== ready " };
    };
}
#endif /* Task_1_4_H_ */
