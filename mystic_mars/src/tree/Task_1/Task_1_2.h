/*
 * Task_1_2.h
 *
 *  Created on: Apr 30, 2017
 *      Author: rmerriam
 */

#ifndef Task_1_2_H_
#define Task_1_2_H_

namespace t1_2 {
    const string task_name { "task_1_2" };
//--------------------------------------------------------------------------------------------------------------------------
    struct Task_1_2 {

        SelectorNode node { task_name,
            {
                harness_active,
                check_pt_1_2_done,
                *new MemSequenceNode { "memsequence_" + task_name,
                    {
                        ready,
                        initialize.node,
                        //                        *new Announce { task_name, " turn to console " },
//                        turn_to_console,

                        look_down.node,
                        console_distance.node,
                        *new Announce { task_name, " walk to console " },
                        walk_to_console,

//                        *new Announce { task_name, " jump to 1.4 " },
//                        skip_prep.node,
//                        start_task_1_4,
//                        wait_harness_active,
                        finished,
                    }
                },
            }
        };

    private:

        IsHarnessActive harness_active { "harness_active_" + task_name };
        IsCheckpointDone check_pt_1_2_done { "check_" + task_name, 1, 2 };

        Initialize initialize;
        Turn turn_to_console { "turn to dish", Turn::eBlackboard };

        LookDown look_down { 20, 20 };
        ConsoleDistance console_distance { -0.6 };
        Move walk_to_console { "walk_to_console" };

        Announce finished { task_name, "<<=== finished " };
        Announce ready { task_name, "<<=== ready " };

        SetTaskNode start_task_1_4 { "start task 1/4 ", 1, 4 };
        SkipPrep skip_prep;
        Inverter wait_harness_active { "wait_harness_active", *new IsHarnessActive { "harness_started after skip to 1_4" } };
    };
}
#endif /* Task_1_2_H_ */
