/*
 * Task_1_3.h
 *
 *  Created on: May 12, 2017
 *      Author: rmerriam
 */

#ifndef TASK_1_3_H_
#define TASK_1_3_H_

namespace t1_3 {
    const string task_name { "task_1_3 " };

//--------------------------------------------------------------------------------------------------------------------------
    struct Task_1_3 {

        SelectorNode node { task_name,
            {
                harness_active,
                check_pt_1_3_done,
                *new MemSequenceNode { "memsequence_" + task_name,
                    {
                        ready,
                        initialize.node,
                        *new Announce { task_name, " jump to 1.4 " },
                        skip_prep.node,
                        start_task_1_4,
                    }
                },
                finished
            }
        };

    private:

        IsHarnessActive harness_active { "harness_active_" + task_name };
        Initialize initialize;
        IsCheckpointDone check_pt_1_3_done { "check_" + task_name, 1, 3 };
        SetTaskNode start_task_1_4 { "start_task_1_4", 1, 4 };
        SkipPrep skip_prep;

        Announce finished { task_name, "<<=== finished " };
        Announce ready { task_name, "<<=== ready " };
    };
}
#endif /* TASK_1_3_H_ */
