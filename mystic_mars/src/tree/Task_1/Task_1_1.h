/*
 * Task_1_1.h
 *
 *  Created on: Apr 30, 2017
 *      Author: rmerriam
 */

#ifndef Task_1_1_H_
#define Task_1_1_H_

namespace t1_1 {
    const string task_name = { "task_1_1" };

    //--------------------------------------------------------------------------------------------------------------------------
    struct Task_1_1 {

        SelectorNode node { task_name,
            {
                harness_active,
                //                check_pt_1_1_done,
                stop_checkpoint_done,
                *new MemSequenceNode { "memsequence_" + task_name,
                    {
                        announce_ready,
                        initialize.node,
                        console_distance.node,
                        *new Announce { task_name, " start leave start box " },
                        leave_start,

                        // for the odd ball case where the console is just off a meter when R5 walks by
                        turn_to_console,
                        walk_to_console,
                        announce_finished,
                    }
                },
            }
        };
        private:

        IsHarnessActive harness_active { "harness_active_" + task_name };
        IsCheckpointDone check_pt_1_1_done { "check_" + task_name, 1, 1 };
        Initialize initialize;
        Move leave_start { "leave_start", true };
        ConsoleDistance console_distance;
        Announce announce_finished { task_name, "<<=== finished " };
        Announce announce_ready { task_name, "<<=== ready " };
        Turn turn_to_console { "turn to dish", Turn::eBlackboard };
        Move walk_to_console { "walk_to_console" };

        WalkingStop stop_checkpoint_done { "stop_checkpoint_done " + task_name, check_pt_1_1_done };

    };
}
#endif /* Task_1_1_H_ */
