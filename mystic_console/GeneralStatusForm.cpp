/*
 * GeneralStatusForm.cpp
 *
 *  Created on: Apr 2, 2017
 *      Author: rmerriam
 */

#include <mystic.h>
using namespace ros;

#include "DataField.h"
using namespace scr;

#include "GeneralStatusForm.h"
//--------------------------------------------------------------------------------------------------------------------------
GeneralStatusForm::GeneralStatusForm(const int y, const int x) :
    mFields { }, mForm { mFields, y, x } {

    uint8_t item_row { 2 };
    int width { 18 };
    NField::build_header(mFields, "System Information", 1, COLS - 3);

    wall_time = NField::build_data_item(mFields, "Wall Time:", item_row++, width);
    simulation_time = NField::build_data_item(mFields, "Simulation Time:", item_row++, width);

    item_row = 2;
    uint8_t item_col = (width * 1.5);
    total_penalty = NField::build_data_item(mFields, "Total Penalty:", item_row++, width, item_col);
    total_time = NField::build_data_item(mFields, "Total Time:", item_row++, width, item_col);
    total_score = NField::build_data_item(mFields, "Total score:", item_row++, width, item_col);
    mForm.init();
}
//--------------------------------------------------------------------------------------------------------------------------
void GeneralStatusForm::update() {

    wall_time->setData(ros::WallTime::now().sec);
    simulation_time->setData(ros::Time::now().sec);

    total_penalty->setData(mScoring.totalPenalty());
    total_time->setData(mScoring.totalTime().sec);
    total_score->setData(mScoring.totalScore());

    wrefresh(mForm.win());
}
