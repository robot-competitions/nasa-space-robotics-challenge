/*
 * main.cpp
 *
 *  Created on: Apr 6, 2016
 *      Author: rmerriam
 */

#include <mystic.h>

using namespace ros;

#include <unistd.h>
#include <panel.h>
#include <form.h>

#include "NCurses.h"
#include "NWindow.h"
#include "NForm.h"
#include "NField.h"
#include "DataField.h"
#include "HeaderField.h"
#include "LabelField.h"
using namespace scr;

//--hide-menubar
//--full-screen

#include "GeneralStatusForm.h"
#include "RobotPoseForm.h"
#include "SatelliteDishForm.h"
#include "TaskStatusForm.h"

constexpr float version { 0.1 };
//--------------------------------------------------------------------------------------------------------------------------
int main(int argc, char** argv) {
    try {
        init(argc, argv, "mystic_console");

        NodeHandle nh;
        Time::waitForValid();
        AsyncSpinner spinner(0);
        spinner.start();

        NCurses nc;
        init_pair(1, COLOR_WHITE, COLOR_BLACK);
        wattron(stdscr, COLOR_PAIR(1));

        const int row = 0; //LINES / 4;
        GeneralStatusForm gsf(LINES - 10, 0);
        RobotPoseForm rpf(row, 0);
        TaskStatusForm tsf(row, rpf.getX() + 2);
        SatelliteDishForm sdf(row, tsf.getX() + 2);

        while (NCurses::check_input() && ros::master::check()) {
            gsf.update();
            rpf.update();
            tsf.update();
            sdf.update();
            refresh();
            Duration(0.500).sleep();
        }
        endwin();
    }
    catch (exception& e) {
        cerr << "\n\n  exception " << e.what() << '\n';
    }

    return 0;
}

