/*
 * NCurses.cpp
 *
 *  Created on: Apr 7, 2016
 *      Author: rmerriam
 */
#include <iostream>
using namespace std;

#include "NCurses.h"

//--------------------------------------------------------------------------------------------------------------------------
NCurses::NCurses() {

    initscr();
    raw();
    keypad(stdscr, TRUE); /* We get F1, F2 etc..		*/
    noecho(); /* Don't echo() while we do getch */
    curs_set(cursor_off);
    start_color();
    getmaxyx(stdscr, mRows, mCols);
    refresh();
}
//--------------------------------------------------------------------------------------------------------------------------
NCurses::~NCurses() {
    endwin();
}
//--------------------------------------------------------------------------------------------------------------------------
void NCurses::cursorPos(int& y, int& x) {
    getyx(stdscr, y, x);
}
