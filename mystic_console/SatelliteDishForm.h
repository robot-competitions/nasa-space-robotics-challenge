/*
 * SatelliteDishForm.h
 *
 *  Created on: Apr 2, 2017
 *      Author: rmerriam
 */

#ifndef SatelliteDishForm_H_
#define SatelliteDishForm_H_
#include <mystic.h>
using namespace ros;

#include <SatelliteDishStatus.h>

#include <iostream>
using namespace std;

#include "NField.h"
#include "NForm.h"
using namespace scr;
//--------------------------------------------------------------------------------------------------------------------------
class SatelliteDishForm {
public:
    SatelliteDishForm(const int y, const int x);

    void update();
    const int getX() const {
        return mForm.x() + mForm.cols();
    }
private:
    FieldVec mFields;
    NForm mForm;
    SatelliteDishStatus mSatStatus;

    DataFieldPtr target_pitch;
    DataFieldPtr target_yaw;
    DataFieldPtr current_pitch;
    DataFieldPtr current_yaw;
    DataFieldPtr pitch_correct_now;
    DataFieldPtr yaw_correct_now;
    DataFieldPtr pitch_completed;
    DataFieldPtr yaw_completed;

};
#endif /* SatelliteDishForm_H_ */
