/*
 * TaskStatusForm.cpp
 *
 *  Created on: Apr 2, 2017
 *      Author: rmerriam
 */

#include <mystic.h>
#include <TaskStatus.h>
using namespace ros;

#include "DataField.h"
using namespace scr;

#include "TaskStatusForm.h"
//--------------------------------------------------------------------------------------------------------------------------
TaskStatusForm::TaskStatusForm(const int y, const int x) :
    mFields { }, mForm { mFields, y, x } {

    uint8_t item_row { 2 };
    int width { 15 };
    NField::build_header(mFields, "Task Status");

    task = NField::build_data_item(mFields, "Task:", item_row++, width);
    check_point = NField::build_data_item(mFields, "Check Point:", item_row++, width);
    item_row++;
    start_time = NField::build_data_item(mFields, "Start Time:", item_row++, width);
    elapsed_time = NField::build_data_item(mFields, "Elapsed Time:", item_row++, width);
    remaining_time = NField::build_data_item(mFields, "Remaining Time:", item_row++, width);
    item_row++;
    is_finished = NField::build_data_item(mFields, "Is Finished:", item_row++, width);
    is_timedout = NField::build_data_item(mFields, "Is Timed Out:", item_row++, width);

    item_row++;
    harness_status = NField::build_data_item(mFields, "Harness Status:", item_row++, width);

    mForm.init();

}
//--------------------------------------------------------------------------------------------------------------------------
void TaskStatusForm::update() {
    task->setData(mTaskStatus.task());
    check_point->setData(mTaskStatus.checkPt());

    start_time->setData(uint32_t(mTaskStatus.startTime().toSec()));
    elapsed_time->setData(uint32_t(mTaskStatus.elapsedTime().toSec()));

    uint32_t rem_time { 0 };
    switch (mTaskStatus.task()) {
        case 1:
            rem_time = (30 * 60) - mTaskStatus.elapsedTime().toSec();
            break;
        case 2:
            case 3:
            rem_time = (2 * 60 * 60) - mTaskStatus.elapsedTime().toSec();
            break;
        default:
            break;
    }
    remaining_time->setData(rem_time);

    is_timedout->setData(mTaskStatus.isTimedout());
    is_finished->setData(mTaskStatus.isFinished());

    string status_text[] {
        "Released", "Attaching", "Attached", "Lowering", "Standing", "Releasing"
    };

    harness_status->setData(status_text[mHarnessStatus.status()]);

    wrefresh(mForm.win());
}
