/*
 * NForm.h
 *
 *  Created on: Apr 1, 2017
 *      Author: rmerriam
 */

#ifndef NFORM_H_
#define NFORM_H_
#include <mystic.h>

#include <form.h>
#include "NField.h"

namespace scr {
    using FormPtr = FORM*;
    using WindowPtr = WINDOW*;
//--------------------------------------------------------------------------------------------------------------------------
    class NForm {
    public:
        NForm(FieldVec& f, const int y, const int x) :
                mFields(f),
                mX(x), mY(y) {
        }

        const int box() {
            return ::box(mWindow, 0, 0);
        }

        void init() {
            mFields.push_back(nullptr);
            mForm = new_form(mFields.data());

            scale_form(mForm, &mRows, &mCols);
            mWindow = newwin(mRows + 2, mCols + 2, mY, mX);

            /* Set main window and sub window */
            set_form_win(mForm, mWindow);
            mSubWin = derwin(mWindow, mRows + 1, mCols, 0, 0);
            set_form_sub(mForm, mSubWin);
            post_form(mForm);
            box();
            wrefresh(mWindow);
        }
        //--------------------------------------------------------------------------------------------------------------------------
        const int cols() const {
            return mCols;
        }
        const int rows() const {
            return mRows;
        }
        FormPtr operator()() const {
            return mForm;
        }
        WindowPtr win() const {
            return mWindow;
        }
        WindowPtr subWin() const {
            return mSubWin;
        }

        const int x() const {
            return mX;
        }
    private:
        FieldVec& mFields;
        int mX;
        int mY;
        FormPtr mForm {};
        int mRows {};
        int mCols {};
        WindowPtr mWindow {};
        WindowPtr mSubWin {};
    };
}
#endif /* NFORM_H_ */
