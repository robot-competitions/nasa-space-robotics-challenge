/*
 * GeneralStatusForm.h
 *
 *  Created on: Apr 2, 2017
 *      Author: rmerriam
 */

#ifndef GeneralStatusForm_H_
#define GeneralStatusForm_H_
#include <mystic.h>
using namespace ros;
#include <Scoring.h>

#include <iostream>
using namespace std;

#include "NField.h"
#include "NForm.h"
using namespace scr;
//-------------------------------------------------------------------------------------------------------------------------
using Durations = std::vector<ros::Duration>;
//--------------------------------------------------------------------------------------------------------------------------
class GeneralStatusForm {
public:
    GeneralStatusForm(const int y, const int x);

    void update();

private:
    Scoring mScoring;

    FieldVec mFields;
    NForm mForm;

    DataFieldPtr simulation_time;
    DataFieldPtr wall_time;

    DataFieldPtr total_score;
    DataFieldPtr total_time;
    DataFieldPtr total_penalty;


};
#endif /* GeneralStatusForm_H_ */
