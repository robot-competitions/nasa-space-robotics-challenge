/*
 * RobotPoseForm.h
 *
 *  Created on: Apr 2, 2017
 *      Author: rmerriam
 */

#ifndef RobotPoseForm_H_
#define RobotPoseForm_H_
#include <mystic.h>
#include <RobotPose.h>
using namespace ros;

#include <iostream>
using namespace std;

#include "NField.h"
#include "NForm.h"
using namespace scr;
//--------------------------------------------------------------------------------------------------------------------------
class RobotPoseForm {
public:
    RobotPoseForm(const int y, const int x);

    void update();
    const int getX() const {
        return mForm.x() + mForm.cols();
    }

private:
    FieldVec mFields;
    NForm mForm;
    RobotPose mPose;

    DataFieldPtr pose_x_field;
    DataFieldPtr pose_y_field;
    DataFieldPtr pose_z_field;
    DataFieldPtr pose_angle_rad;
    DataFieldPtr pose_angle_deg;

};
#endif /* RobotPoseForm_H_ */
