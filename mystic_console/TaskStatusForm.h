/*
 * TaskStatusForm.h
 *
 *  Created on: Apr 2, 2017
 *      Author: rmerriam
 */

#ifndef TaskStatusForm_H_
#define TaskStatusForm_H_
#include <mystic.h>
#include <TaskStatus.h>
#include <HarnessStatus.h>
using namespace ros;

#include <iostream>
using namespace std;

#include "NField.h"
#include "NForm.h"
using namespace scr;
//--------------------------------------------------------------------------------------------------------------------------
class TaskStatusForm {
public:
    TaskStatusForm(const int y, const int x);

    void update();
    const int getX() const {
        return mForm.x() + mForm.cols();
    }
private:
    FieldVec mFields;
    NForm mForm;
    TaskStatus mTaskStatus;
    HarnessStatus mHarnessStatus;

    DataFieldPtr task;
    DataFieldPtr check_point;
    DataFieldPtr start_time;
    DataFieldPtr elapsed_time;
    DataFieldPtr remaining_time;

    DataFieldPtr is_finished;
    DataFieldPtr is_timedout;

    DataFieldPtr harness_status;

};
#endif /* TaskStatusForm_H_ */
