/*
 * SatelliteDishForm.cpp
 *
 *  Created on: Apr 2, 2017
 *      Author: rmerriam
 */

#include <mystic.h>
using namespace ros;
#include <SatelliteDishStatus.h>

#include "DataField.h"
using namespace scr;

#include "SatelliteDishForm.h"
//--------------------------------------------------------------------------------------------------------------------------
SatelliteDishForm::SatelliteDishForm(const int y, const int x) :
    mFields { }, mForm { mFields, y, x }, mSatStatus { } {

    uint8_t item_row { 2 };
    int width { 15 };
    NField::build_header(mFields, "Sat Dish Status", 1, width * 1.5);

    NField::build_subhead(mFields, "Pitch", item_row++, width);

    target_pitch = NField::build_data_item(mFields, "Target:", item_row++, width);
    current_pitch = NField::build_data_item(mFields, "Current:", item_row++, width);
    pitch_correct_now = NField::build_data_item(mFields, "Correct Now:", item_row++, width);
    pitch_completed = NField::build_data_item(mFields, "Completed:", item_row++, width);

    item_row++;
    NField::build_subhead(mFields, "Yaw", item_row++);
    target_yaw = NField::build_data_item(mFields, "Target:", item_row++, width);
    current_yaw = NField::build_data_item(mFields, "Current:", item_row++, width);
    yaw_correct_now = NField::build_data_item(mFields, "Correct Now:", item_row++, width);
    yaw_completed = NField::build_data_item(mFields, "Completed:", item_row++, width);

    mForm.init();
}
//--------------------------------------------------------------------------------------------------------------------------
void SatelliteDishForm::update() {

    target_pitch->setData(mSatStatus.targetPitch());
    target_yaw->setData(mSatStatus.targetYaw());
    current_pitch->setData(mSatStatus.currentPitch());
    current_yaw->setData(mSatStatus.currentYaw());
    pitch_correct_now->setData(mSatStatus.isPitchCorrectNow());
    yaw_correct_now->setData(mSatStatus.isYawCorrectNow());
    pitch_completed->setData(mSatStatus.isPitchFinished());
    yaw_completed->setData(mSatStatus.isYawFinished());

    wrefresh(mForm.win());
}
