/*
 * RobotPoseForm.cpp
 *
 *  Created on: Apr 2, 2017
 *      Author: rmerriam
 */

#include <mystic.h>
#include <RobotPose.h>
using namespace ros;

#include "DataField.h"
using namespace scr;

#include "RobotPoseForm.h"
//--------------------------------------------------------------------------------------------------------------------------
RobotPoseForm::RobotPoseForm(const int y, const int x) :
    mFields { }, mForm { mFields, y, x } {

    uint8_t item_row { 2 };

    NField::build_header(mFields, "Robot Pose");
    NField::build_subhead(mFields, "Location", item_row++);

    pose_x_field = NField::build_data_item(mFields, "X:", item_row++);
    pose_y_field = NField::build_data_item(mFields, "Y:", item_row++);
    pose_z_field = NField::build_data_item(mFields, "Z:", item_row++);

    item_row++;

    NField::build_subhead(mFields, "Orientation", item_row++);

    pose_angle_rad = NField::build_data_item(mFields, "Angle (rad):", item_row++);
    pose_angle_deg = NField::build_data_item(mFields, "Angle (deg):", item_row++);

    mForm.init();
}
//--------------------------------------------------------------------------------------------------------------------------
void RobotPoseForm::update() {
    pose_x_field->setData(mPose.poseX());
    pose_y_field->setData(mPose.poseY());
    pose_z_field->setData(mPose.poseZ());

    pose_angle_rad->setData(mPose.yaw());
    pose_angle_deg->setData(mPose.yaw() * 180 / M_PI);

    wrefresh(mForm.win());
}
