/*
 * Application.cpp
 *
 *  Created on: Dec 8, 2016
 *      Author: rmerriam
 */
#include <mystic.h>

#include <FootstepStatus.h>
#include <PelvisHeightTraj.h>
#include "Setup.h"
#include "WalkSteps.h"

#include "Application.h"
//---------------------------------------------------------------------------------------------------------------------
void step_track(const Walk& w) {
    FootstepStatus fs;

    while (w.steps() == 0) {
        ros::Duration(0.1).sleep();
    }
    fs.stepCnt(w.steps());

    while (w.steps() != 0) {
        ros::Duration(0.1).sleep();
    }

    cerr << "status done " << mys::nl;
}
//---------------------------------------------------------------------------------------------------------------------
void Application::operator ()() {

    ROS_INFO_STREAM(ros::this_node::getName() << " starting run");
    cerr << fixed << setprecision(5);

    Setup wu(mRaisePelvis);
    ROS_INFO_STREAM(ros::this_node::getName() << " wu creation done");

    WalkSteps w2d(mWalk, mPose);

    ROS_INFO_STREAM(ros::this_node::getName() << " WalkSteps creations done");

    while ( !mPose.isValid()) {
        ros::Duration(0.1).sleep();
    }
    ROS_INFO_STREAM(ros::this_node::getName() << " time valid");

    ros::Time::sleepUntil(ros::Time(27));
    ROS_INFO_STREAM(ros::this_node::getName() << " sim time ready");

    wu();
    thread st(step_track, std::ref(mWalk));
    w2d();

    ROS_INFO_STREAM(ros::this_node::getName() << " shutdown at end");

    st.join();
    ros::shutdown();
}
