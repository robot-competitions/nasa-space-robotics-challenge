/*
 * Application.h
 *
 *  Created on: Dec 8, 2016
 *      Author: rmerriam
 */

#ifndef SRC_APPLICATION_H_
#define SRC_APPLICATION_H_

#include <HandTraj.h>
#include <PelvisHeightTraj.h>
#include <RobotPose.h>
#include <Walk.h>

//---------------------------------------------------------------------------------------------------------------------
class Application {
public:
    Application() = default;

    void operator ()();

private:
    PelvisHeightTraj mRaisePelvis { "pelvis_height_trajectory" };
    Walk mWalk { "footstep_list" };
    RobotPose mPose { "robot_pose" };
};
#endif /* SRC_APPLICATION_H_ */
