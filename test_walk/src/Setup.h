/*
 * WarmUp.h
 *
 *  Created on: Dec 8, 2016
 *      Author: rmerriam
 */

#ifndef SRC_SETUP_H_
#define SRC_SETUP_H_

#include "ActionBase.h"
class PelvisHeightTraj;
//---------------------------------------------------------------------------------------------------------------------
class Setup : public ActionBase {
public:
    Setup(PelvisHeightTraj& rp);

     ~Setup() {
     }
     void operator ()();

private:
     PelvisHeightTraj& mRaisePelvis;
};

#endif /* SRC_SETUP_H_ */
