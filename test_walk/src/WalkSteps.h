/*
 * WalkToDoor.h
 *
 *  Created on: Dec 8, 2016
 *      Author: rmerriam
 */

#ifndef SRC_WalkSteps_H_
#define SRC_WalkSteps_H_

#include <Walk.h>
#include "ActionBase.h"
class RobotPose;
//---------------------------------------------------------------------------------------------------------------------
class WalkSteps : public ActionBase {
public:
    WalkSteps(Walk& w, RobotPose& r);

    ~WalkSteps() {
    }
    void operator ()();

private:
    Walk& mWalk;
    RobotPose& mPose;

    void step_back();
    void step_forward();
    void set_stance();
    void reset_stance();
    void pivot(const double angle);
};

#endif /* SRC_WalkSteps_H_ */

