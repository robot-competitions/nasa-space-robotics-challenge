/*
 * WarmUp.cpp
 *
 *  Created on: Dec 8, 2016
 *      Author: rmerriam
 */

#include <mystic.h>
#include <PelvisHeightTraj.h>
#include <std_msgs/String.h>

#include "params.h"
#include "Setup.h"

//---------------------------------------------------------------------------------------------------------------------
Setup::Setup(PelvisHeightTraj& rp) :
        mRaisePelvis(rp) {
    ROS_INFO_STREAM(ros::this_node::getName() << " wu constructor");

}
//---------------------------------------------------------------------------------------------------------------------
void Setup::operator ()() {

    ROS_INFO_STREAM(ros::this_node::getName() << " warm up");

    mRaisePelvis(WalkParams::pelvis_height);

    ROS_INFO_STREAM(ros::this_node::getName() << " warm up complete");
}
