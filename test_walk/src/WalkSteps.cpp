/*
 * WalkToDoor.cpp
 *
 *  Created on: Dec 8, 2016
 *      Author: rmerriam
 */
#include <mystic.h>
#include <RobotPose.h>
#include <Walk.h>

#include "params.h"
#include "WalkSteps.h"
#include <ChestTraj.h>
//---------------------------------------------------------------------------------------------------------------------
WalkSteps::WalkSteps(Walk& w, RobotPose& r) :
        mWalk(w), mPose(r) {
    ROS_INFO_STREAM(ros::this_node::getName() << " WalkSteps constructor");
}
//---------------------------------------------------------------------------------------------------------------------
void WalkSteps::reset_stance() {
    mWalk.startSteps();

    Vec3 l_loc { mWalk.getLocation(mys::eLeft) };
    Vec3 r_loc { mWalk.getLocation(mys::eRight) };

}
//---------------------------------------------------------------------------------------------------------------------
void WalkSteps::step_back() {
    mWalk.startSteps();

    mWalk.incStep(mys::eLeft, Vec3( -0.4, 0.0, 0));
    mWalk.incStep(mys::eRight, Vec3( -0.4, 0.0, 0));

    mWalk.doSteps();
    mWalk.waitForFinished();
}
//---------------------------------------------------------------------------------------------------------------------
void WalkSteps::step_forward() {
    mWalk.startSteps();

    mWalk.incStep(mys::eLeft, Vec3(0.4, 0.0, 0));
    mWalk.incStep(mys::eRight, Vec3(0.4, 0.0, 0));

    mWalk.doSteps();
    mWalk.waitForFinished();
}
double y;
//---------------------------------------------------------------------------------------------------------------------
void WalkSteps::set_stance() {
    using namespace mys;

    mWalk.startSteps();

    Vec3 l_base = mWalk.getLocation(mys::eLeft);
    Vec3 r_base = mWalk.getLocation(mys::eRight);

#if 1
    Vec3 l_vec(0, WalkParams::spread, l_base.getZ());
    mWalk.setStep(mys::eLeft, l_vec);

    Vec3 r_vec(0, -WalkParams::spread, r_base.getZ());
    mWalk.setStep(mys::eRight, r_vec);

//    mWalk.alignPose(mPose.orientation());

    mWalk.doSteps();
    mWalk.waitForFinished();
#endif
}
//---------------------------------------------------------------------------------------------------------------------
void WalkSteps::pivot(const double yaw) {
    mWalk.startSteps();
    mWalk.turn(yaw);
    mWalk.doSteps();
    mWalk.waitForFinished();
}
//---------------------------------------------------------------------------------------------------------------------
void WalkSteps::operator ()() {

//    ROS_INFO_STREAM(ros::this_node::getName() << " WalkSteps");
    WalkParams::pace_setup(mWalk);

    mPose.display("WalkSteps begin");

    uint64_t count { };
    while (true) {
    set_stance();
        cerr << ++count << mys::nl;
}
    y = 45;

//    step_forward();
//    pivot(30 * M_PI / 180.0);
//    step_forward();
//    step_back();
//    constexpr double deg { 45 };
//    for (int i = 0; i < 8; ++i) {
//        step_forward();
//        pivot( -deg * M_PI / 180.0);
//    }
//    for (int i = 0; i < 8; ++i) {
//        step_forward();
//        pivot(deg * M_PI / 180.0);
//    }
}
