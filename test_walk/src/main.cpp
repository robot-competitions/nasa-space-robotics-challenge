#include <mystic.h>

#include "Application.h"
constexpr float version { 0.1 };
//---------------------------------------------------------------------------------------------------------------------

int main(int argc, char** argv) {
    init(argc, argv, "test_walk");
    NodeHandle nh;

    Time::waitForValid();

    ROS_INFO_STREAM(ros::this_node::getName() << " initialized v." << version);

    AsyncSpinner spinner(0);
    spinner.start();

    Application app;
    app();

    return 0;
}
