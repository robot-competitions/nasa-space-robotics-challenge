#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H

#include <string>
#include <stdexcept>
using namespace std;
//--------------------------------------------------------------------------------------------------------------------------
namespace BT {

    class BehaviorTreeException : public std::exception {
    private:
        const char* Message;
    public:
        BehaviorTreeException(const string Message);
        const char* what();
    };
    //--------------------------------------------------------------------------------------------------------------------------
    inline BehaviorTreeException::BehaviorTreeException(const string Message) {
        this->Message = std::string("BehaviorTreeException: " + Message).c_str();
    }
    //--------------------------------------------------------------------------------------------------------------------------
    inline const char* BehaviorTreeException::what() {
        return Message;
    }
}

#endif
