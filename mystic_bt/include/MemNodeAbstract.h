/*
 * MemNodeAbstract.h
 *
 *  Created on: Mar 15, 2017
 *      Author: rmerriam
 */

#ifndef MEMNODEABSTRACT_H_
#define MEMNODEABSTRACT_H_

#include <CompositeAbstract.h>
/*--------------------------------------------------------------------------------------------------------------------------
 *
 *  Memory nodes start each pass at the last child node that was not completed, especially if it was left in a running mode.
 *  The nodes derived from a memory node must also be derived from a LoopNodeAbstract using multiple inheritance.
 *
 *-------------------------------------------------------------------------------------------------------------------------*/
namespace BT {
    class MemNodeAbstract : public CompositeAbstract {
    public:

        MemNodeAbstract(const string& name) :
                CompositeAbstract(name) {
        }
        MemNodeAbstract(const string& name, TreeVect v) :
                CompositeAbstract(name, v), mChildrenMemory(v) {
        }

        virtual ~MemNodeAbstract() = default;

        virtual const TreeNode::NodeState eval(Blackboard& bb) override {
            CompositeAbstract::eval(bb);
            cout << "++++" << getResults() << endl;
            memResultCheck();
            return getState();
        }
        virtual void reset(Blackboard& bb) override;

    protected:
        virtual void memResultCheck() = 0;

        TreeVect mChildrenMemory;
    };
}
//--------------------------------------------------------------------------------------------------------------------------
inline
void MemNodeAbstract::reset(Blackboard& bb) {
    mChildren = mChildrenMemory;

    for (auto c : mChildren) {
        TreeNode& child = c.get();
        child.reset(bb);
    }
}

#endif /* MEMNODEABSTRACT_H_ */
