#ifndef CONDITIONNODE_H
#define CONDITIONNODE_H

//#include <ConditionNode.h>
#include "TreeNode.h"
//---------------------------------------------------------------------------------------------------------------------
namespace BT
{
    class ConditionNode : public TreeNode
    {
    public:
        ConditionNode(const string& name);
        virtual ~ConditionNode() = default;
    };
    //---------------------------------------------------------------------------------------------------------------------
    inline ConditionNode::ConditionNode(const string& name) :
            TreeNode(name, Condition) {
    }

}

#endif
