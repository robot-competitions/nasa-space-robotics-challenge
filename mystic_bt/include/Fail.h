#ifndef Fail_H
#define Fail_H

#include <DecoratorNodeAbstract.h>
/*--------------------------------------------------------------------------------------------------------------------------
 *      A Fail decorator always fails.
 *
 *------------------------------------------------------------------------------------------------------------------------*/
namespace BT {

    class Fail : public DecoratorNodeAbstract {
    public:
        Fail(const string& name) :
            DecoratorNodeAbstract { name, *this } {
        }
        virtual ~Fail() = default;

        virtual const NodeState eval(Blackboard& bb) override {
            setState(Failure);
            return getState();
        }

    protected:

    private:

    };
}

#endif
