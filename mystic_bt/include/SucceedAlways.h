#ifndef SucceedAlways_H
#define SucceedAlways_H

#include <DecoratorNodeAbstract.h>
/*--------------------------------------------------------------------------------------------------------------------------
 *      A SucceedAlways decorator reports success for its child regardless of the actual state of the child.
 *
 *      If a reset is received the child will be evaluated.
 *------------------------------------------------------------------------------------------------------------------------*/
namespace BT {

    class SucceedAlways : public DecoratorNodeAbstract {
    public:
        SucceedAlways(const string& name, TreeNode& child) :
            DecoratorNodeAbstract { name, child } {
        }
        virtual ~SucceedAlways() = default;

        virtual const NodeState eval(Blackboard& bb) override {
            setState(Success);
            mChild.eval(bb);

            return getState();
        }

        virtual void reset() override;

    protected:
        bool mSucceeded { false };

    private:

    };
    //--------------------------------------------------------------------------------------------------------------------------
    inline void SucceedAlways::reset() {
        mSucceeded = false;
        mChild.reset();
    }
}

#endif
