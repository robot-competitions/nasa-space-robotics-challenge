#ifndef BEHAVIORTREE_H
#define BEHAVIORTREE_H
/*
 * Behavior Tree:
 *
 * A behavior tree (BT) is a representation of the activity in an entity, which may be a robot or character in a game.
 * It provides a form of artificial intelligence.
 *
 * The BT is a tree on nodes. At the tips, or leaves, are two types of nodes that do the actual decision and actions
 * of the robot.
 *
 * Within the BT are control nodes that takes a single node or an aggregation of other nodes as its
 * children. A control node operates on each of its children. A control node may be a child of another control node.
 * Decision and action nodes do not have children.
 *
 * Each node returns a status to the parent which may be success, failure, or running. Success means the node
 * completed its operation. Failure means the node could not compete its operation. Running means the node is not
 * finished with its operation.
 *
 * Sequence: AND operation, all nodes must return success, failure halts following nodes
 * Selector: OR operation, any node may return success, failure proceeds to next node
 * Parallel: all nodes processed, specified number must return success.
 *
 * Threader: runs child as a separate thread of execution
 *
 *
 */

#include <ControlNode.h>
#include <Blackboard.h>

#include <Fail.h>
#include <Inverter.h>
#include <IteratorNode.h>
#include <LoopNode.h>
#include <MemSelectorNode.h>
#include <MemSequenceNode.h>
#include "Reset.h"
#include <SelectorNode.h>
#include <SequenceNode.h>
#include <SucceedAlways.h>
#include <SucceedOnce.h>

#endif
