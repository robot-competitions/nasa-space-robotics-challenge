#ifndef MemSequenceNode_H
#define MemSequenceNode_H

#include <CompositeAbstract.h>
/*--------------------------------------------------------------------------------------------------------------------------
 *
 *      A mem sequence runs until a child returns failure and then returns failure. It returns success if all children
 *      return success. A running response is returned as running and the running child is stored. The mem sequence
 *      executes the running child and continues as normal if it fails or succeeds.
 *
 *      Once a failure or running response is received the remainder of the children are preempted which allows additional
 *      processing by the following children.
 *
 *      It is an AND operation on the results of the children.
 --------------------------------------------------------------------------------------------------------------------------*/
namespace BT {
    class MemSequenceNode : public CompositeAbstract {
    public:
        MemSequenceNode(const string& name, TreeVect v);

        virtual ~MemSequenceNode() = default;

    protected:
        virtual bool isLoopDone(const NodeState s) override;

        virtual bool skipSuccessfulChild(const TreeNode::NodeState s) override;
        //        void resetChildren();

        uint16_t mSuccessCnt { 0 };
    };

}
#endif
