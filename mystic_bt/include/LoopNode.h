#ifndef LoopNode_H
#define LoopNode_H

#include <CompositeAbstract.h>
//--------------------------------------------------------------------------------------------------------------------------
/*
 *      A loop runs all the children returning success if any return success.
 *
 *      It is an OR operation, like a selector, on the results of the children but
 *      does not stop on the first success.
 */
//--------------------------------------------------------------------------------------------------------------------------
namespace BT {

    class LoopNode : public CompositeAbstract {
    public:

        LoopNode(const string& name, TreeVect v, const int32_t& passes = -1);

        virtual ~LoopNode() = default;
        virtual const NodeState eval(Blackboard& bb) override;

    protected:
        virtual bool isLoopDone(const TreeNode::NodeState s);
        virtual bool skipSuccessfulChild(const TreeNode::NodeState s) override;

        int32_t mCnt { 0 };
        int32_t mPasses;
    };

}

#endif
