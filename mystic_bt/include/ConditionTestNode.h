#ifndef CONDITIONTEST_H
#define CONDITIONTEST_H

//#include <ConditionTestNode.h>

#include <ConditionNode.h>
using namespace BT;
//---------------------------------------------------------------------------------------------------------------------
namespace BT {
    class ConditionTestNode : public ConditionNode {
    public:
        ConditionTestNode(const string& name, const int bit_pattern) :
                ConditionNode::ConditionNode(name), mBits(bit_pattern) {
        }
        ~ConditionTestNode() = default;

        const virtual NodeState eval(Blackboard& bb) override;

    private:
        int i {};
        int mBits;
    };
}

#endif
