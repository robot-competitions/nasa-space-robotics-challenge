#ifndef SEQUENCENODE_H
#define SEQUENCENODE_H

#include <CompositeAbstract.h>

/*--------------------------------------------------------------------------------------------------------------------------
 *      A sequence runs until a child returns failure and then returns failure. It returns success if no child returns failure.
 *      A running response is returned as running.
 *
 *      Once a failure or running response is received the remainder of the children are preempted which allows additional
 *      processing by the following children.
 *
 *      It is an AND operation on the results of the children.
 *------------------------------------------------------------------------------------------------------------------------*/
namespace BT {
    class SequenceNode : public CompositeAbstract {
    public:
        SequenceNode(const string& name, TreeVect v);
        SequenceNode(const SequenceNode& s) :
            SequenceNode(s.mName, s.mChildren) {
        }

        virtual ~SequenceNode() = default;

    protected:
        virtual bool isLoopDone(const NodeState s) override;
    };

}
#endif
