#ifndef Iterator_H
#define Iterator_H

#include <CompositeAbstract.h>
//--------------------------------------------------------------------------------------------------------------------------
/*
 *      An Iterator runs all the children returning running if a child is running. Once all children are exercised
 *      it returns success.
 *
 *      It is an OR operation, like a selector, on the results of the children but
 *      does not stop on the first success.
 */
//--------------------------------------------------------------------------------------------------------------------------
namespace BT {

    class IteratorNode : public CompositeAbstract {
    public:

        IteratorNode(const string& name, TreeVect v);

        virtual ~IteratorNode() = default;
        virtual const NodeState eval(Blackboard& bb) override;

    protected:
        virtual bool isLoopDone(const TreeNode::NodeState s);
    };

}

#endif
