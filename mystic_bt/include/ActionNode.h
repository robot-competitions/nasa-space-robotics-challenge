#ifndef ACTIONNODE_H
#define ACTIONNODE_H

#include <TreeNode.h>

namespace BT {
    //--------------------------------------------------------------------------------------------------------------------------
    class ActionNode : public TreeNode {
    public:

        ActionNode(const string& name);
        virtual ~ActionNode() = default;
    };
    //--------------------------------------------------------------------------------------------------------------------------
    inline ActionNode::ActionNode(const string& name) :
            TreeNode(name, Action) {
    }

}

#endif
