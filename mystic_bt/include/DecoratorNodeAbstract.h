#ifndef DecoratorNode_H
#define DecoratorNode_H

#include <ControlNode.h>
/*--------------------------------------------------------------------------------------------------------------------------
 *      A decorator node takes a single child and changes its behavior. For instance, it might invert the status of the
 *      result from the child.
 *------------------------------------------------------------------------------------------------------------------------*/
namespace BT {

    class DecoratorNodeAbstract : public ControlNode {
    public:
        DecoratorNodeAbstract(const string& name, TreeNode& child) :
                ControlNode { name }, mChild(child) {
        }
        virtual ~DecoratorNodeAbstract() = default;

    protected:
        TreeNode& mChild;
    };
}

#endif
