#ifndef LOOPNODEABSTRACT_H
#define LOOPNODEABSTRACT_H
/*--------------------------------------------------------------------------------------------------------------------------
 *
 *  LoopNodeAbstract is a base class for all the loop nodes. The main eval() method is used by almost all the derived
 *  classes. The different affects of the derived classes are obtained by overriding the three virtual methods. The "star"
 *  methods are needed only for the "star" type of nodes that continue a running node rather than start again at the
 *  beginning of the list of children.
 *
 *  void checkResult(const TreeNode::NodeState s) = 0; - this method checks the result of the eval of a child for the
 *      to determine if the loop is a success, failure, or running. It also determines if the remaining nodes in the
 *      list of children should be called through eval or preempt.
 *
 *------------------------------------------------------------------------------------------------------------------------*/

#include <functional>
#include <deque>
using namespace std;

#include "ControlNode.h"
using namespace BT;

//--------------------------------------------------------------------------------------------------------------------------
using TreeNodeRef = reference_wrapper<TreeNode>;
using TreeVect = deque<reference_wrapper<TreeNode>>;
//--------------------------------------------------------------------------------------------------------------------------
namespace BT {

    class CompositeAbstract : public ControlNode {
    public:

        CompositeAbstract(const string& name, TreeVect v);
        virtual ~CompositeAbstract() = default;

        // The method used to fill the child vector
        void addChild(TreeNode& child);

        virtual const TreeNode::NodeState eval(Blackboard& bb) override;

    protected:
        // isLoopDone determines if the remaining children of this loop are finished for this tick
        virtual bool isLoopDone(const TreeNode::NodeState s) = 0;

        // isChildFinished checks if the child previously ran successfully. It returns true if processing should be skipped.
        virtual bool skipSuccessfulChild(const TreeNode::NodeState s) {
            return false;
        }
        virtual void resetChildren();
        virtual void reset() {
            resetChildren();
            TreeNode::reset();
        }

        TreeVect mChildren;
    };
}

#endif
