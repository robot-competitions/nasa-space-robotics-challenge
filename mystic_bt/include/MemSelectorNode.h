#ifndef MemSelectorNode_H
#define MemSelectorNode_H

#include <CompositeAbstract.h>
//--------------------------------------------------------------------------------------------------------------------------
/*
 *      A memselector runs until a child returns success and then returns success. It returns success if all children
 *      return success. A running response is returned as running. On the next pass the memselector
 *      executes the running child and continues as normal if it fails or succeeds.
 *
 *      Once a failure or running response is received the remainder of the children are preempted which allows additional
 *      processing by the following children.
 *
 *      It is an OR operation on the results of the children.
 */

namespace BT {
    //--------------------------------------------------------------------------------------------------------------------------
    class MemSelectorNode : public CompositeAbstract {
    public:
        MemSelectorNode(const string& name, TreeVect v);

        virtual ~MemSelectorNode() = default;

    protected:
        virtual bool isLoopDone(const NodeState s);

        virtual bool skipSuccessfulChild(const TreeNode::NodeState s) override;

        bool mHaveRunner { false };
    };
}
#endif
