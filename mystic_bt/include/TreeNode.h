#ifndef TREENODE_H
#define TREENODE_H

#include <atomic>
#include <iostream>
#include <iomanip>
#include <string>
#include <typeinfo>
#include <unistd.h>

using namespace std;

#include "Blackboard.h"
#include "Exceptions.h"
//--------------------------------------------------------------------------------------------------------------------------
namespace BT {

    class TreeNode {
    public:
        enum NodeType {
            Action, Condition, Control
        };
        enum NodeState {
            Failure, Success, Running
        };

        TreeNode(const string& name, const NodeType type);
        TreeNode(TreeNode& tn) :
            TreeNode(tn.mName, tn.mType) {
        }
        virtual ~TreeNode() = default;

        virtual const NodeState eval(Blackboard& bb) = 0;     // The method that is going to be executed by the node

        const string& getName() const;
        string getNodeId() const;
        virtual NodeState getState() const;
        const NodeType getType() const;

        virtual void reset();

        // utility methods for examining node state as text
        const string getNodeText() const;
        static const string& getStateText(const NodeState ns);
        static const string& getTypeText(const NodeType nt);

        struct Depth {
            Depth() {
                ++mDepth;
            }
            ~Depth() {
                --mDepth;
            }
            static int8_t mDepth;
        };

    protected:
        void setState(const NodeState new_state);

        const string mName;
        mutable atomic<NodeState> mState { Failure };
        const NodeType mType;
    };
    //--------------------------------------------------------------------------------------------------------------------------
    // Reset a node if it needs it to restart processing
    inline void TreeNode::reset() {
        mState = Failure;
    }
    //--------------------------------------------------------------------------------------------------------------------------
    inline ostream& indent(ostream& os) {
        os << setw(TreeNode::Depth::mDepth * 3) << " ";
        return os;
    }
    //--------------------------------------------------------------------------------------------------------------------------
    inline string TreeNode::getNodeId() const {
        return typeid( *this).name();
    }
}

#endif
