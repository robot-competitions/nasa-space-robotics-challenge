#ifndef Reset_H
#define Reset_H

#include <DecoratorNodeAbstract.h>
/*--------------------------------------------------------------------------------------------------------------------------
 *      A Reset decorator always Resets.
 *
 *------------------------------------------------------------------------------------------------------------------------*/
namespace BT {

    class Reset : public DecoratorNodeAbstract {
    public:
        Reset(const string& name, TreeNode& child) :
            DecoratorNodeAbstract { name, child } {
        }
        virtual ~Reset() = default;

        virtual const NodeState eval(Blackboard& bb) override {
            mChild.reset();
            setState(mChild.eval(bb));
            return getState();
        }


    protected:

    private:

    };
}

#endif
