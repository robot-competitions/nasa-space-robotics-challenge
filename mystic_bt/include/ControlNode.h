#ifndef CONTROLNODE_H
#define CONTROLNODE_H

#include <TreeNode.h>
//--------------------------------------------------------------------------------------------------------------------------
namespace BT {

    class ControlNode : public TreeNode {
    public:
        ControlNode(const string& name);
        ControlNode(ControlNode&) = default;
        virtual ~ControlNode() = default;

    protected:

    };
    //--------------------------------------------------------------------------------------------------------------------------
    inline ControlNode::ControlNode(const string& name) :
            TreeNode(name, Control) {
    }

}

#endif
