/*
 * Blackboard.h
 *
 *  Created on: Mar 8, 2017
 *      Author: rmerriam
 */

#ifndef BLACKBOARD_H_
#define BLACKBOARD_H_

namespace BT {

    class Blackboard {
    public:
        Blackboard();
        virtual ~Blackboard() = default;
    };

} /* namespace BT */

#endif /* BLACKBOARD_H_ */
