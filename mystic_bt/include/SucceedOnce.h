#ifndef SucceedOnce_H
#define SucceedOnce_H

#include <DecoratorNodeAbstract.h>
/*--------------------------------------------------------------------------------------------------------------------------
 *      A SucceedOnce decorator reports success for its child once the child succeeds. It is useful when a series of tasks
 *      must be performed without repeating completed tasks.
 *
 *      If a reset is received the child will be evaluated.
 *------------------------------------------------------------------------------------------------------------------------*/
namespace BT {

    class SucceedOnce : public DecoratorNodeAbstract {
    public:
        SucceedOnce(const string& name, TreeNode& child) :
            DecoratorNodeAbstract { name, child } {
        }
        virtual ~SucceedOnce() = default;

        virtual const NodeState eval(Blackboard& bb) override {

            if (mSucceeded) {
                setState(Success);
            }
            else {
                mChild.eval(bb);
                if (mChild.getState() == Success) {
                    mSucceeded = true;
                    setState(Success);
                }
                else {
                    setState(Failure);
                }
            }
            return getState();
        }

        virtual void reset() override;

    protected:
        bool mSucceeded { false };

    private:

    }
    ;
    //--------------------------------------------------------------------------------------------------------------------------
    inline void SucceedOnce::reset() {
        mSucceeded = false;
        mChild.reset();
    }
}

#endif
