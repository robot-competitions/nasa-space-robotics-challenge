#ifndef Inverter_H
#define Inverter_H

#include <DecoratorNodeAbstract.h>
/*--------------------------------------------------------------------------------------------------------------------------
 *      A Inverter decorator reports the inverse of the child's status.
 *
 *      If a reset is received the child will be evaluated.
 *------------------------------------------------------------------------------------------------------------------------*/
namespace BT {

    class Inverter : public DecoratorNodeAbstract {
    public:
        Inverter(const string& name, TreeNode& child) :
            DecoratorNodeAbstract { name, child } {
        }
        virtual ~Inverter() = default;

        const virtual NodeState eval(Blackboard& bb) override;

    protected:

    private:

    };
    //---------------------------------------------------------------------------------------------------------------------
    inline const BT::TreeNode::NodeState Inverter::eval(Blackboard& bb) {
        Depth depth;
        mys::display(indent(cerr), getNodeId(), getName()) << " start " << mys::nl;
        mChild.eval(bb);
        switch (mChild.getState()) {
            case Success:
                setState(Failure);
                break;
            case Failure:
                setState(Success);
                break;
            case Running:
                setState(Running);
                break;
        }
        mys::display(indent(cerr), getNodeText()) << mys::nl;
        return getState();
    }
}

#endif
