#ifndef ACTIONTEST_H
#define ACTIONTEST_H
#include <../../mystic_lib/include/display.h>
#include <ActionNode.h>
//---------------------------------------------------------------------------------------------------------------------
namespace BT
{
    using namespace mys;
    class ActionTestNode : public ActionNode
    {
    public:
        //---------------------------------------------------------------------------------------------------------------------
        ActionTestNode(const string& name, const int bit_pattern) :
            ActionNode(name), mBits(bit_pattern) {
        }
        ActionTestNode(const ActionTestNode& atn) :
            ActionTestNode { atn.mName, atn.mBits } {
        }
        virtual ~ActionTestNode() = default;

        //---------------------------------------------------------------------------------------------------------------------
        const virtual TreeNode::NodeState eval(Blackboard& bb) override
        {
            Depth depth;
            mys::display(indent(cerr), getNodeId(), getName()) << " start " << mys::nl;

            if (mRunning) {
                setState(Running);
                --i;
            }
            else if ((i & mBits) == mBits) {
                setState(Success);
            }
            else {
                setState(Failure);
            }
            usleep(500);
            ++i;
            mRunning = !mRunning;

            mys::display(indent(cerr), getNodeText()) << mys::nl;
            return getState();
        }

    private:
        int i { };
        int mBits;
        bool mRunning { true };
    };
}

#endif
