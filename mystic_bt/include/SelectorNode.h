#ifndef SelectorNode_H
#define SelectorNode_H
/*
 * SEQUENCE:
 * A sequence control node is an AND of the child nodes it controls. If FAILS if any child FAILS. Only when all return SUCCESS does it return SUCCESS.
 */
#include <CompositeAbstract.h>
//--------------------------------------------------------------------------------------------------------------------------
/*
 *      A selector runs until a child returns success and then returns success. It returns failure if no child returns success.
 *      A running response is returned as running.
 *
 *      Once a success or running response is received the remainder of the children are preempted which allows additional
 *      processing by the following children.
 *
 *      It is an OR operation on the results of the children.
 */

namespace BT {
    //--------------------------------------------------------------------------------------------------------------------------
    class SelectorNode : public CompositeAbstract {
    public:
        SelectorNode(const string& name, TreeVect v);
        SelectorNode(const string& name, const bool reset, TreeVect v);
        const virtual TreeNode::NodeState eval(Blackboard& bb) override;
        virtual ~SelectorNode() = default;

    protected:
        virtual bool isLoopDone(const TreeNode::NodeState s) override {
            return false;
        }

        bool mResetChildren;
    };

}
#endif
