#include <SequenceNode.h>
using namespace BT;
//--------------------------------------------------------------------------------------------------------------------------
namespace BT {

    SequenceNode::SequenceNode(const string& name, TreeVect v) :
            CompositeAbstract(name, v) {
    }
    //--------------------------------------------------------------------------------------------------------------------------
    bool SequenceNode::isLoopDone(const NodeState ns) {
        setState(ns);
        bool res { false };

        switch (ns) {
            case Failure: {
                res = true;
                break;
            }

            case Success: {
                break;
            }

            case Running: {
                res = true;
                break;
            }
        }
        return res;
    }

}
