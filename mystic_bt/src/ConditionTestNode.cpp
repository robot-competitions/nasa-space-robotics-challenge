#include <../../mystic_lib/include/display.h>

#include <ConditionTestNode.h>
//---------------------------------------------------------------------------------------------------------------------
using namespace BT;
namespace BT {

    const BT::TreeNode::NodeState ConditionTestNode::eval(Blackboard& bb) {
        Depth depth;
        mys::display(indent(cerr), getNodeId(), getName()) << " start " << mys::nl;

        setState(Running);
        if ((i & mBits) == mBits) {
            setState(Success);
        }
        else {
            setState(Failure);
        }
        usleep(500);
        ++i;
        mys::display(indent(cerr), getNodeText()) << mys::nl;
        return getState();
    }
}
