#include <MemSelectorNode.h>
using namespace BT;
//--------------------------------------------------------------------------------------------------------------------------
MemSelectorNode::MemSelectorNode(const string& name, TreeVect v) :
        CompositeAbstract(name, v) {
}
//--------------------------------------------------------------------------------------------------------------------------
bool MemSelectorNode::isLoopDone(const NodeState ns) {
    setState(ns);
    bool res { false };
    mHaveRunner = false;

    switch (ns) {
        case Failure: {
            break;
        }

        case Success: {
            res = true;
            break;
        }

        case Running: {
            res = true;
            mHaveRunner = true;
            break;
        }
    }
    return res;
}
//--------------------------------------------------------------------------------------------------------------------------
bool MemSelectorNode::skipSuccessfulChild(const TreeNode::NodeState s) {
    return mHaveRunner && s != Running;
}
