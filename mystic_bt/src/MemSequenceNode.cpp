#include <MemSequenceNode.h>
using namespace BT;
//--------------------------------------------------------------------------------------------------------------------------
MemSequenceNode::MemSequenceNode(const string& name, TreeVect v) :
    CompositeAbstract(name, v) {
}
//--------------------------------------------------------------------------------------------------------------------------
bool MemSequenceNode::isLoopDone(const NodeState ns) {
    bool res { false };
    setState(ns);

    switch (ns) {
        case Failure: {
            res = true;
            break;
        }

        case Success: {
            ++mSuccessCnt;
//            cerr << mSuccessCnt << '\t' << mChildren.size() << '\n';

            if (mSuccessCnt == mChildren.size()) {
                mSuccessCnt = 0;
                resetChildren();
            }
            break;
        }

        case Running: {
            res = true;
            break;
        }
    }
    return res;
}
//--------------------------------------------------------------------------------------------------------------------------
//  In the loop this returns true if a child was previously successful causing the loop to skip over the child.
bool MemSequenceNode::skipSuccessfulChild(const TreeNode::NodeState s) {
    return s == Success;
}
