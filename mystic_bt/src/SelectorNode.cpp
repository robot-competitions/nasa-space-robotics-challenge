#include <../../mystic_lib/include/display.h>
#include <SelectorNode.h>
using namespace BT;
//--------------------------------------------------------------------------------------------------------------------------
namespace BT {
    //--------------------------------------------------------------------------------------------------------------------------
    SelectorNode::SelectorNode(const string& name, TreeVect v) :
        SelectorNode(name, false, v) {
    }
    //--------------------------------------------------------------------------------------------------------------------------
    SelectorNode::SelectorNode(const string& name, const bool reset, TreeVect v) :
        CompositeAbstract(name, v), mResetChildren(reset) {
    }
    //--------------------------------------------------------------------------------------------------------------------------
    const BT::TreeNode::NodeState SelectorNode::eval(Blackboard& bb) {
        Depth depth;
        mys::display(indent(cerr), getNodeId(), getName()) << " start " << mys::nl;

        for (auto c : mChildren) {
            TreeNode& child = c.get();
            mys::display(indent(cerr), getNodeId(), child.getName()) << " process " << mys::nl;

            setState(child.eval(bb));
            if (child.getState() != Failure) {
                break;
            }
        }
        mys::display(indent(cerr), getNodeText()) << mys::nl;

//         exiting with a Success or Failure means the selector is done so reset if flagged to do so
        if (getState() != Running && mResetChildren) {
            resetChildren();
            setState(Failure);
            return Success;
        }
        return getState();
    }

}
