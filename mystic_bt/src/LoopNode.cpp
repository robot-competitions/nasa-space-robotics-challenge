/*
 * LoopNode.cpp
 *
 *  Created on: Mar 6, 2017
 *      Author: rmerriam
 */
#include <../../mystic_lib/include/display.h>
#include <LoopNode.h>

using namespace BT;
//--------------------------------------------------------------------------------------------------------------------------
namespace BT {

    LoopNode::LoopNode(const string& name, TreeVect v, const int32_t& passes) :
        CompositeAbstract(name, v), mPasses(passes) {
    }
    //--------------------------------------------------------------------------------------------------------------------------
    const TreeNode::NodeState LoopNode::eval(Blackboard& bb) {
        Depth depth;
//        mys::display(indent(cerr), getNodeId(), getName()) << " start" << mCnt << mys::nl;
        setState(Success);

        while (mCnt < mPasses && getState() == Success) {
            mys::display(indent(cerr), getNodeId(), getName()) << " cntx " << mCnt << mys::nl;

            setState(CompositeAbstract::eval(bb));

            if (getState() == Running) {
                return getState();
            }
//            else {
//                ++mCnt;
//                mys::display(indent(cerr), getNodeId(), getName()) << " ***** kludge ********** " << mCnt << mys::nl;
//            }
            if (mCnt >= 0) {
                ++mCnt;
            }
            resetChildren();
        }
//        mCnt = 0;
        mys::display(indent(cerr), getNodeText()) << mys::nl;
        return getState();
    }
    //--------------------------------------------------------------------------------------------------------------------------
    bool LoopNode::isLoopDone(const NodeState ns) {
        setState(ns);
        bool res { false };

        switch (ns) {
            case Failure: {
                break;
            }

            case Success: {
                setState(Success);
                break;
            }

            case Running: {
                setState(Running);
                res = true;
                break;
            }
        }
        return res;
    }
    //--------------------------------------------------------------------------------------------------------------------------
    //  In the loop this returns true if a child was previously successful causing the loop to skip over the child.
    bool LoopNode::skipSuccessfulChild(const TreeNode::NodeState s) {
        return s == Success;
    }
}
