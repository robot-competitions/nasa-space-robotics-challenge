/*
 * LoopNode.cpp
 *
 *  Created on: Mar 6, 2017
 *      Author: rmerriam
 */
#include <../../mystic_lib/include/display.h>

#include <CompositeAbstract.h>

using namespace BT;
//--------------------------------------------------------------------------------------------------------------------------
CompositeAbstract::CompositeAbstract(const string& name, TreeVect v) :
    ControlNode(name), mChildren(v) {
}
//--------------------------------------------------------------------------------------------------------------------------
void CompositeAbstract::addChild(TreeNode& child) {
    mChildren.push_back(child);
}
//--------------------------------------------------------------------------------------------------------------------------
const TreeNode::NodeState CompositeAbstract::eval(Blackboard& bb) {
    Depth depth;
    mys::display(indent(cerr), getNodeId(), getName()) << " start " << mys::nl;

    for (auto c : mChildren) {
        TreeNode& child = c.get();
        if (skipSuccessfulChild(child.getState())) {
            mys::display(indent(cerr), getNodeId(), child.getName()) << " skip " << mys::nl;
            setState(Success);
            continue;
        }
        else {
            mys::display(indent(cerr), getNodeId(), child.getName()) << " process " << mys::nl;
            child.eval(bb);
        }
        if (isLoopDone(child.getState())) {
            break;
        }
    }
    mys::display(indent(cerr), getNodeText()) << mys::nl;
    return getState();
}
//--------------------------------------------------------------------------------------------------------------------------
void CompositeAbstract::resetChildren()
{
    Depth depth;
    mys::display(indent(cerr), getNodeId(), getName()) << " reset " << mys::nl;

    for (auto c : mChildren) {
        TreeNode& child = c.get();
        child.reset();
//        cerr << getNodeId() << " " << getName() << " reset " << child.getNodeText() << '\n';
    }
}
