#include <TreeNode.h>
using namespace std;

using namespace BT;
//--------------------------------------------------------------------------------------------------------------------------
int8_t TreeNode::Depth::mDepth { 1 };
//--------------------------------------------------------------------------------------------------------------------------
TreeNode::TreeNode(const string& name, const TreeNode::NodeType type) :
    mName { name }, mType { type } {
}
//--------------------------------------------------------------------------------------------------------------------------
const std::string TreeNode::getNodeText() const {
    string text = getNodeId() + " ";
    text += getName() + " ";
    text += getStateText(mState) + " ";
    return text;
}
//--------------------------------------------------------------------------------------------------------------------------
const std::string& TreeNode::getName() const {
    return mName;
}
//--------------------------------------------------------------------------------------------------------------------------
TreeNode::NodeState TreeNode::getState() const {
    return mState;
}
//--------------------------------------------------------------------------------------------------------------------------
const string& TreeNode::getStateText(const TreeNode::NodeState ns) {
    static const string text[] = { "Failure ", "Success ", "Running " };
    return text[ns];
}
//--------------------------------------------------------------------------------------------------------------------------
const TreeNode::NodeType TreeNode::getType() const {
    return mType;
}
//--------------------------------------------------------------------------------------------------------------------------
const string& TreeNode::getTypeText(const TreeNode::NodeType nt) {
    static const string text[] = { "Action ", "Condition ", "Control " };
    return text[nt];
}
//--------------------------------------------------------------------------------------------------------------------------
void TreeNode::setState(const TreeNode::NodeState new_state) {
    mState = new_state;
}
