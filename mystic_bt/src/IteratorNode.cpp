/*
 * Iterator.cpp
 *
 *  Created on: Mar 6, 2017
 *      Author: rmerriam
 */
#include <IteratorNode.h>

using namespace BT;
//--------------------------------------------------------------------------------------------------------------------------
namespace BT {

    IteratorNode::IteratorNode(const string& name, TreeVect v) :
        CompositeAbstract { name, v } {
    }
    //--------------------------------------------------------------------------------------------------------------------------
    const TreeNode::NodeState IteratorNode::eval(Blackboard& bb) {
        setState(Failure);

        if (CompositeAbstract::eval(bb) == Running) {
            setState(Running);
        }
        else {
            setState(Success);

        }
        return getState();
    }
    //--------------------------------------------------------------------------------------------------------------------------
    //  The iterator only returns when a child is running or it reaches the end of the list of children.
    bool IteratorNode::isLoopDone(const NodeState s) {
        bool res { false };
        if (s == Running) {
            setState(Running);
            res = true;
        }
        return res;
    }
}
