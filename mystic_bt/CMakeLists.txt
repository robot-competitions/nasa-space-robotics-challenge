cmake_minimum_required(VERSION 2.8.3)
project(mystic_bt)
catkin_package()

add_definitions(-std=c++11)  

#configure variables

#configure directories
set (PROJ_MODULE_PATH "${PROJECT_SOURCE_DIR}")
set (PROJ_SRC_PATH  "${PROJ_MODULE_PATH}/src" )
set (PROJ_INCLUDE_PATH  "${PROJ_MODULE_PATH}/include")

#set includes
#include_directories (${PROJ_INCLUDE_PATH} ${THIRD_PARTY_INCLUDE_PATH})

#set sources
file (GLOB LIB_HEADER_FILES "${PROJ_INCLUDE_PATH}/*.h")
file (GLOB LIB_SOURCE_FILES "${PROJ_SRC_PATH}/*.cpp")

include_directories(${roscpp_INCLUDE_DIRS} include)
include_directories(${catkin_INCLUDE_DIRS}  ${PCL_INCLUDE_DIRS} )

message(${PROJ_MODULE_PATH})
message(${PROJ_SRC_PATH})
message(${PROJ_INCLUDE_PATH})
message(${PROJECT_SOURCE_DIR})

#set library
add_library (${PROJECT_NAME} STATIC ${LIB_SOURCE_FILES} ${LIB_HEADER_FILES})
