#include <functional>
#include <iostream>
#include <memory>
#include <string>
#include <bits/unique_ptr.h>
using namespace std;

#include <display.h>

#include <memory>

#include "BehaviorTree.h"
#include <ActionTestNode.h>
#include <ConditionTestNode.h>

using namespace BT;
constexpr float version { 0.1 };
//--------------------------------------------------------------------------------------------------------------------------
ActionTestNode task_1("task 1", 1);
ActionTestNode task_2("task 2", 2);
ActionTestNode task_3("task 3", 1);
ConditionTestNode cond_1("cond 1", 1);

//--------------------------------------------------------------------------------------------------------------------------

//using TreeList = TreeNode[];
//
//TreeList list {
//    &cond_1,
//    &task_1,
//    &task_2,
//    &task_3
//};
//--------------------------------------------------------------------------------------------------------------------------
SelectorNode task_sequence { "task seq",
    {
        cond_1,
        task_1,
        task_2,
        task_3
    } };

//using n_vec = vector<TreeNode>;
//n_vec v {
//    ActionTestNode { "abc", 1 },
//};
//task_2, task_3};

int n = 1;

Blackboard blackboard;
//--------------------------------------------------------------------------------------------------------------------------
std::unique_ptr<TreeNode> rrr {
    new MemSequenceNode { "task seq",
        {
            task_1,
            task_2,
            task_3
        }
    }
};

//--------------------------------------------------------------------------------------------------------------------------
void test_seq() {
    cerr << "test_seq" << endl;
//    loop.eval(blackboard);

    for (auto i = 0; i < 64; ++i) {
        rrr->eval(blackboard);
        cerr << endl;
    }
}
//--------------------------------------------------------------------------------------------------------------------------
int main(int argc, char **argv) {
//    std::cout.setstate(std::ios_base::badbit);

    test_seq();
    return 0;
}
