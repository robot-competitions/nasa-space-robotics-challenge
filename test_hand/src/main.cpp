#include "mystic.h"
#include <thread>
#include <iostream>
using namespace std;
using namespace mys;
#include <RPY.h>
#include <HandTraj.h>
#include <GoHome.h>
//---------------------------------------------------------------------------------------------------------------------
constexpr float version { 0.1 };
//---------------------------------------------------------------------------------------------------------------------
void test_hand()
{
    ROS_INFO_STREAM(ros::this_node::getName() << " Test Hand");

    HandTraj right_hand { mys::eRight };
    HandTraj left_hand { mys::eLeft };

    right_hand.delayTime(0.75);
    right_hand.moveTime(0.5);

    left_hand.delayTime(0.75);
    left_hand.moveTime(1.0);

    tf2::Vector3 pos { 0.4, -0.12, 0.93 };
//    tf2::Vector3 pos { 0.4, -0.12, 0.93 };
    tf2::Vector3 rot { 0.0, -0.0, 0.0 };

//    right_hand.update();
//    right_hand.setPose(pos, rot, true);
//    right_hand.send();

//    pos.setY(0.2);
//    pos.setZ(0.6);
//    left_hand.update();
//    left_hand.setPose(pos, rot, true);
//    left_hand.send();

    double roll { deg2rad( -0.0) };  // palm tip
    double pitch { deg2rad( -00.0) };  // wrist roll
    double yaw { deg2rad(0.0) };   // palm wave
    RPY rpy { roll, pitch, yaw };

    right_hand.update();
    cerr << "send ";
    display(cerr, right_hand.getTranslation());
    display(cerr, right_hand.getRPY());
    display(cerr, rad2deg(right_hand.getRPY())) << nl;

    right_hand.setPose(right_hand.getTranslation(), tf2::Vector3 { 0.0005, -0.0000, -0.0452 }, true);
//    right_hand.setPose(tf2::Vector3 { 0., -0.2, 1.4 }, tf2::Vector3 { roll, pitch, yaw }, true);
    right_hand.send();

    for (auto i = 0; i < 30; i++) {
        Duration(1).sleep();

        right_hand.update();
        cerr << "transform ";
        display(cerr, right_hand.getTranslation());
//        display(cerr, transform.getRotation()) << nl;
        display(cerr, right_hand.getRPY());
        display(cerr, rad2deg(right_hand.getRPY())) << nl;
    }
//    for (double r = -roll; r <= roll; r += 0.2) {
//
//        for (double p = -pitch; p <= pitch; p += 0.5) {
//            for (double y = -yaw; y <= yaw; y += 0.2) {
//
//                right_hand.update();
//                right_hand.incPose(zero_vec3, tf2::Vector3 { 0.2, p, y }, true);
//                right_hand.send();
//
//                mys::display(cerr, r, p, y) << mys::nl;
////                ::sleep(4);
//            }
//        }
//    }

//    double val { -0.1 };
//    for (auto i = 0; i < 20; ++i) {
//        if (i == 5) {
//            val = -val;
//        }
//        right_hand.update();
//        right_hand.incPose(zero_vec3, tf2::Vector3 { 0, 0, val }, true);
//        right_hand.send();
//    }
//    ::sleep(2);
    ROS_INFO_STREAM(ros::this_node::getName() << " Test Hand Done");
}
//---------------------------------------------------------------------------------------------------------------------
void home_all() {
    GoHome gh;
    cerr << "home all start" << tab;
    gh.setPose(mys::eLeft, GoHome::eARM, 0.0);
    gh.send();
    gh.setPose(mys::eRight, GoHome::eARM, 0.0);
    gh.send();
    gh.setPose(mys::eRight, GoHome::eCHEST, 0.0);
    gh.send();
    gh.setPose(mys::eRight, GoHome::ePELVIS, 1.0);
    gh.send();
    cerr << "end" << nl;
}
//---------------------------------------------------------------------------------------------------------------------
int main(int argc, char** argv) {
    init(argc, argv, "test_hand_home");
    NodeHandle nh;

    Time::waitForValid();

    ROS_INFO_STREAM(ros::this_node::getName() << " initialized v." << version);

    AsyncSpinner spinner(0);
    spinner.start();

    cerr << std::fixed << std::setprecision(4);
    ros::Time::sleepUntil(ros::Time(26));

//    home_all();

    test_hand();

//    home_all();
    return 0;
}
