/*
 * WalkToDoor.cpp
 *
 *  Created on: Dec 8, 2016
 *      Author: rmerriam
 */
#include "mystic.h"

#include "HeadTraj.h"
#include "NeckTraj.h"
#include "TestHead.h"

#include "mystic.h"
//---------------------------------------------------------------------------------------------------------------------
TestHead::TestHead(HeadTraj& ht, NeckTraj& nt) :
        mHeadTraj(ht), mNeckTraj(nt) {
    ROS_INFO_STREAM(ros::this_node::getName() << " TestHead constructor");
}
//---------------------------------------------------------------------------------------------------------------------
void TestHead::headPosTest() {
    ROS_INFO_STREAM(ros::this_node::getName() << " TestHead");
    double pos = 0.5;
    mHeadTraj.delayTime(1.5);
    mHeadTraj.setPose(0, 0, 0);
    mHeadTraj.send();
    mHeadTraj.setPose(0, pos, 0);
    mHeadTraj.send();
    mHeadTraj.setPose(pos, 0.0, 0);
//    mHeadTraj();

    mHeadTraj.setPose(0.0, pos, pos);
    mHeadTraj.send();
}
//---------------------------------------------------------------------------------------------------------------------
void TestHead::resetNeck() {
    mNeckTraj.setPose(0, 0, 0);
    mNeckTraj.send();
}
//---------------------------------------------------------------------------------------------------------------------
void TestHead::neckPosTest() {
    ROS_INFO_STREAM(ros::this_node::getName() << " TestHead");

    mNeckTraj.delayTime(0.75);

    double lo_pos = 0.0;
    double yaw = 1.0472;

    resetNeck();
//    mNeckTraj.setPose(lo_pos, 0, 0);
//    mNeckTraj.send();

    for (double pos = -yaw; pos <= yaw; pos += 0.25) {
        double hi = 0.0;
//
//        for (double hi = -0.872; hi <= 0; hi += 0.2) {
        mNeckTraj.setPose(lo_pos, pos, hi);
        mNeckTraj.send();
        display(lo_pos, pos, hi);
        cerr << nl;
//        }
    }
    //    mNeckTraj.setPose(pos, 0.0, 0);
    //    mNeckTraj();

//    mNeckTraj.setPose(pos, pos, 0);
//    mNeckTraj();
//
//    mNeckTraj.setPose(pos, pos, -pos);
//    mNeckTraj();
}
//---------------------------------------------------------------------------------------------------------------------
void TestHead::operator ()() {

    resetNeck();

//    neckPosTest();
//    headPosTest();

    ROS_INFO_STREAM(ros::this_node::getName() << " TestHead complete");
}
