/*
 * Application.h
 *
 *  Created on: Dec 8, 2016
 *      Author: rmerriam
 */

#ifndef SRC_APPLICATION_H_
#define SRC_APPLICATION_H_

#include "TestHead.h"
//---------------------------------------------------------------------------------------------------------------------
class Application {
public:
    Application() = default;

    void operator ()();

private:
    HeadTraj mHeadTraj;
    NeckTraj mNeckTraj;
};
#endif /* SRC_APPLICATION_H_ */
