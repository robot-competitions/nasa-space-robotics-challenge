/*
 * ActionBase.h
 *
 *  Created on: Dec 31, 2016
 *      Author: rmerriam
 */

#ifndef ACTIONBASE_H_
#define ACTIONBASE_H_

class ActionBase {
public:

protected:
    Rate mRate { 1000 / 50 };    // 20 msec
};




#endif /* ACTIONBASE_H_ */
