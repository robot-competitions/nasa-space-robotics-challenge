/*
 * WalkToDoor.h
 *
 *  Created on: Dec 8, 2016
 *      Author: rmerriam
 */

#ifndef SRC_WalkSteps_H_
#define SRC_WalkSteps_H_

#include "ActionBase.h"
#include "HeadTraj.h"
#include "NeckTraj.h"
//---------------------------------------------------------------------------------------------------------------------
class TestHead : public ActionBase {
public:
    TestHead(HeadTraj& ht, NeckTraj& nt);

    ~TestHead() {
    }
    void operator ()();

private:
    HeadTraj& mHeadTraj;
    NeckTraj& mNeckTraj;

    void headPosTest();
    void neckPosTest();
    void resetNeck();
};

#endif /* SRC_WalkSteps_H_ */

