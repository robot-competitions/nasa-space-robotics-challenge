/*
 * Application.cpp
 *
 *  Created on: Dec 8, 2016
 *      Author: rmerriam
 */
#include "mystic.h"
#include <tf2_ros/transform_listener.h>

#include "TestHead.h"
#include "Application.h"
//---------------------------------------------------------------------------------------------------------------------
//  Get the current position of the foot
//
geometry_msgs::Transform getTransform(const string& frame_name) {
    tf2_ros::Buffer tfBuffer;
    tf2_ros::TransformListener tfListener { tfBuffer };

    geometry_msgs::TransformStamped transformStamped;

// transform often throws exceptions despite valid input - just loop until it gets it right
    do {
        try {
            transformStamped = tfBuffer.lookupTransform("world", frame_name, Time(0), Duration(0.02));
        }
        catch (...) {
        }
    } while (transformStamped.child_frame_id.size() == 0);

    geometry_msgs::Transform t(transformStamped.transform);

    return t;
}
//---------------------------------------------------------------------------------------------------------------------
void Application::operator ()() {

    ROS_INFO_STREAM(ros::this_node::getName() << " starting run");
    cerr << fixed << setprecision(3);

    TestHead th(mHeadTraj, mNeckTraj);

    ROS_INFO_STREAM(ros::this_node::getName() << " creations done");

    ros::Time::sleepUntil(ros::Time(27));
    ROS_INFO_STREAM(ros::this_node::getName() << " sim time ready");

//    th();

    geometry_msgs::Transform t = getTransform("torso");

    tf2::Transform tx;
    fromMsg(t, tx);
    tf2::Quaternion qtx = tx.getRotation();

    tf2::Matrix3x3 mx(qtx);

    double roll;
    double pitch;
    double yaw;
    mx.getRPY(roll, pitch, yaw);

    cerr << roll << " " << pitch << " " << yaw << " " << endl;


//    geometry_msgs::Quaternion r = t.rotation;
//    tf2::Quaternion q(r.x, r.y, r.z, r.w);
//    Vector3 v3 = q.angle(q)
//    v3.normalize();
//    cerr << v3.getX() << " " << v3.getY() << " " << v3.getZ() << " " << endl;





    ROS_INFO_STREAM(ros::this_node::getName() << " shutdown at end");

    ros::shutdown();
}
