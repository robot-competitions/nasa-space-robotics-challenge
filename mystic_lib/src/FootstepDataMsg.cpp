/*
 * FootstepDataMsg.cpp
 *
 *  Created on: Nov 14, 2016
 *      Author: rmerriam
 */
//---------------------------------------------------------------------------------------------------------------------
#include "mystic.h"
#include "FootstepDataMsg.h"
//---------------------------------------------------------------------------------------------------------------------
void FootstepDataMsg::alignPose(const ::geometry_msgs::Quaternion& q) {
    Quaternion q_yaw(geo2tf2(q));
    q_yaw.normalize();
    mFootstep.orientation = tf2geo(q_yaw);
}
//---------------------------------------------------------------------------------------------------------------------
void FootstepDataMsg::incLocation(const Vec3& res) {
    mFootstep.location.x += res.getX();
    mFootstep.location.y += res.getY();
    mFootstep.location.z += res.getZ();
}
//---------------------------------------------------------------------------------------------------------------------
void FootstepDataMsg::setLocation(const Vec3& vec) {

    Matrix3x3 m(getTransform().getRotation());
    Vec3 res = m * vec;

    incLocation(res);
}
//---------------------------------------------------------------------------------------------------------------------
void FootstepDataMsg::setRotation(const double yaw, FootstepDataMsg& r_fdm) {
    using namespace mys;

    FootstepDataMsg& l_fdm = *this;

    Vec3 l_base { l_fdm.getLocation() };
    Vec3 r_base { r_fdm.getLocation() };

    // calculate distance between feet - make sure it is a reasonable distance
    Vec3 spread = (l_base - r_base);
    double radius = sqrt(pow(spread.getX(), 2) + pow(spread.getY(), 2)) / 2.0;

    // calculate the new location of the feet
    Vec3 l_offset { -radius * sin(yaw), radius * (1 - cos(yaw)), 0 };
    Vec3 r_offset = -l_offset;

    // get current yaw angle from orientation of one foot - adjust the sign based on the Z of the quat
    double cur_yaw { copysign(l_fdm.getOrientation().getAngle(), l_fdm.getOrientation().getZ()) };

    // use the current and requested yaw to create new orientation quat
    Quaternion q_yaw;
    q_yaw.setRPY(0, 0, yaw + cur_yaw);
    q_yaw.normalize();

    // set the orientation of the feet
    l_fdm.mFootstep.orientation = tf2geo(q_yaw);
    r_fdm.mFootstep.orientation = tf2geo(q_yaw);

    // calculate the positions of the feet after the turn - set in footsteps
    Matrix3x3 m(q_yaw);
    l_offset = m * l_offset;
    r_offset = m * r_offset;

    l_fdm.incLocation(l_offset);
    r_fdm.incLocation(r_offset);
}
//---------------------------------------------------------------------------------------------------------------------
void FootstepDataMsg::update(const string& rebase) {
    const Transform& t = WorldFrame::update(rebase);

    mFootstep.robot_side = mSide;
    mFootstep.origin = FootstepDataRosMessage::AT_ANKLE_FRAME;
    mFootstep.swing_height = mStepHeight;
    mFootstep.trajectory_type = mTrajectoryType;

    mFootstep.location = tf2geo(t.getOrigin());
    tf2::convert(t.getRotation(), mFootstep.orientation);
}
