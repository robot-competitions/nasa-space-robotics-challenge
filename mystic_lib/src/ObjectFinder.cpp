/*
 * ObjectFinder.cpp
 *
 *  Created on: Apr 18, 2017
 *      Author: rmerriam
 */

#include <mystic.h>
using namespace mys;

#include <open_cv.h>
using namespace cv;
#include "ObjectFinder.h"
//---------------------------------------------------------------------------------------------------------------------
int16_t ObjectFinder::zeroLimit(int16_t value) {
    if (value < 0) {
        value = 0;
    }
    else if (value > 255) {
        value = 255;
    }
    return value;
}
//---------------------------------------------------------------------------------------------------------------------
Vec3b ObjectFinder::colors() {
    return mLower;
}
//---------------------------------------------------------------------------------------------------------------------
Mat ObjectFinder::findByColor(const Mat& img) {
    constexpr int morph_elem = MORPH_RECT;
    constexpr int morph_size = 4;
    static const Mat element = getStructuringElement(morph_elem, Size(morph_size, morph_size));

    Mat hsv;
    cvtColor(img, hsv, COLOR_BGR2HSV);
    Mat bounded;
    inRange(hsv, mLower, mUpper, bounded);
    morphologyEx(bounded, mObjects, MORPH_GRADIENT, element, Point( -1, -1), 1);
    return mObjects;
}
//---------------------------------------------------------------------------------------------------------------------
const Contours& ObjectFinder::process(const Mat& img) {
    findByColor(img);
    findContours(mObjects, mContours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
    return mContours;
}
//---------------------------------------------------------------------------------------------------------------------
//const Contours& ObjectFinder::process(const ImageVision& orig, const Mat& merge) {
//
//    Mat object = findByColor(orig.image()) | merge;
//    findContours(object, mContours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
//    return mContours;
//}

