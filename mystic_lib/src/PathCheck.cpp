/*
 * PathCheck.cpp
 *
 *  Created on: Apr 26, 2017
 *      Author: rmerriam
 */

#include <open_cv.h>
using namespace cv;

#include <mystic.h>
using namespace mys;
#include "PathCheck.h"
//---------------------------------------------------------------------------------------------------------------------
bool PathCheck::isPathClear() {
    // the end pixel is the farthest white pixel along the center but the path to it might not be clear at the edges.
    // check if the rectangle to the end pixel is all white, if not shorten the rectangle until an all white path is
    // found
    bool res = false;
//    cerr << " PathCheck " << mEndPixel << nl;
    while (mEndPixel > 1) {
//        cerr << mEndPixel << nl;

        Rect walk_area(0, 0, mPath.cols, mEndPixel);
        Mat roi = mPath(walk_area);
//        cerr << " roi " << nl;

        ImageVision roi_iv(roi);
        updateWindow(roi_iv.flipped(), "walk area");

        if ((roi.cols * roi.rows * 0.98) - countNonZero(roi) <= 0) {
            res = true;
            break; // done!!
        }
        --mEndPixel;
    }
    return res;
}
//---------------------------------------------------------------------------------------------------------------------
void PathCheck::findPathEndPixel() {
    Mat col { mPath.col(mPath.cols / 2) };
    mEndPixel = 1;

    for (; mEndPixel < col.rows; ++mEndPixel) {
        const auto x = col.ptr<uint8_t>(mEndPixel);
        if (x[0] != 255) {
            break;
        }
    }
    --mEndPixel;
}
//---------------------------------------------------------------------------------------------------------------------
void PathCheck::getCamFramePosition() {
    ImageVision iv(mCloudSub.pointCloud());
    int iv_mid { iv.image().cols / 2 };

    //  get camera frame position of end of path pixel
    const PCloud& pcl { mCloudSub.pointCloud() };
    pcl::PointXYZRGB pt = pcl.at(iv_mid, mEndPixel);

    // sometimes that point cloud position is nan, scan back until find a value
    for (; mEndPixel > 0 && (isnan(pt.x)); --mEndPixel) {
        pt = pcl.at(iv_mid, mEndPixel);
    }
    cerr << "path loc ";
    display(cerr, pt.z, pt.x, pt.y) << tab;
    mVisionLoc = Vector3 { pt.z, pt.x, pt.y };

    mWorldLoc = getWorldLocation(pt.x, pt.y, pt.z).getOrigin();
    display(cerr, mWorldLoc) << nl;
}
//---------------------------------------------------------------------------------------------------------------------
void PathCheck::processImage() {
    constexpr int thresh = 1;

    // get latest image from point cloud
    ImageVision iv(mCloudSub.pointCloud());

    // get grayscale ROI to examine that is straight ahead of camera
    ImageVision gray(iv.grayMask(40, 60));
    Mat g_img = gray.image();

    updateWindow(gray.flipped(), "gray");

    mSubImg = Rect(g_img.cols * col_offset, 0, g_img.cols - (g_img.cols * 2 * col_offset), g_img.rows * (row_offset));
    Mat roi = gray.ROI(mSubImg);

//    ImageVision roi_iv(roi);
//    updateWindow(roi_iv.flipped(), "ROI");

//    cerr << " morph ";
    constexpr int morph_elem = 0; // 0: Rect - 1: Cross - 2: Ellipse
    constexpr int morph_size = 4;
    Mat element = getStructuringElement(morph_elem, Size(2 * morph_size + 1, 2 * morph_size + 1), Point(morph_size, morph_size));
    morphologyEx(roi, mPath, MORPH_GRADIENT, element, Point( -1, -1), 1);

//    cerr << " threshold ";
    threshold(mPath, mPath, thresh, 255, THRESH_BINARY);

//    cerr << " path " << nl;

//    ImageVision mPath_iv(mPath);
//    updateWindow(mPath_iv.flipped(), "path");
//    cerr << " done " << nl;

    Mat g2;
    Canny(mPath, g2, 0, 255, 3);

//    ImageVision g2_iv(g2);
//    updateWindow(g2_iv.flipped(), "canny");

//    vector<Vec4i> lines;
//    HoughLinesP(g2, lines, 1, CV_PI / 180, 80, 0, 20);
//    cerr << "lines " << lines.size() << nl;
//
//    Mat hl(mPath.rows, mPath.cols, CV_8UC3, Scalar(0, 0, 0));
//
//    for (size_t i = 0; i < lines.size(); i++) {
//        int x1 { lines[i][0] };
//        int y1 { lines[i][1] };
//        int x2 { lines[i][2] };
//        int y2 { lines[i][3] };
//        line(hl,
//             Point(x2, y2),
//             Point(x1, y1),
//             Scalar(0, 127, 0), 1, 8);
//
//        display(cerr, int((180.0 / M_PI) * atan2(x2 - x1, y2 - y1)));
//
//        display(cerr, lines[i]) << nl;
//    }
//
//    ImageVision hl_iv(hl);
//    updateWindow(hl_iv.flipped(), "hl");

}
