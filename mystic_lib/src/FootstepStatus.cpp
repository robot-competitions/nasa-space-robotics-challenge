/*
 * IsDoubleSupport.cpp
 *
 *  Created on: Dec 28, 2016
 *      Author: rmerriam
 */
#include "mystic.h"
#include <RobotPose.h>

#include <std_msgs/Bool.h>
#include "FootstepStatus.h"

FootstepStatus::FootstepStatus(const char* msg) :
        SubscribeTopic { msg } {
}

//---------------------------------------------------------------------------------------------------------------------
void FootstepStatus::process() {
    mStepCnt -= mSubscribeTopic.status;
}

