/*
 * ImageVision.cpp
 *
 *  Created on: Mar 19, 2017
 *      Author: rmerriam
 */
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
using namespace cv;
#include <cv_bridge/cv_bridge.h>

#include <mystic.h>

#include <algorithm>
using namespace std;

#include "ImageVision.h"
//---------------------------------------------------------------------------------------------------------------------
ImageVision::ImageVision(const Mat img, uint8_t conversion) :
    ImageVision(img) {
    mImage.create(img.size(), img.type());
    cvtColor(img, mImage, conversion);
}
//---------------------------------------------------------------------------------------------------------------------
ImageVision::ImageVision(const Mat img) :
        mImage(img) {
}
//---------------------------------------------------------------------------------------------------------------------
Mat& ImageVision::image() {
    return mImage;
}
//---------------------------------------------------------------------------------------------------------------------
ImageVision::ImageVision(const PCloud& cloud, const string encoding) {
    // convert cloud to a ROS image in a message, then convert to OpenCV image
    Image pc_msg;
    pcl::toROSMsg(cloud, pc_msg);
    mImage = cv_bridge::toCvCopy(pc_msg, encoding)->image;
}
//---------------------------------------------------------------------------------------------------------------------
const cv::Mat ImageVision::flipped() const {
    Mat matRotation = getRotationMatrix2D(Point(mImage.cols / 2, mImage.rows / 2), 180, 1);
    Mat imgRotated;
    warpAffine(mImage, imgRotated, matRotation, mImage.size());
    return imgRotated;
}
//---------------------------------------------------------------------------------------------------------------------
const cv::Mat ImageVision::grayMask(const uint8_t min_value, const uint8_t max_value) {
    Mat gray_img(mImage.size(), CV_8UC1, 0.0);
    cvtColor(mImage, gray_img, CV_BGR2GRAY, 1);

    MatIterator_<uint8_t> it2 = gray_img.begin<uint8_t>();
    MatConstIterator_<Vec3b> it = mImage.begin<Vec3b>();
    MatConstIterator_<Vec3b> it_end = mImage.end<Vec3b>();

    for (; it != it_end; ++it, ++it2) {
        const Vec3b& pix = *it;
        int mx = max( { pix[0], pix[1], pix[2] });
        int mn = min( { pix[0], pix[1], pix[2] });
        if (abs(mx - mn) > 2 || mn <= min_value || mx >= max_value) {
            *it2 = 0xFF;
        }
        else {
            *it2 = ( *it2 * 10) / 10;
        }
    }
    return gray_img;
}
