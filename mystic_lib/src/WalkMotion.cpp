/*
 * WalkMotion.cpp
 *
 *  Created on: Dec 28, 2016
 *      Author: rmerriam
 */
#include "mystic.h"

#include <std_msgs/String.h>
#include "WalkMotion.h"
//---------------------------------------------------------------------------------------------------------------------
WalkMotion::WalkMotion(const char* msg) :
        SubscribeTopic(msg) {
}
//---------------------------------------------------------------------------------------------------------------------
WalkMotion::~WalkMotion() {
}
//---------------------------------------------------------------------------------------------------------------------
void WalkMotion::waitForStanding() {
    while (isNotStanding()) {
        mRate.sleep();
    }
}
//---------------------------------------------------------------------------------------------------------------------
void WalkMotion::waitForNotStanding() {
    while (isStanding()) {
        mRate.sleep();
    }
}
//---------------------------------------------------------------------------------------------------------------------
void WalkMotion::process() {
    mMotionState = mSubscribeTopic.data == "STANDING";
}
