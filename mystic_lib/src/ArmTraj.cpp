#include <ihmc_msgs/TrajectoryPoint1DRosMessage.h>
#include <ihmc_msgs/OneDoFJointTrajectoryRosMessage.h>
using namespace ihmc_msgs;

#include "ArmTraj.h"
static TrajectoryPoint1DRosMessage empty_point { };
static OneDoFJointTrajectoryRosMessage empty_traj { };

//---------------------------------------------------------------------------------------------------------------------
ArmTraj::ArmTraj(const mys::Side side, const char* msg) :
    PublishTemplate(msg), mSide { side } {
    mTopic.joint_trajectory_messages.resize(eWristY + 1);
    mTopic.robot_side = mSide;
    mTopic.execution_mode = ArmTrajectoryRosMessage::OVERRIDE;
}
//---------------------------------------------------------------------------------------------------------------------
void ArmTraj::doMove() {

    // if any joints don't have a position then put an empty one there
    for (auto& msg : mTopic.joint_trajectory_messages) {

        if (msg.trajectory_points.empty()) {
            msg.trajectory_points.push_back(empty_point);
        }
    }
    send();
}
//---------------------------------------------------------------------------------------------------------------------
void ArmTraj::setPose(const ArmPart part, const double& position) {
    OneDoFJointTrajectoryRosMessage& traj = mTopic.joint_trajectory_messages[part];

    // only has two members
    traj.unique_id = id();
    traj.trajectory_points.push_back(empty_point);

    // has four members
    TrajectoryPoint1DRosMessage& pt = traj.trajectory_points.back();

    pt.unique_id = id();
    pt.time = mDelayTime;
    pt.position = position;
    pt.velocity = 0.0;  // for completeness
}
//---------------------------------------------------------------------------------------------------------------------
void ArmTraj::sendMessage()
{
    mPublisher.publish(mTopic);
//    Duration(mDelayTime).sleep();
}
//---------------------------------------------------------------------------------------------------------------------
void ArmTraj::startMove() {

    // get rid of all previous points
    for (auto& msg : mTopic.joint_trajectory_messages) {
        msg.trajectory_points.clear();
    }
    // give each joint an empty set of data
    for (auto& msg : mTopic.joint_trajectory_messages) {
        msg = empty_traj;
    }
}
