/*
 * Walk.cpp
 *
 *  Created on: Nov 14, 2016
 *      Author: rmerriam
 */
//---------------------------------------------------------------------------------------------------------------------
#include "mystic.h"
#include <std_msgs/String.h>
#include "FootstepDataMsg.h"
#include "Walk.h"
//---------------------------------------------------------------------------------------------------------------------
Walk::Walk(const char* msg) :
    PublishTemplate { msg } {
    startSteps();

    mStepList.default_transfer_time = mTransfer;
    mStepList.default_swing_time = mSwing;
    mStepList.execution_mode = 0;
    mStepList.unique_id = -1;
}
//---------------------------------------------------------------------------------------------------------------------
void Walk::alignFeet() {
    FootstepDataMsg& l_fdm = mFootstepDataMsg[mys::eLeft];
    FootstepDataMsg& r_fdm = mFootstepDataMsg[mys::eRight];

    l_fdm.setRotation(l_fdm.getOrientation().getAngle(), r_fdm);
}
//---------------------------------------------------------------------------------------------------------------------
void Walk::incStep(const mys::Side side, const Vec3& vec) {

    FootstepDataMsg& fdm = mFootstepDataMsg[side];

    fdm.setLocation(Vec3 { vec });
    mStepList.footstep_data_list.push_back(fdm.getMsg());
}
//=====================================================================================================================
void Walk::incStep(const mys::Side side, const double x, const double y, const double z) {
    incStep(side, Vec3 { x, y, z });
}
//---------------------------------------------------------------------------------------------------------------------
void Walk::sendMessage() {

    mStepCnt = mStepList.footstep_data_list.size();
    mPublisher.publish(mStepList);
    mRate.sleep();
    mRate.sleep();
}
//---------------------------------------------------------------------------------------------------------------------
void Walk::setStep(const mys::Side side, const Vec3& vec) {
    FootstepDataMsg& fdm = mFootstepDataMsg[side];
    fdm.setLocation(Vec3 { vec - fdm.getLocation() });
    mStepList.footstep_data_list.push_back(fdm.getMsg());
}
//---------------------------------------------------------------------------------------------------------------------
void Walk::updateByFrame(const string& rebase) {
    mStepList.footstep_data_list.clear();
    mFootstepDataMsg[mys::eLeft].get().update(rebase);
    mFootstepDataMsg[mys::eRight].get().update(rebase);
}
//---------------------------------------------------------------------------------------------------------------------
//  clear out previous steps and reset current foot position
void Walk::startSteps() {
    mStepList.footstep_data_list.clear();
    mFootstepDataMsg[mys::eLeft].get().update();
    mFootstepDataMsg[mys::eRight].get().update();
}
//---------------------------------------------------------------------------------------------------------------------
void Walk::transfer(const double t) {
    mTransfer = t;
    mStepList.default_transfer_time = mTransfer;
    mStepList.final_transfer_time = -1;
}
//---------------------------------------------------------------------------------------------------------------------
void Walk::turn(const double yaw) {
    FootstepDataMsg& l_fdm = mFootstepDataMsg[mys::eLeft];
    FootstepDataMsg& r_fdm = mFootstepDataMsg[mys::eRight];

    l_fdm.setRotation(yaw, r_fdm);

    if (yaw > 0) {
        mStepList.footstep_data_list.push_back(l_fdm.getMsg());
        mStepList.footstep_data_list.push_back(r_fdm.getMsg());
    }
    else {
        mStepList.footstep_data_list.push_back(r_fdm.getMsg());
        mStepList.footstep_data_list.push_back(l_fdm.getMsg());
    }
}
//---------------------------------------------------------------------------------------------------------------------
void Walk::waitForFinished() {
    static const string status("/ihmc_ros/valkyrie/output/robot_motion_status");
    string s;
    s = "bad";
    while (s != "STANDING") {
        std_msgs::StringConstPtr s_const = ros::topic::waitForMessage<std_msgs::String>(status);
        s = s_const->data;
        mRate.sleep();
    }
    mStepCnt = 0;
}
