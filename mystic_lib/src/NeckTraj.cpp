/*
 * MoveArm.cpp
 *
 *  Created on: Dec 6, 2016
 *      Author: rmerriam
 */
//---------------------------------------------------------------------------------------------------------------------
#include "mystic.h"

#include <ihmc_msgs/OneDoFJointTrajectoryRosMessage.h>
using namespace ros;
#include "NeckTraj.h"
//---------------------------------------------------------------------------------------------------------------------
NeckTraj::NeckTraj(const char* msg) :
    PublishTemplate { msg } {
}
//---------------------------------------------------------------------------------------------------------------------
void NeckTraj::setOneDeg(const double& pos, OneDoFJointTrajectoryRosMessage& traj) {
    TrajectoryPoint1DRosMessage one_deg;
    traj.trajectory_points.clear();

    one_deg.time = mMoveTime;
    one_deg.position = pos;
    one_deg.unique_id = ++mId;

    traj.trajectory_points.push_back(one_deg);
}
//---------------------------------------------------------------------------------------------------------------------
void NeckTraj::setPose(const double& lo_pitch, const double& yaw, const double& hi_pitch) {
    OneDoFJointTrajectoryRosMessage traj;

    traj.unique_id = ++mId;

    setOneDeg(lo_pitch, traj);
    mTopic.joint_trajectory_messages.push_back(traj);

    setOneDeg(yaw, traj);
    mTopic.joint_trajectory_messages.push_back(traj);

    setOneDeg(hi_pitch, traj);
    mTopic.joint_trajectory_messages.push_back(traj);
}
//---------------------------------------------------------------------------------------------------------------------
void NeckTraj::sendMessage() {
    publisher().publish(mTopic);
    ROS_DEBUG_STREAM(ros::this_node::getName() << mMsg << " sent ");
    mTopic.joint_trajectory_messages.clear();
}
