#include "mystic.h"
using namespace ros;

#include <srcsim/Satellite.h>

#include "SatelliteDishStatus.h"
//---------------------------------------------------------------------------------------------------------------------
SatelliteDishStatus::SatelliteDishStatus(const char* msg) :
    SubscribeTopic { msg } {
    ROS_DEBUG_STREAM(ros::this_node::getName() << " ready to subscribe " << msg);
}
