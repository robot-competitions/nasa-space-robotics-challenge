/*
 * MoveArm.cpp
 *
 *  Created on: Dec 6, 2016
 *      Author: rmerriam
 */
//---------------------------------------------------------------------------------------------------------------------
#include "mystic.h"

#include "HandTraj.h"

#include <ihmc_msgs/SE3TrajectoryPointRosMessage.h>
//---------------------------------------------------------------------------------------------------------------------
const string HandTraj::mFrameName[2] { "leftPalm", "rightPalm" };
//---------------------------------------------------------------------------------------------------------------------
HandTraj::HandTraj(const mys::Side side, const char* msg) :
    PublishTemplate { msg }, WorldFrame { mFrameName[side], "world" }, mSide { side } {

    ROS_DEBUG_STREAM(ros::this_node::getName() << " " << typeid(*this).name() << " " << msg);

    mTopic.robot_side = mSide;
    mTopic.base_for_control = HandTrajectoryRosMessage::CHEST;
//    mTopic.base_for_control = HandTrajectoryRosMessage::WORLD;
    mTopic.base_for_control = HandTrajectoryRosMessage::WALKING_PATH;
    mTopic.execution_mode = HandTrajectoryRosMessage::OVERRIDE;
}
//---------------------------------------------------------------------------------------------------------------------
void HandTraj::incPose(const tf2::Vector3& pos, const tf2::Vector3& rot, const bool move_delay) {
    setPose(getTranslation() + pos, getRPY() + rot, move_delay);
}
//---------------------------------------------------------------------------------------------------------------------
void HandTraj::setPose(const tf2::Vector3& pos, const tf2::Vector3& rot, const bool move_delay) {
    Vec3 offset { 0.0, 0.0, 0.0 };

    mTopic.unique_id = ++mId;

    SE3TrajectoryPointRosMessage traj;

    traj.unique_id = ++mId;

    traj.time = mMoveTime;
    traj.linear_velocity = zero_gvec;
    traj.angular_velocity = zero_gvec;

    traj.position = tf2geo(pos + offset);

    Quaternion q_rot;
    q_rot.setRPY(rot.getX(), rot.getY(), rot.getZ());
    traj.orientation = tf2geo(q_rot);

    mTopic.taskspace_trajectory_points.push_back(traj);
}
//---------------------------------------------------------------------------------------------------------------------
void HandTraj::sendMessage() {

    publisher().publish(mTopic);
    mTopic.taskspace_trajectory_points.clear();

    ROS_DEBUG_STREAM(ros::this_node::getName() << " Hand Moved ");
    Duration(mDelayTime).sleep();
}
