/*
 * MoveHead.cpp
 *
 *  Created on: Nov 29, 2016
 *      Author: rmerriam
 */
#include "mystic.h"
#include <PelvisHeightTraj.h>
//---------------------------------------------------------------------------------------------------------------------
void PelvisHeightTraj::sendMessage() {

    TrajectoryPoint1DRosMessage pt;

    pt.unique_id = ++mId;
    pt.time = mDelayTime;
    pt.velocity = 0.0;
    pt.position = mHeight;

    mTopic.execution_mode = PelvisHeightTrajectoryRosMessage::OVERRIDE;
    mTopic.previous_message_id = 0;
    mTopic.trajectory_points.push_back(pt);

    mPublisher.publish(mTopic);
}
