/*
 * RobotPose.cpp
 *
 *  Created on: Dec 11, 2016
 *      Author: rmerriam
 */
#include "mystic.h"
using namespace mys;
#include <RobotPose.h>

//---------------------------------------------------------------------------------------------------------------------
RobotPose::RobotPose(const char* msg) :
        SubscribeTopic { msg } {

    ROS_DEBUG_STREAM(ros::this_node::getName() << " ready to subscribe " << msg);
}
//---------------------------------------------------------------------------------------------------------------------
void RobotPose::display(const char* msg) const {
    const ::geometry_msgs::Point& pos = topic().pose.pose.position;
    ROS_DEBUG_STREAM(ros::this_node::getName() << " " << msg << " pose " << pos.x << tab << pos.y << tab << pos.z);
}

