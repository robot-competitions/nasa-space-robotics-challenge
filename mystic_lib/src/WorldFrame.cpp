/*
 * WorldFrame.cpp
 *
 *  Created on: Mar 20, 2017
 *      Author: rmerriam
 */

#include <mystic.h>
#include <ros_transform.h>

#include "WorldFrame.h"
//---------------------------------------------------------------------------------------------------------------------
const Vector3 WorldFrame::getRPY() const {
    if ( !mRPYValid) {
        Matrix3x3 mx(mTransform.getRotation());
        mx.getRPY(mRoll, mPitch, mYaw);
        mRPYValid = true;
    }
    return Vector3(mRoll, mPitch, mYaw);
}
//---------------------------------------------------------------------------------------------------------------------
const tf2::Transform& WorldFrame::getWorldLocation(const double& x, const double& y, const double& z) {

    using namespace geometry_msgs;
    TransformStamped ts_in;
    TransformStamped ts_out;

    ts_in.header.frame_id = mTarget;
    ts_in.child_frame_id = mTarget;
    ts_in.header.stamp = Time(0);
    ts_in.transform.translation.x = x;
    ts_in.transform.translation.y = y;
    ts_in.transform.translation.z = z;

    while (true) {
        try {
            tfBuffer.transform(ts_in, ts_out, mBase, Duration(0.1));
            break;
        }
        catch (exception& e) {
//            cerr << e.what() << '\n';
        }
    }
    tf2::convert(ts_out.transform, mTransform);

    return mTransform;
}
//---------------------------------------------------------------------------------------------------------------------
const tf2::Transform WorldFrame::getFrameLocation(const double& x, const double& y, const double& z, const string& child,
    const string& base) {

    using namespace geometry_msgs;
    TransformStamped ts_in;
    TransformStamped ts_out;

    ts_in.header.frame_id = child;
    ts_in.child_frame_id = child;
    ts_in.header.stamp = Time(0);
    ts_in.transform.translation.x = x;
    ts_in.transform.translation.y = y;
    ts_in.transform.translation.z = z;

    while (true) {
        try {
            tfBuffer.transform(ts_in, ts_out, base, Duration(0.1));
            break;
        }
        catch (exception& e) {
//            cerr << e.what() << '\n';
        }
    }
    tf2::convert(ts_out.transform, mTransform);

//    cerr << ts_out << '\n';

    return mTransform;
}
//---------------------------------------------------------------------------------------------------------------------
const tf2::Transform& WorldFrame::update(const string& rebase) {
    using namespace geometry_msgs;
    TransformStamped transformStamped;

    string base { rebase.empty() ? mBase : rebase };
    do {
        try {
            transformStamped = tfBuffer.lookupTransform(base, mTarget, Time(0), Duration(0.1));
        }
        catch (exception& e) {
            cerr << e.what() << endl;
        }
    } while (transformStamped.child_frame_id.size() == 0);

    tf2::convert(transformStamped.transform, mTransform);

    mRPYValid = false;
    return mTransform;
}
