/*
 * PointCloud2Sub.cpp
 *
 *  Created on: Sep 14, 2016
 *      Author: rmerriam
 */
//---------------------------------------------------------------------------------------------------------------------
#include "mystic.h"
#include "PointCloud2Sub.h"
//---------------------------------------------------------------------------------------------------------------------
PointCloud2Sub::PointCloud2Sub(const char* msg) :
        SubscribeTopic(msg) {
}
//---------------------------------------------------------------------------------------------------------------------
void PointCloud2Sub::process() {
    mImgSeq = mSubscribeTopic.header.seq;
}

#if 0
///*
// * ImageFetch.cpp
// *
// *  Created on: Sep 14, 2016
// *      Author: rmerriam
// */
#include <cv_bridge/cv_bridge.h>

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
using namespace cv;

#include "mystic.h"
#include "PointCloud2Sub.h"
//---------------------------------------------------------------------------------------------------------------------
PointCloud2Sub::PointCloud2Sub(const char* msg) :
        SubscribeTopic(msg) {
    namedWindow(OPENCV_WINDOW);
}
//---------------------------------------------------------------------------------------------------------------------
void PointCloud2Sub::process() {
    mImgSeq = mSubscribeTopic.header.seq;
}
//---------------------------------------------------------------------------------------------------------------------
void PointCloud2Sub::convertToImage() {
    using namespace pcl;

    // convert cloud to a ROS image in a message, then convert to OpenCV image
    sensor_msgs::Image pc_msg;
    toROSMsg(mSubscribeTopic, pc_msg);

//    mCurImage = cv_bridge::toCvCopy(pc_msg, sensor_msgs::image_encodings::MONO8)->image;
    mCurImage = cv_bridge::toCvCopy(pc_msg, sensor_msgs::image_encodings::BGR8)->image;

    Mat matRotation = getRotationMatrix2D(Point(mCurImage.cols / 2, mCurImage.rows / 2), 180, 1);
    Mat imgRotated;
    warpAffine(mCurImage, imgRotated, matRotation, mCurImage.size());

    updateWindow(imgRotated);
//    updateWindow(mCurImage);
}
//---------------------------------------------------------------------------------------------------------------------
// Update GUI Window
void PointCloud2Sub::updateWindow(const Mat& image) {

    if ( !image.empty()) {
        cv::imshow(OPENCV_WINDOW, image);
    }
    waitKey(1);
}
#endif
