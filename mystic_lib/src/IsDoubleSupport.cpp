/*
 * IsDoubleSupport.cpp
 *
 *  Created on: Dec 28, 2016
 *      Author: rmerriam
 */
#include "mystic.h"
#include <RobotPose.h>

#include <std_msgs/Bool.h>
#include "IsDoubleSupport.h"
//---------------------------------------------------------------------------------------------------------------------
IsDoubleSupport::IsDoubleSupport(const char* msg) :
        SubscribeTopic(msg) {
}
//---------------------------------------------------------------------------------------------------------------------
IsDoubleSupport::~IsDoubleSupport() {
}
//---------------------------------------------------------------------------------------------------------------------
void IsDoubleSupport::process() {
    mDoubleState = mSubscribeTopic.data;
}
//---------------------------------------------------------------------------------------------------------------------
void IsDoubleSupport::waitForSupported() {
    while (isNotSupported()) {
        mRate.sleep();
    }
}
//---------------------------------------------------------------------------------------------------------------------
void IsDoubleSupport::waitForNotSupported() {
    while (isSupported()) {
        mRate.sleep();
    }
}

