/*
 * ObjectFinder.h
 *
 *  Created on: Apr 18, 2017
 *      Author: rmerriam
 */

#ifndef OBJECTFINDER_H_
#define OBJECTFINDER_H_

#include "WorldFrame.h"
#include "ImageVision.h"
//---------------------------------------------------------------------------------------------------------------------
inline Vec3b gimp2cv(const double& h, const double& s, const double& v) {
    return Vec3b { uint8_t(h / 2), uint8_t(255 * s), uint8_t(255 * v) };
}
//---------------------------------------------------------------------------------------------------------------------
using Contours = vector < vector<Point> >;
using ColorVec = vector<Vec3b>;
//---------------------------------------------------------------------------------------------------------------------
class ObjectFinder : public WorldFrame {
public:

    ObjectFinder(const uint8_t id, const Vec3b& lower, const Vec3b& upper) :
        WorldFrame { "left_camera_optical_frame" }, mId { id }, mLower { lower }, mUpper { upper } {
    }

    ~ObjectFinder() = default;

    Vec3b colors();
    const uint8_t id() const;
    const Mat& objects() const;

    const Contours& process(const Mat& img);

private:
    Mat findByColor(const Mat& img);
    int16_t zeroLimit(int16_t value);

    const uint8_t mId;
    const Vec3b mLower;
    const Vec3b mUpper;
    Contours mContours { };
    Mat mObjects;
};
//---------------------------------------------------------------------------------------------------------------------
inline const uint8_t ObjectFinder::id() const {
    return mId;
}
//---------------------------------------------------------------------------------------------------------------------
inline const cv::Mat& ObjectFinder::objects() const {
    return mObjects;
}

#endif /* OBJECTFINDER_H_ */
