/*
 * MoveArm.h
 *
 *  Created on: Dec 6, 2016
 *      Author: rmerriam
 */

#ifndef SRC_ChestTraj_H_
#define SRC_ChestTraj_H_
//---------------------------------------------------------------------------------------------------------------------
#include <ihmc_msgs/ChestTrajectoryRosMessage.h>
using namespace ihmc_msgs;

//  chest_trajectory:=/ihmc_ros/valkyrie/control/chest_trajectory

#include "SO3Traj.h"
//---------------------------------------------------------------------------------------------------------------------
class ChestTraj : public SO3Traj<ihmc_msgs::ChestTrajectoryRosMessage> {
public:
    ChestTraj(const char* msg = "chest_trajectory");
    virtual ~ChestTraj() = default;

private:

};
//---------------------------------------------------------------------------------------------------------------------
inline ChestTraj::ChestTraj(const char* msg) :
    SO3Traj(msg, "torso", "world") {
}

#endif /* SRC_ChestTraj_H_ */
