/*
 * open_cv.h
 *
 *  Created on: Mar 22, 2017
 *      Author: rmerriam
 */

#ifndef OPEN_CV_H_
#define OPEN_CV_H_

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#endif /* OPEN_CV_H_ */
