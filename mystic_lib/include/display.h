/*
 * display.h
 *
 *  Created on: Jan 3, 2017
 *      Author: rmerriam
 */

#ifndef DISPLAY_H_
#define DISPLAY_H_
#include <iostream>
using namespace std;
namespace mys {
//---------------------------------------------------------------------------------------------------------------------
    constexpr char tab { '\t' };
    constexpr char nl { '\n' };
    constexpr char sp { ' ' };
//---------------------------------------------------------------------------------------------------------------------
    template <typename T>
    inline void display(const T& v) {
        cerr << '[' << v << ']';
    }
    //---------------------------------------------------------------------------------------------------------------------
    template <typename T>
    inline ostream& display(ostream& os, const T& v) {
        os << '[' << v << ']';
        return os;
    }
    //---------------------------------------------------------------------------------------------------------------------
    template <typename T>
    void display(const T x, const T y) {
        std::cerr << '[' << x << ", " << y << ']';
    }
    //---------------------------------------------------------------------------------------------------------------------
    template <typename T>
    ostream& display(ostream& os, const T& x, const T& y) {
        os << '[' << x << ", " << y << ']';
        return os;
    }
    //---------------------------------------------------------------------------------------------------------------------
    template <typename T>
    void display(const T x, const T y, const T z) {
        std::cerr << '[' << x << ", " << y << ", " << z << ']';
    }
    //---------------------------------------------------------------------------------------------------------------------
    template <typename T>
    ostream& display(ostream& os, const T x, const T y, const T z) {
        os << '[' << x << ", " << y << ", " << z << ']';
        return os;
    }
    //---------------------------------------------------------------------------------------------------------------------
    template <typename T>
    void display(const T x, const T y, const T z, const T w) {
        std::cerr << '[' << x << ", " << y << ", " << z << ", " << w << ']';
    }
    //---------------------------------------------------------------------------------------------------------------------
    template <typename T>
    ostream& display(ostream& os, const T x, const T y, const T z, const T w) {
        os << '[' << x << ", " << y << ", " << z << ", " << w << ']';
        return os;
    }
}
#endif /* DISPLAY_H_ */
