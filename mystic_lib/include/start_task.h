/*
 * start_task.h
 *
 *  Created on: Mar 17, 2017
 *      Author: rmerriam
 */

#ifndef START_TASK_H_
#define START_TASK_H_
#include <iomanip>
#include <iostream>
using namespace std;
#include <exec_prog.h>
//---------------------------------------------------------------------------------------------------------------------
inline
void start_task(const uint8_t task, const uint8_t check_point) {
    const char* cmdline[] = { "bash", "-c", "", 0 };

    string c("source /opt/nasa/indigo/setup.bash && rosservice call /srcsim/finals/start_task          ");
    c[83] = task + '0';
    c[85] = check_point + '0';

    cmdline[2] = const_cast<char*>(c.c_str());
    exec_prog(const_cast<const char**>(cmdline));
}


#endif /* START_TASK_H_ */
