/*
 * IsDoubleSupport.h
 *
 *  Created on: Dec 28, 2016
 *      Author: rmerriam
 */

#ifndef ISDOUBLESUPPORT_H_
#define ISDOUBLESUPPORT_H_

#include <SubscribeTopic.h>
#include <std_msgs/Bool.h>

//---------------------------------------------------------------------------------------------------------------------
class IsDoubleSupport : public SubscribeTopic<std_msgs::BoolPtr> {
public:
    IsDoubleSupport(const char* msg = "is_in_double_support");
    ~IsDoubleSupport();

    bool isSupported();
    bool isNotSupported();
    void waitForSupported();
    void waitForNotSupported();

protected:
    virtual void process() override;

    bool mDoubleState { false };
};
//---------------------------------------------------------------------------------------------------------------------
inline
bool IsDoubleSupport::isSupported() {
    return mDoubleState;
}
//---------------------------------------------------------------------------------------------------------------------
inline
bool IsDoubleSupport::isNotSupported() {
    return !isSupported();
}

#endif /* ISDOUBLESUPPORT_H_ */
