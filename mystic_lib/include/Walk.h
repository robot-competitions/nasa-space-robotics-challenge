/*
 * Walk.h
 *
 *  Created on: Nov 14, 2016
 *      Author: rmerriam
 */

#ifndef SRC_WALK_H_
#define SRC_WALK_H_
//--------------------------------------------------------------------------------------------------------------------
#include "FootstepDataMsg.h"
#include <ihmc_msgs/FootstepDataListRosMessage.h>
#include "PublishTemplate.h"
#include "RPY.h"
//---------------------------------------------------------------------------------------------------------------------
class Walk : public PublishTemplate<ihmc_msgs::FootstepDataListRosMessage> {
public:
    Walk(const char* msg = "footstep_list");

    void alignFeet();

    void incStep(const mys::Side side, const double x, const double y, const double z = 0.0);
    void incStep(const mys::Side side, const Vec3& vec);
    void setStep(const mys::Side side, const Vec3& vec);

    void turn(const double yaw);

    void doSteps();
    void startSteps();
    Vec3 getLocation(const mys::Side side) const;
    Quaternion getRotation(const mys::Side side) const;
    RPY getRPY(const mys::Side side) const;

    void updateByFrame(const string& rebase);
    void stepHeight(const double h);
    double swing() const;
    void swing(const double s);

    volatile size_t steps() const;

    double transfer() const;
    void transfer(const double t);

    enum TrajectoryType {
        eDefault, eObstacleClearance, eCustom
    };
    void trajectoryType(const TrajectoryType t);

    void waitForFinished() override;

private:

    void sendMessage() override;

    ihmc_msgs::FootstepDataListRosMessage mStepList;

    size_t mStepCnt { };
    double mStepHeight { 0.05 };
    double mSwing { 0.8 };
    double mTransfer { 0.8 };
    TrajectoryType mTrajectoryType { eDefault };

    reference_wrapper<FootstepDataMsg> mFootstepDataMsg[2] {
        *new FootstepDataMsg(mys::Side::eLeft),
        *new FootstepDataMsg(mys::Side::eRight)
    };
};
//---------------------------------------------------------------------------------------------------------------------
inline
void Walk::doSteps() {
    publish();
}
//---------------------------------------------------------------------------------------------------------------------
inline Vec3 Walk::getLocation(const mys::Side side) const {
    return mFootstepDataMsg[side].get().getLocation();
}
//---------------------------------------------------------------------------------------------------------------------
inline tf2::Quaternion Walk::getRotation(const mys::Side side) const {
    return mFootstepDataMsg[side].get().getRotation();
}
//---------------------------------------------------------------------------------------------------------------------
inline RPY Walk::getRPY(const mys::Side side) const {
    return mFootstepDataMsg[side].get().getRPY();
}
//---------------------------------------------------------------------------------------------------------------------
inline
void Walk::stepHeight(const double h) {
    mStepHeight = h;
}
//---------------------------------------------------------------------------------------------------------------------
inline volatile size_t Walk::steps() const {
    return mStepCnt;
}
//---------------------------------------------------------------------------------------------------------------------
inline
double Walk::swing() const {
    return mSwing;
}
//---------------------------------------------------------------------------------------------------------------------
inline
void Walk::swing(const double s) {
    mSwing = s;
    mStepList.default_swing_time = mSwing;
}
//---------------------------------------------------------------------------------------------------------------------
inline
void Walk::trajectoryType(const TrajectoryType t) {
    mTrajectoryType = t;
}
//---------------------------------------------------------------------------------------------------------------------
inline
double Walk::transfer() const {
    return mTransfer;
}

#endif /* SRC_WALK_H_ */
