/*
 * JointStatus.h
 *
 *  Created on: Sep 14, 2016
 *      Author: rmerriam
 */

#ifndef SRC_JointStatus_H_
#define SRC_JointStatus_H_

// joint_states:=/ihmc_ros/valkyrie/output/joint_states

#include <SubscribeTopic.h>
#include <sensor_msgs/JointState.h>
//---------------------------------------------------------------------------------------------------------------------
using namespace sensor_msgs;
namespace std {
    typedef basic_string<char> string;
}
//---------------------------------------------------------------------------------------------------------------------
class JointStatus : public SubscribeTopic<sensor_msgs::JointStatePtr> {
public:
    enum class Joint
    ;

    JointStatus(const char* msg = "joint_states");

    double getEffort(const Joint j);
    const std::string& getName(const Joint j);
    double getPosition(const Joint j);

private:
    virtual void process() override;

    uint32_t mMsgSeq {};

public:
    enum class Joint {
        leftHipYaw,
        leftHipRoll,
        leftHipPitch,
        leftKneePitch,
        leftAnklePitch,
        leftAnkleRoll,
        rightHipYaw,
        rightHipRoll,
        rightHipPitch,
        rightKneePitch,
        rightAnklePitch,
        rightAnkleRoll,
        torsoYaw,
        torsoPitch,
        torsoRoll,
        leftShoulderPitch,
        leftShoulderRoll,
        leftShoulderYaw,
        leftElbowPitch,
        leftForearmYaw,
        leftWristRoll,
        leftWristPitch,
        lowerNeckPitch,
        neckYaw,
        upperNeckPitch,
        hokuyo_joint,
        rightShoulderPitch,
        rightShoulderRoll,
        rightShoulderYaw,
        rightElbowPitch,
        rightForearmYaw,
        rightWristRoll,
        rightWristPitch
    };
};
//---------------------------------------------------------------------------------------------------------------------
inline JointStatus::JointStatus(const char* msg) :
        SubscribeTopic(msg) {
}
//---------------------------------------------------------------------------------------------------------------------
inline double JointStatus::getEffort(const Joint j) {
    return mSubscribeTopic.effort[int(j)];
}
//---------------------------------------------------------------------------------------------------------------------
inline const std::string& JointStatus::getName(const Joint j) {
    return mSubscribeTopic.name[int(j)];
}
//---------------------------------------------------------------------------------------------------------------------
inline double JointStatus::getPosition(const Joint j) {
    return mSubscribeTopic.position[int(j)];
}
//---------------------------------------------------------------------------------------------------------------------
inline void JointStatus::process() {
    mMsgSeq = mSubscribeTopic.header.seq;
}

#endif /* SRC_IMAGEFETCH_H_ */
