/*
 * SubscribeTopic.h
 *
 *  Created on: Dec 11, 2016
 *      Author: rmerriam
 */

#ifndef INCLUDE_SUBSCRIBETOPIC_H_
#define INCLUDE_SUBSCRIBETOPIC_H_
//---------------------------------------------------------------------------------------------------------------------
template <typename TPtr>
class SubscribeTopic {
public:
    using T = typename TPtr::element_type;

    SubscribeTopic(const char* msg, float rate = 20);
    virtual ~SubscribeTopic() {
        mSubscriber.shutdown();
    }

    const T& topic() const {
        return mSubscribeTopic;
    }
    bool isSubscribed() const;

    void sleep() {
        mRate.sleep();
    }
    bool isValid() const {
        return mValid;
    }

protected:
    void receiveSubscribeTopic(const TPtr& m);
    virtual void process() = 0;

    T mSubscribeTopic;
    Rate mRate;

    NodeHandle mNodeHandle;
    Subscriber mSubscriber;
    bool mValid { false };
};
//---------------------------------------------------------------------------------------------------------------------
template <typename TPtr>
inline SubscribeTopic<TPtr>::SubscribeTopic(const char* msg, float rate) :
        mRate(rate) {
    mSubscriber = mNodeHandle.subscribe(msg, 1, &SubscribeTopic::receiveSubscribeTopic, this);
    ROS_DEBUG_STREAM(ros::this_node::getName() << " ready to subscribe " << msg);
}
//---------------------------------------------------------------------------------------------------------------------
template <typename TPtr>
inline bool SubscribeTopic<TPtr>::isSubscribed() const {
    return mSubscriber.getNumPublishers() != 0;
}
//---------------------------------------------------------------------------------------------------------------------
template <typename TPtr>
inline void SubscribeTopic<TPtr>::receiveSubscribeTopic(const TPtr& m) {
    mSubscribeTopic = *m;
    process();
    mValid = true;
}

#endif /* INCLUDE_SUBSCRIBETOPIC_H_ */
