/*
 * HarnessStatus.h
 *
 *  Created on: Nov 14, 2016
 *      Author: rmerriam
 */

#ifndef SRC_HarnessStatus_H_
#define SRC_HarnessStatus_H_

//  /srcsim/finals/task

//---------------------------------------------------------------------------------------------------------------------
#include <srcsim/Harness.h>
using namespace ros;

#include "SubscribeTopic.h"

//---------------------------------------------------------------------------------------------------------------------
class HarnessStatus : public SubscribeTopic<srcsim::HarnessPtr> {
public:
    enum eStatus {
        eNone, eDetachToAttach, eAttachToLower, eLowerToStand, eStandToDetach, eDetachToMove
    };

    HarnessStatus(const char* msg = "/srcsim/finals/harness");
    virtual ~HarnessStatus() override = default;

    const eStatus status() const;

private:
    virtual void process() override;
};
//---------------------------------------------------------------------------------------------------------------------
inline HarnessStatus::HarnessStatus(const char* msg) :
        SubscribeTopic { msg } {
}
//---------------------------------------------------------------------------------------------------------------------
inline const HarnessStatus::eStatus HarnessStatus::status() const {
    return (eStatus)topic().status;
}
//---------------------------------------------------------------------------------------------------------------------
inline void HarnessStatus::process() {
}

#endif /* SRC_HarnessStatus_H_ */
