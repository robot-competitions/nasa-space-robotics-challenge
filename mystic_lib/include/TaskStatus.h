/*
 * TaskStatus.h
 *
 *  Created on: Nov 14, 2016
 *      Author: rmerriam
 */

#ifndef SRC_TaskStatus_H_
#define SRC_TaskStatus_H_

//  /srcsim/finals/task

//---------------------------------------------------------------------------------------------------------------------
#include <srcsim/Task.h>
using namespace ros;

#include "SubscribeTopic.h"

//---------------------------------------------------------------------------------------------------------------------
class TaskStatus : public SubscribeTopic<srcsim::TaskPtr> {
public:
    TaskStatus(const char* msg = "task");
    virtual ~TaskStatus() override = default;

    const ros::Time startTime() const;
    const ros::Duration elapsedTime() const;

    const uint32_t task() const;
    const uint32_t checkPt() const;
    const bool isTimedout() const;
    const bool isFinished() const;

private:
    virtual void process() override;
};
//---------------------------------------------------------------------------------------------------------------------
inline TaskStatus::TaskStatus(const char* msg) :
        SubscribeTopic { msg } {
}
////---------------------------------------------------------------------------------------------------------------------
//inline const typename srcsim::Task_<allocator<void>>::_checkpoints_completion_type TaskStatus::completions() const {
//    return topic().checkpoints_completion;
//}
//---------------------------------------------------------------------------------------------------------------------
inline const ros::Time TaskStatus::startTime() const {
    return topic().start_time;
}
//---------------------------------------------------------------------------------------------------------------------
inline const ros::Duration TaskStatus::elapsedTime() const {
    return topic().elapsed_time;
}
//---------------------------------------------------------------------------------------------------------------------
inline const uint32_t TaskStatus::task() const {
    return topic().task;
}
//---------------------------------------------------------------------------------------------------------------------
inline const uint32_t TaskStatus::checkPt() const {
    return topic().current_checkpoint;
}
//---------------------------------------------------------------------------------------------------------------------
inline const bool TaskStatus::isTimedout() const {
    return topic().timed_out;
}
//---------------------------------------------------------------------------------------------------------------------
inline const bool TaskStatus::isFinished() const {
    return topic().finished;
}
//---------------------------------------------------------------------------------------------------------------------
inline void TaskStatus::process() {
}

#endif /* SRC_TaskStatus_H_ */
