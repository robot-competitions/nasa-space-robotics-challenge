/*
 * HandTraj.h
 *
 *  Created on: Dec 6, 2016
 *      Author: rmerriam
 */

#ifndef SRC_HandTraj_H_
#define SRC_HandTraj_H_
//---------------------------------------------------------------------------------------------------------------------
//  hand_trajectory:=/ihmc_ros/valkyrie/control/hand_trajectory
//

#include "mystic.h"
#include "ros_transform.h"

#include <ihmc_msgs/HandTrajectoryRosMessage.h>
using namespace ihmc_msgs;

#include "WorldFrame.h"
#include "PublishTemplate.h"
//---------------------------------------------------------------------------------------------------------------------
class HandTraj : public PublishTemplate<ihmc_msgs::HandTrajectoryRosMessage>, public WorldFrame {
public:
    HandTraj(const mys::Side side, const char* msg = "hand_trajectory");
    virtual ~HandTraj() = default;

    void incPose(const tf2::Vector3& pos, const tf2::Vector3& rot = zero_vec3, const bool move_delay = true);
    void setPose(const tf2::Vector3& pos, const tf2::Vector3& rot = zero_vec3, const bool move_delay = true);

private:
    virtual void sendMessage() override;

    const mys::Side mSide;

    static const string mFrameName[2];
};

#endif /* SRC_MOVEHAND_H_ */
