/*
 * WalkStop.h
 *
 *  Created on: Dec 28, 2016
 *      Author: rmerriam
 */

#ifndef WalkStop_H_
#define WalkStop_H_

#include <ihmc_msgs/AbortWalkingRosMessage.h>

// abort_walking:=/ihmc_ros/valkyrie/control/abort_walking

#include <PublishTemplate.h>

//---------------------------------------------------------------------------------------------------------------------
class WalkStop : public PublishTemplate<ihmc_msgs::AbortWalkingRosMessage> {
public:
    WalkStop(const char* msg = "abort_walking") :
        PublishTemplate { msg } {
    }

    virtual ~WalkStop() = default;

protected:
    virtual void sendMessage() override {
        mTopic.unique_id = ++mId;
        mPublisher.publish(mTopic);
    }

};
#endif /* WalkStop_H_ */
