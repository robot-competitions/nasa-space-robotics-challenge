/*
 * ArmTraj.h
 *
 *  Created on: Dec 6, 2016
 *      Author: rmerriam
 */

#ifndef SRC_ArmTraj_H_
#define SRC_ArmTraj_H_
//---------------------------------------------------------------------------------------------------------------------
//  arm_trajectory:=/ihmc_ros/valkyrie/control/arm_trajectory e ihmc_msgs/ArmTrajRosMessage
/*
 0   Shoulder rotation
 1   Shoulder pitch
 2   Upper arm rotation
 3   Elbow joint
 4   Lower arm rotation
 5   Wrist X
 6   Wrist Y
 */

#include "mystic.h"

#include <ihmc_msgs/ArmTrajectoryRosMessage.h>
using namespace ihmc_msgs;

#include "WorldFrame.h"
#include "PublishTemplate.h"
//---------------------------------------------------------------------------------------------------------------------
class ArmTraj : public PublishTemplate<ihmc_msgs::ArmTrajectoryRosMessage> {
public:
    enum ArmPart {
        eShoulderRotate, eShoulderJoint, eUpperArmRotate, eElbowJointRotate, eLowerArmRotate, eWristX, eWristY
    };

    ArmTraj(const mys::Side side, const char* msg = "arm_trajectory");
    virtual ~ArmTraj() = default;

    void setPose(const ArmPart part, const double& position);

    void startMove();
    void doMove();

private:
    virtual void sendMessage() override;

    const mys::Side mSide;
};
#endif /* SRC_MOVEHAND_H_ */
