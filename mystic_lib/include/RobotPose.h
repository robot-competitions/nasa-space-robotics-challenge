/*
 * RobotPose.h
 *
 *  Created on: Dec 11, 2016
 *      Author: rmerriam
 */

#ifndef INCLUDE_ROBOTPOSE_H_
#define INCLUDE_ROBOTPOSE_H_
//---------------------------------------------------------------------------------------------------------------------
// /ihmc_ros/valkyrie/output/robot_pos

#include <nav_msgs/Odometry.h>
using namespace nav_msgs;

#include "SubscribeTopic.h"
//---------------------------------------------------------------------------------------------------------------------
class RobotPose : public SubscribeTopic<nav_msgs::OdometryPtr> {
public:

    RobotPose(const char* msg = "robot_pose");

    const ::geometry_msgs::Vector3 angular() const;
    const ::geometry_msgs::Quaternion& orientation() const;
    const ::geometry_msgs::Pose& pose() const;
    const ::geometry_msgs::Twist twist() const;

    double poseX() const;
    double poseY() const;
    double poseZ() const;

    double roll() const;
    double pitch() const;
    double yaw() const;

    void display(const char* msg) const;

private:
    virtual void process() override;

    double mRoll;
    double mPitch;
    double mYaw;
};
//---------------------------------------------------------------------------------------------------------------------
inline const geometry_msgs::Pose& RobotPose::pose() const {
    return topic().pose.pose;
}
//---------------------------------------------------------------------------------------------------------------------
inline const geometry_msgs::Quaternion& RobotPose::orientation() const {
    return topic().pose.pose.orientation;
}
//---------------------------------------------------------------------------------------------------------------------
inline const geometry_msgs::Vector3 RobotPose::angular() const {
    return topic().twist.twist.angular;
}
//---------------------------------------------------------------------------------------------------------------------
inline const geometry_msgs::Twist RobotPose::twist() const {
    return topic().twist.twist;
}
//---------------------------------------------------------------------------------------------------------------------
inline double RobotPose::poseX() const {
    return topic().pose.pose.position.x;
}
//---------------------------------------------------------------------------------------------------------------------
inline double RobotPose::poseY() const {
    return topic().pose.pose.position.y;
}
//---------------------------------------------------------------------------------------------------------------------
inline double RobotPose::poseZ() const {
    return topic().pose.pose.position.z;
}
//---------------------------------------------------------------------------------------------------------------------
inline double RobotPose::roll() const {
    return mRoll;
}
//---------------------------------------------------------------------------------------------------------------------
inline double RobotPose::pitch() const {
    return mPitch;
}
//---------------------------------------------------------------------------------------------------------------------
inline double RobotPose::yaw() const {
    return mYaw;
}
//--------------------------------------------------------------------------------------------------------------------------
inline void toEulerianAngle(const geometry_msgs::Quaternion& q, double& roll, double& pitch, double& yaw)
    {
    double ysqr = q.y * q.y;

    // roll (x-axis rotation)
    double t0 = +2.0 * (q.w * q.x + q.y * q.z);
    double t1 = +1.0 - 2.0 * (q.x * q.x + ysqr);
    roll = std::atan2(t0, t1);

    // pitch (y-axis rotation)
    double t2 = +2.0 * (q.w * q.y - q.z * q.x);
    t2 = t2 > 1.0 ? 1.0 : t2;
    t2 = t2 < -1.0 ? -1.0 : t2;
    pitch = std::asin(t2);

    // yaw (z-axis rotation)
    double t3 = +2.0 * (q.w * q.z + q.x * q.y);
    double t4 = +1.0 - 2.0 * (ysqr + q.z * q.z);
    yaw = std::atan2(t3, t4);
}
//---------------------------------------------------------------------------------------------------------------------
inline void RobotPose::process() {
    const geometry_msgs::Quaternion& q { topic().pose.pose.orientation };
    toEulerianAngle(q, mRoll, mPitch, mYaw);
}

#endif /* INCLUDE_ROBOTPOSE_H_ */
