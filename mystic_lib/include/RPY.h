/*
 * RPY.h
 *
 *  Created on: Apr 6, 2017
 *      Author: rmerriam
 */
#ifndef RPY_H_
#define RPY_H_

// #include <RPY.h>
#include <ros_transform.h>

//---------------------------------------------------------------------------------------------------------------------
struct RPY {
    RPY() noexcept {
    }
    RPY(const double& r, const double& p, const double& y) noexcept :
    mRoll(r), mPitch(p), mYaw(y) {
    }
    RPY(const tf2::Vector3& v) noexcept :
    mRoll(v.getX()), mPitch(v.getY()), mYaw(v.getZ()) {
    }

    bool isValid() const {return !std::isnan(mRoll);}
    double mRoll {nan("")};
    double mPitch {nan("")};
    double mYaw {nan("")};
};
//---------------------------------------------------------------------------------------------------------------------
const RPY rpy_no_value { nan(""), nan(""), nan("") };
const RPY rpy_reset_pose { std::numeric_limits<double>::infinity(), std::numeric_limits<double>::infinity(),
    std::numeric_limits<double>::infinity() };
const RPY rpy_zero { 0, 0, 0 };
//---------------------------------------------------------------------------------------------------------------------
inline const RPY operator+(RPY lhs, const RPY& rhs) {
    lhs.mRoll += rhs.mRoll;
    lhs.mPitch += rhs.mPitch;
    lhs.mYaw += rhs.mYaw;
    return lhs;
}
//---------------------------------------------------------------------------------------------------------------------
template <typename T>
const T deg2rad(const T& deg) {
    return deg * M_PI / 180;
}
//---------------------------------------------------------------------------------------------------------------------
template <typename T>
const T rad2deg(const T& rad) {
    return rad * 180.0 / M_PI;
}
//---------------------------------------------------------------------------------------------------------------------
inline ostream& operator<<(ostream& os, const RPY rpy) {
    os << '[' << rpy.mRoll << ", " << rpy.mPitch << ", " << rpy.mYaw << ']';
    return os;

}
#endif /* RPY_H_ */
