/*
 * ImageVision.h
 *
 *  Created on: Mar 19, 2017
 *      Author: rmerriam
 */

#ifndef IMAGEVISION_H_
#define IMAGEVISION_H_
//---------------------------------------------------------------------------------------------------------------------
#include <cv_bridge/cv_bridge.h>

#include <PointCloud2Sub.h>
using namespace pcl;

#include <open_cv.h>
using namespace cv;
//---------------------------------------------------------------------------------------------------------------------
// Update GUI Window
inline void updateWindow(const Mat image, const string& name) {

    if ( !image.empty()) {
        cv::imshow(name, image);
    }
    waitKey(1);
}
//---------------------------------------------------------------------------------------------------------------------
class ImageVision {

public:
    ImageVision(const Mat img);
    ImageVision(const Mat img, uint8_t conversion);
    ImageVision(const PCloud& cloud, const string encoding = sensor_msgs::image_encodings::BGR8);

    Mat& image();
    const Mat flipped() const;
    const Mat ROI(const Rect& r) const {
        return mImage(r);
    }
    void apply(const Mat& img, const Rect& r) {
        img.copyTo(mImage(r));
    }

    const Mat grayMask(const uint8_t min_value, const uint8_t max_value);
private:
    Mat mImage;
};
#endif /* IMAGEVISION_H_ */
