/*
 * SatelliteDishStatus.h
 *
 *  Created on: Nov 14, 2016
 *      Author: rmerriam
 */

#ifndef SRC_SatelliteDishStatus_H_
#define SRC_SatelliteDishStatus_H_

//  /task1/checkpoint2/satellite

//---------------------------------------------------------------------------------------------------------------------
#include <srcsim/Satellite.h>
using namespace ros;

#include "SubscribeTopic.h"
//---------------------------------------------------------------------------------------------------------------------
class SatelliteDishStatus : public SubscribeTopic<srcsim::SatellitePtr> {
public:
    SatelliteDishStatus(const char* msg = "satellite");
    virtual ~SatelliteDishStatus() override = default;

    const double currentPitch() const;
    const double currentYaw() const;
    const double targetPitch() const;
    const double targetYaw() const;
    const bool isPitchCorrectNow() const;
    const bool isYawCorrectNow() const;
    const bool isPitchFinished() const;
    const bool isYawFinished() const;

private:
    virtual void process() override;
};
//---------------------------------------------------------------------------------------------------------------------
inline void SatelliteDishStatus::process() {
}
//---------------------------------------------------------------------------------------------------------------------
inline const double SatelliteDishStatus::currentPitch() const {
    return topic().current_pitch;
}
//---------------------------------------------------------------------------------------------------------------------
inline const double SatelliteDishStatus::currentYaw() const {
    return topic().current_yaw;
}
//---------------------------------------------------------------------------------------------------------------------
inline const double SatelliteDishStatus::targetPitch() const {
    return topic().target_pitch;
}
//---------------------------------------------------------------------------------------------------------------------
inline const double SatelliteDishStatus::targetYaw() const {
    return topic().target_yaw;
}
//---------------------------------------------------------------------------------------------------------------------
inline const bool SatelliteDishStatus::isPitchCorrectNow() const {
    return topic().pitch_correct_now;
}
//---------------------------------------------------------------------------------------------------------------------
inline const bool SatelliteDishStatus::isYawCorrectNow() const {
    return topic().yaw_correct_now;
}
//---------------------------------------------------------------------------------------------------------------------
inline const bool SatelliteDishStatus::isPitchFinished() const {
    return topic().pitch_completed;
}
//---------------------------------------------------------------------------------------------------------------------
inline const bool SatelliteDishStatus::isYawFinished() const {
    return topic().yaw_completed;
}
#endif /* SRC_SatelliteDishStatus_H_ */
