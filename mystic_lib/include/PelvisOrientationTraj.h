/*
 * MoveArm.h
 *
 *  Created on: Dec 6, 2016
 *      Author: rmerriam
 */

#ifndef SRC_HeadTraj_H_
#define SRC_HeadTraj_H_
//---------------------------------------------------------------------------------------------------------------------
#include <ihmc_msgs/PelvisOrientationTrajectoryRosMessage.h>
using namespace ihmc_msgs;

#include "SO3Traj.h"
//---------------------------------------------------------------------------------------------------------------------
class PelvisOrientationTraj : public SO3Traj<ihmc_msgs::PelvisOrientationTrajectoryRosMessage>, public WorldFrame {
public:
    PelvisOrientationTraj(const char* msg = "pelvis_orientation_trajectory");
    virtual ~PelvisOrientationTraj() = default;

private:

};
//---------------------------------------------------------------------------------------------------------------------
inline PelvisOrientationTraj::PelvisOrientationTraj(const char* msg) :
    SO3Traj(msg, "pelvis") {
}

#endif /* SRC_HeadTraj_H_ */
