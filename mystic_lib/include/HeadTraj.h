/*
 * MoveArm.h
 *
 *  Created on: Dec 6, 2016
 *      Author: rmerriam
 */

#ifndef SRC_HeadTraj_H_
#define SRC_HeadTraj_H_
//---------------------------------------------------------------------------------------------------------------------
#include <ihmc_msgs/HeadTrajectoryRosMessage.h>
using namespace ihmc_msgs;

//  head_trajectory:=/ihmc_ros/valkyrie/control/head_trajectory

#include "SO3Traj.h"
//---------------------------------------------------------------------------------------------------------------------
class HeadTraj : public SO3Traj<ihmc_msgs::HeadTrajectoryRosMessage>, public WorldFrame {
public:
    HeadTraj(const char* msg = "head_trajectory");
    virtual ~HeadTraj() = default;

private:
};
//---------------------------------------------------------------------------------------------------------------------
inline HeadTraj::HeadTraj(const char* msg) :
    SO3Traj(msg, "head") {
}

#endif /* SRC_HeadTraj_H_ */
