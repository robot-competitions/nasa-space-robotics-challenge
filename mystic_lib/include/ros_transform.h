/*
 * ros_transform.h
 *
 *  Created on: Mar 22, 2017
 *      Author: rmerriam
 */

#ifndef ROS_TRANSFORM_H_
#define ROS_TRANSFORM_H_

// #include <ros_transform.h>
#include <tf2/impl/utils.h>
#include <tf2_ros/transform_listener.h>
#include <tf2/LinearMath/Vector3.h>
using namespace tf2;
using namespace tf2_ros;
//---------------------------------------------------------------------------------------------------------------------
using Vec3 = tf2::Vector3;
const Vec3 zero_vec3 { 0, 0, 0 };
const geometry_msgs::Vector3 zero_gvec;
//=====================================================================================================================
inline Vec3 geo2tf2(const geometry_msgs::Vector3 v) {
    Vec3 t;
    t.setX(v.x);
    t.setY(v.y);
    t.setZ(v.z);
    return t;
}
//---------------------------------------------------------------------------------------------------------------------
inline const geometry_msgs::Vector3 tf2geo(const Vec3& v) {
    geometry_msgs::Vector3 g;
    g.x = v.getX();
    g.y = v.getY();
    g.z = v.getZ();
    return g;
}
//---------------------------------------------------------------------------------------------------------------------
inline tf2::Quaternion geo2tf2(const geometry_msgs::Quaternion v) {
    tf2::Quaternion t;
    t.setX(v.x);
    t.setY(v.y);
    t.setZ(v.z);
    t.setW(v.w);
    return t;
}
//---------------------------------------------------------------------------------------------------------------------
inline const geometry_msgs::Quaternion tf2geo(const tf2::Quaternion& v) {
    geometry_msgs::Quaternion g;
    g.x = v.getX();
    g.y = v.getY();
    g.z = v.getZ();
    g.w = v.getW();
    return g;
}
//=====================================================================================================================
inline ostream& operator<<(ostream& os, const Vec3& v) {
    os << '[' << v.getX() << ", " << v.getY() << ", " << v.getZ() << ']';
    return os;
}
//---------------------------------------------------------------------------------------------------------------------
inline ostream& operator<<(ostream& os, const ::geometry_msgs::Vector3& v) {
    os << '[' << v.x << ", " << v.y << ", " << v.z << ']';
    return os;
}
//---------------------------------------------------------------------------------------------------------------------
inline ostream& operator<<(ostream& os, const tf2::Quaternion& q) {
    os << q.getX() << " " << q.getY() << " " << q.getZ() << " " << q.getW();
    return os;
}
//---------------------------------------------------------------------------------------------------------------------
inline ostream& operator<<(ostream& os, const ::geometry_msgs::Quaternion& q) {
    os << q.x << " " << q.y << " " << q.z << " " << q.w;
    return os;
}
#endif /* ROS_TRANSFORM_H_ */
