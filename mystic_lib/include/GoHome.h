/*
 * GoHome.h
 *
 *  Created on: Dec 6, 2016
 *      Author: rmerriam
 */

#ifndef SRC_GoHome_H_
#define SRC_GoHome_H_
//---------------------------------------------------------------------------------------------------------------------
//  GoHomeRosMessage:=/ihmc_ros/valkyrie/control/go_home
//  ihmc_msgs/GoHomeRosMessage
//
// #include <GoHome.h>
#include "mystic.h"
#include "ros_transform.h"

#include <ihmc_msgs/GoHomeRosMessage.h>
using namespace ihmc_msgs;

#include "WorldFrame.h"
#include "PublishTemplate.h"
//---------------------------------------------------------------------------------------------------------------------
class GoHome : public PublishTemplate<ihmc_msgs::GoHomeRosMessage> {
public:
    enum GoPart {
        eARM, eCHEST, ePELVIS
    };

    GoHome(const char* msg = "GoHomeRosMessage") :
        PublishTemplate(msg) {
    }
    virtual ~GoHome() = default;

    void setPose(const mys::Side side, const GoPart part, const double& t) {
        mTopic.body_part = part;
        mTopic.robot_side = side;
        mTopic.trajectory_time = t;
        mTopic.unique_id = ++mId;
    }

private:
    virtual void sendMessage() override {
        publisher().publish(mTopic);
        ROS_DEBUG_STREAM(ros::this_node::getName() << " Go Home Sent");
        Duration(mDelayTime).sleep();
    }
};

#endif /* SRC_MOVEHAND_H_ */
