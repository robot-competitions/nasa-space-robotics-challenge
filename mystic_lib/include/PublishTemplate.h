/*
 * PublishTemplate.h
 *
 *  Created on: Nov 30, 2016
 *      Author: rmerriam
 */

#ifndef SRC_PUBLISHTEMPLATE_H_
#define SRC_PUBLISHTEMPLATE_H_
//---------------------------------------------------------------------------------------------------------------------
enum MsgIds {
    eGeneric = -1, eIgnore
};
//---------------------------------------------------------------------------------------------------------------------
template <typename T, int queue = 1>
class PublishTemplate {
public:
    PublishTemplate(const char* msg, float rate = 20) :
        mMsg { msg }, mRate(rate), mPublisher { mNodeHandle.advertise < T > (msg, queue) } {
        ROS_DEBUG_STREAM(ros::this_node::getName() << " " << typeid( *this).name() << mMsg);
    }
    virtual ~PublishTemplate();

    void delayTime(const double& t);
    void moveTime(const double& t);
    bool publish();
    ros::Publisher& publisher();
    virtual bool send();
    void subscribeWait();

    bool isSubscribed() const;
    virtual void waitForFinished() {
    }
    const uint32_t id() {
        return ++mId;
    }

protected:
    virtual void sendMessage() = 0;

    double mDelayTime { 0.05 };
    uint32_t mId { 1 };
    double mMoveTime { 0.5 };
    string mMsg;
    NodeHandle mNodeHandle;
    Rate mRate;
    Publisher mPublisher;
    T mTopic;
};

//---------------------------------------------------------------------------------------------------------------------
template <typename T, int queue>
inline PublishTemplate<T, queue>::~PublishTemplate()
{
    mPublisher.shutdown();
}
//---------------------------------------------------------------------------------------------------------------------
template <typename T, int queue>
inline void PublishTemplate<T, queue>::delayTime(const double& t) {
    mDelayTime = t;
}
//---------------------------------------------------------------------------------------------------------------------
template <typename T, int queue>
inline bool PublishTemplate<T, queue>::isSubscribed() const {
    return mPublisher.getNumSubscribers() != 0;
}
//---------------------------------------------------------------------------------------------------------------------
template <typename T, int queue>
inline void PublishTemplate<T, queue>::moveTime(const double& t) {
    mMoveTime = t;
}
//---------------------------------------------------------------------------------------------------------------------
template <typename T, int queue>
inline bool PublishTemplate<T, queue>::publish()
{
    if (isSubscribed()) {
        sendMessage();
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------------------------------------
template <typename T, int queue>
inline bool PublishTemplate<T, queue>::send() {
    mTopic.unique_id = ++mId;

    while ( !publish()) {
        mRate.sleep();
    }
    Duration(mDelayTime).sleep();
    return true;
}
//---------------------------------------------------------------------------------------------------------------------
template <typename T, int queue>
inline ros::Publisher& PublishTemplate<T, queue>::publisher() {
    return mPublisher;
}
//---------------------------------------------------------------------------------------------------------------------
template <typename T, int queue>
inline void PublishTemplate<T, queue>::subscribeWait() {
    while ( !isSubscribed()) {
        ROS_DEBUG_STREAM_THROTTLE(5.0, ros::this_node::getName() << " waiting for " << mMsg << " subscription");
        mRate.sleep();
    }
}
#endif /* SRC_PUBLISHTEMPLATE_H_ */
