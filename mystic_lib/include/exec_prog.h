/*
 * exec_prog.h
 *
 *  Created on: Feb 23, 2017
 *      Author: rmerriam
 */

#ifndef EXEC_PROG_H_
#define EXEC_PROG_H_

#include <unistd.h>
/*
 * Example Usage:
 *
 * const char* start_args[] = { "bash", "-c", "source /opt/nasa/indigo/setup.bash && rostopic pub -1 /srcsim/qual1/start std_msgs/Empty", 0 };
 *
 *     exec_prog(start_args);
 *
 */
//---------------------------------------------------------------------------------------------------------------------
inline
void exec_prog(const char* argv[]) {
    pid_t my_pid;

    if (0 == (my_pid = fork())) {
        if ( -1 == execvp(argv[0], (char **)argv)) {
            perror(argv[2]);
        }
    }
}

#endif /* EXEC_PROG_H_ */
