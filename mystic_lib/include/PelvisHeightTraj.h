/*
 * PelvisHeightTraj.h
 *
 *  Created on: Nov 29, 2016
 *      Author: rmerriam
 */

#ifndef PelvisHeightTraj_H_
#define PelvisHeightTraj_H_
//---------------------------------------------------------------------------------------------------------------------
#include <ihmc_msgs/PelvisHeightTrajectoryRosMessage.h>

using namespace ihmc_msgs;

#include "PublishTemplate.h"
//---------------------------------------------------------------------------------------------------------------------
class PelvisHeightTraj : public PublishTemplate<ihmc_msgs::PelvisHeightTrajectoryRosMessage> {
public:
    PelvisHeightTraj(const char* msg = "pelvis_height_trajectory");
    virtual ~PelvisHeightTraj() override = default;

    void set(const double& h);

private:
    virtual void sendMessage() override;

    double mHeight { 1.05 };
};
//---------------------------------------------------------------------------------------------------------------------
inline PelvisHeightTraj::PelvisHeightTraj(const char* msg) :
        PublishTemplate { msg } {
    ROS_DEBUG_STREAM(ros::this_node::getName() << " Pelvis Height Constructor ");
}
//---------------------------------------------------------------------------------------------------------------------
inline void PelvisHeightTraj::set(const double& h) {
    mHeight = h;
}

#endif /* MOVEHEAD_H_ */
