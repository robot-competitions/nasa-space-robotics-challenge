/*
 * WalkMotion.h
 *
 *  Created on: Dec 28, 2016
 *      Author: rmerriam
 */

#ifndef WalkMotion_H_
#define WalkMotion_H_

#include <SubscribeTopic.h>
#include <std_msgs/String.h>

//---------------------------------------------------------------------------------------------------------------------
class WalkMotion : public SubscribeTopic<std_msgs::StringPtr> {
public:
    WalkMotion(const char* msg = "robot_motion_status");
    virtual ~WalkMotion();

    bool isStanding() {
        return mMotionState;
    }
    bool isNotStanding() {
        return !isStanding();
    }
    void waitForStanding();
    void waitForNotStanding();

protected:
    virtual void process() override;

    bool mMotionState { false };
};
#endif /* WalkMotion_H_ */
