/*
 * Scoring.h
 *
 *  Created on: Nov 14, 2016
 *      Author: rmerriam
 */

#ifndef SRC_Scoring_H_
#define SRC_Scoring_H_

//  /srcsim/finals/task

//---------------------------------------------------------------------------------------------------------------------
#include <srcsim/Score.h>
using namespace ros;

#include "SubscribeTopic.h"

//---------------------------------------------------------------------------------------------------------------------
class Scoring : public SubscribeTopic<srcsim::ScorePtr> {
public:
    enum eStatus {
        eNone, eDetachToAttach, eAttachToLower, eLowerToStand, eStandToDetach, eDetachToMove
    };

    Scoring(const char* msg = "/srcsim/finals/score");
    virtual ~Scoring() override = default;

    const ros::Duration totalTime() const;
    const int32_t totalPenalty() const;
    const uint8_t totalScore() const;

private:
    virtual void process() override;
    mutable int32_t mTotalPenalty { };
};
//---------------------------------------------------------------------------------------------------------------------
inline Scoring::Scoring(const char* msg) :
        SubscribeTopic { msg } {
}
//---------------------------------------------------------------------------------------------------------------------
inline const ros::Duration Scoring::totalTime() const {
    return topic().total_completion_time;
}
//---------------------------------------------------------------------------------------------------------------------
inline const int32_t Scoring::totalPenalty() const {
    mTotalPenalty = 0;
    for (ros::Duration d : topic().checkpoint_penalties) {
        mTotalPenalty += d.sec;
    }
    return mTotalPenalty;
}
//---------------------------------------------------------------------------------------------------------------------
inline const uint8_t Scoring::totalScore() const {
    return topic().score;
}

//---------------------------------------------------------------------------------------------------------------------
inline void Scoring::process() {
}

#endif /* SRC_Scoring_H_ */
