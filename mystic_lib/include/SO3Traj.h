/*
 * SO3Traj.h
 *
 *  Created on: Dec 6, 2016
 *      Author: rmerriam
 */

#ifndef SRC_SO3Traj_H_
#define SRC_SO3Traj_H_
//---------------------------------------------------------------------------------------------------------------------
#include <ros_transform.h>
using namespace tf2;

#include <ihmc_msgs/SO3TrajectoryPointRosMessage.h>
using namespace ihmc_msgs;

#include "PublishTemplate.h"
#include "WorldFrame.h"
#include <RPY.h>
//---------------------------------------------------------------------------------------------------------------------
template <typename T, int queue = 1>
class SO3Traj : public PublishTemplate<T, queue>, public WorldFrame {
public:
    SO3Traj(const char* msg, const string& frame, float rate = 20);
    SO3Traj(const char* msg, const string& frame, const string& base, float rate = 20);
    virtual ~SO3Traj() = default;

    void incPose(const Vector3& vec);
    void incPose(const RPY& vec);
    void incPose(const double& roll, const double& pitch, const double& yaw);

    void resetPose();
    void sendMessage();

    void setPose(const Vector3& vec);
    void setPose(const RPY& vec);
    void setPose(const double& roll, const double& pitch, const double& yaw);
private:

};

//---------------------------------------------------------------------------------------------------------------------
template <typename T, int queue>
inline SO3Traj<T, queue>::SO3Traj(const char* msg, const string& frame, float rate) :
    PublishTemplate<T, queue>(msg, rate), WorldFrame(frame) {
}
//---------------------------------------------------------------------------------------------------------------------
template <typename T, int queue>
inline SO3Traj<T, queue>::SO3Traj(const char* msg, const string& frame, const string& base, float rate) :
    PublishTemplate<T, queue>(msg, rate), WorldFrame(frame, base) {
}
//---------------------------------------------------------------------------------------------------------------------
template <typename T, int queue>
inline void SO3Traj<T, queue>::resetPose() {
    updateRPY();
    incPose( -mRoll, -mPitch, -mYaw);
}
//---------------------------------------------------------------------------------------------------------------------
template <typename T, int queue>
inline void SO3Traj<T, queue>::incPose(const Vector3& vec) {
    incPose(vec.getX(), vec.getY(), vec.getZ());
}
//---------------------------------------------------------------------------------------------------------------------
template <typename T, int queue>
inline void SO3Traj<T, queue>::incPose(const RPY& vec) {
    incPose(vec.mRoll, vec.mPitch, vec.mYaw);
}
//---------------------------------------------------------------------------------------------------------------------
template <typename T, int queue>
inline void SO3Traj<T, queue>::setPose(const Vector3& vec) {
    setPose(vec.getX(), vec.getY(), vec.getZ());
}
//---------------------------------------------------------------------------------------------------------------------
template <typename T, int queue>
inline void SO3Traj<T, queue>::setPose(const RPY& vec) {
    setPose(vec.mRoll, vec.mPitch, vec.mYaw);
}
//---------------------------------------------------------------------------------------------------------------------
template <typename T, int queue>
inline void SO3Traj<T, queue>::incPose(const double& roll, const double& pitch, const double& yaw) {
    updateRPY();
    setPose(roll + mRoll, pitch + mPitch, yaw + mYaw);
}
//---------------------------------------------------------------------------------------------------------------------
template <typename T, int queue>
inline void SO3Traj<T, queue>::setPose(const double& roll, const double& pitch, const double& yaw) {
    SO3TrajectoryPointRosMessage traj;

    traj.unique_id = ++PublishTemplate<T, queue>::mId;
    traj.time = PublishTemplate<T, queue>::mMoveTime;

    Quaternion q;
    q.setRPY(roll, pitch, yaw);

    traj.orientation.x = q.x();
    traj.orientation.y = q.y();
    traj.orientation.z = q.z();
    traj.orientation.w = q.w();
    this->mTopic.taskspace_trajectory_points.push_back(traj);
}
//---------------------------------------------------------------------------------------------------------------------
template <typename T, int queue>
inline void SO3Traj<T, queue>::sendMessage() {
    this->publisher().publish(this->mTopic);
    ROS_DEBUG_STREAM(ros::this_node::getName() << this->mMsg << " sent ");
    this->mTopic.taskspace_trajectory_points.clear();
}
#endif /* SRC_SO3Traj_H_ */
