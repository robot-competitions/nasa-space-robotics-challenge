/*
 * FootstepStatus.h
 *
 *  Created on: Nov 14, 2016
 *      Author: rmerriam
 */

#ifndef SRC_FootstepStatus_H_
#define SRC_FootstepStatus_H_
//---------------------------------------------------------------------------------------------------------------------
#include <ihmc_msgs/FootstepStatusRosMessage.h>
using namespace ros;

#include "SubscribeTopic.h"

//---------------------------------------------------------------------------------------------------------------------
class FootstepStatus : public SubscribeTopic<ihmc_msgs::FootstepStatusRosMessagePtr> {
public:
    FootstepStatus(const char* msg = "footstep_status");
    FootstepStatus(const FootstepStatus& w) :
            FootstepStatus(w.mSubscriber.getTopic().c_str()) {
    }

    bool steppingDone();

    int stepCnt() const {
        return mStepCnt;
    }

    void stepCnt(int stepCnt) {
        mStepCnt = stepCnt;
    }

private:
    virtual void process() override;
    ihmc_msgs::FootstepStatusRosMessage mFootStatus[2];
    int mStepCnt {};

};

//---------------------------------------------------------------------------------------------------------------------
inline
bool FootstepStatus::steppingDone() {
    return mStepCnt == 0;
}
#endif /* SRC_FootstepStatus_H_ */
