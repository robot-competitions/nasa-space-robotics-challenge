/*
 * FootstepDataMsg.h
 *
 *  Created on: Nov 14, 2016
 *      Author: rmerriam
 */

#ifndef SRC_FootstepDataMsg_H_
#define SRC_FootstepDataMsg_H_
//---------------------------------------------------------------------------------------------------------------------
#include <WorldFrame.h>

#include <ihmc_msgs/FootstepDataRosMessage.h>
using namespace ros;
using namespace ihmc_msgs;
static constexpr const char* foot_frames[2] = { "leftFoot", "rightFoot" };
//=====================================================================================================================
class FootstepDataMsg : public WorldFrame {
public:
    FootstepDataMsg(const mys::Side side) :
        WorldFrame(foot_frames[side], "world"), mSide(side) {
    }
    virtual ~FootstepDataMsg() = default;

    const Vec3 getLocation() const;
    const tf2::Quaternion getOrientation() const;
    const FootstepDataRosMessage& getMsg() const;

    void alignPose(const ::geometry_msgs::Quaternion& q);
    void setLocation(const double x, const double y, const double z);
    void setLocation(const Vec3& vec);

    void setRotation(const double x, FootstepDataMsg& r_fdm);

    void stepHeight(const double h);

    double swing() const;
    void swing(const double s);

    double transfer() const;
    void transfer(const double t);

    enum TrajectoryType {
        eDefault, eObstacleClearance, eCustom
    };
    void trajectoryType(const TrajectoryType t);

    void update(const string& rebase = "");

private:

    ihmc_msgs::FootstepDataRosMessage mFootstep;

    mys::Side mSide;
    double mStepHeight { 0.05 };
    double mSwing { 0.8 };
    double mTransfer { 0.9 };
    TrajectoryType mTrajectoryType { eDefault };
    //    TrajectoryType mTrajectoryType { eObstacleClearance };

    void incLocation(const Vec3& res);
};

//=====================================================================================================================
inline
double FootstepDataMsg::swing() const {
    return mSwing;
}
//---------------------------------------------------------------------------------------------------------------------
inline
void FootstepDataMsg::swing(const double s) {
    mSwing = s;
}
//---------------------------------------------------------------------------------------------------------------------
inline
double FootstepDataMsg::transfer() const {
    return mTransfer;
}
//---------------------------------------------------------------------------------------------------------------------
inline
void FootstepDataMsg::transfer(const double t) {
    mTransfer = t;
}
//---------------------------------------------------------------------------------------------------------------------
inline
void FootstepDataMsg::trajectoryType(const TrajectoryType t) {
    mTrajectoryType = t;
}
//---------------------------------------------------------------------------------------------------------------------
inline const Vec3 FootstepDataMsg::getLocation() const {
    return geo2tf2(mFootstep.location);
}

inline const tf2::Quaternion FootstepDataMsg::getOrientation() const {
    return geo2tf2(mFootstep.orientation);
}

//---------------------------------------------------------------------------------------------------------------------
inline const ihmc_msgs::FootstepDataRosMessage& FootstepDataMsg::getMsg() const {
    return mFootstep;
}

inline void FootstepDataMsg::setLocation(const double x, const double y, const double z) {
    setLocation(Vec3(x, y, z));
}

//---------------------------------------------------------------------------------------------------------------------
inline
void FootstepDataMsg::stepHeight(const double h) {
    mStepHeight = h;
}
//=====================================================================================================================
//inline ostream& operator<<(ostream& os, const Vec3& v) {
//    os << '[' << v.getX() << ", " << v.getY() << ", " << v.getZ() << ']';
//    return os;
//}
////---------------------------------------------------------------------------------------------------------------------
//inline ostream& operator<<(ostream& os, const ::geometry_msgs::Vector3& v) {
//    os << '[' << v.x << ", " << v.y << ", " << v.z << ']';
//    return os;
//}
////---------------------------------------------------------------------------------------------------------------------
//inline ostream& operator<<(ostream& os, const tf2::Quaternion& q) {
//    os << q.getX() << " " << q.getY() << " " << q.getZ() << " " << q.getW();
//    return os;
//}
////---------------------------------------------------------------------------------------------------------------------
//inline ostream& operator<<(ostream& os, const ::geometry_msgs::Quaternion& q) {
//    os << q.x << " " << q.y << " " << q.z << " " << q.w;
//    return os;
//}

#endif /* SRC_FootstepDataMsg_H_ */
