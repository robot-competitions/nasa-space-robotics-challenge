/*
 * WorldFrame.h
 *
 *  Created on: Mar 20, 2017
 *      Author: rmerriam
 */

#ifndef WORLDFRAME_H_
#define WORLDFRAME_H_
#include "mystic.h"
#include <ros_transform.h>
//---------------------------------------------------------------------------------------------------------------------
class WorldFrame {
public:
    WorldFrame(const string& target, const string& base = "torso");
    virtual ~WorldFrame() = default;

    const tf2::Transform& update(const string& rebase = "");

    const tf2::Quaternion getRotation() const;
    const Vector3 getRPY() const;
    const Vector3 updateRPY();
    const tf2::Transform& getTransform() const;
    const tf2::Transform& getWorldLocation(const Vector3 vec);
    const tf2::Transform& getWorldLocation(const double& x, const double& y, const double& z);
    const tf2::Vector3 getTranslation() const;

    const tf2::Transform getFrameLocation(const double& x, const double& y, const double& z, const string& child, const string& base);

protected:

    mutable bool mRPYValid { false };
    mutable double mRoll { };
    mutable double mPitch { };
    mutable double mYaw { };

private:
    string mTarget;
    string mBase;

    tf2_ros::Buffer tfBuffer;
    tf2_ros::TransformListener tfListener { tfBuffer };

    tf2::Transform mTransform;
};
//---------------------------------------------------------------------------------------------------------------------
inline WorldFrame::WorldFrame(const string& target, const string& base) :
    mTarget(target), mBase(base) {
    ROS_DEBUG_STREAM(ros::this_node::getName() << " " << typeid(*this).name() << " [" << target << "]");
}
//---------------------------------------------------------------------------------------------------------------------
inline const tf2::Transform& WorldFrame::getTransform() const {
    return mTransform;
}
//---------------------------------------------------------------------------------------------------------------------
inline const tf2::Transform& WorldFrame::getWorldLocation(const Vector3 vec) {
    return getWorldLocation(vec.getX(), vec.getY(), vec.getZ());
}
//---------------------------------------------------------------------------------------------------------------------
inline const tf2::Vector3 WorldFrame::getTranslation() const {
    return mTransform.getOrigin();
}
//---------------------------------------------------------------------------------------------------------------------
inline const tf2::Quaternion WorldFrame::getRotation() const {
    return mTransform.getRotation();
}
//---------------------------------------------------------------------------------------------------------------------
inline const tf2::Vector3 WorldFrame::updateRPY() {
    update();
    return getRPY();
}

#endif /* WORLDFRAME_H_ */
