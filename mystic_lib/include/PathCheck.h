/*
 * PathCheck.h
 *
 *  Created on: Apr 26, 2017
 *      Author: rmerriam
 */

#ifndef PATHCHECK_H_
#define PATHCHECK_H_
//---------------------------------------------------------------------------------------------------------------------
#include "WorldFrame.h"
#include "ImageVision.h"

class PointCloud2Sub;
//---------------------------------------------------------------------------------------------------------------------
class PathCheck : public WorldFrame {
public:
    PathCheck(PointCloud2Sub& pc2) :
        WorldFrame { "left_camera_optical_frame", "leftFoot" }, mCloudSub(pc2) {
    }
    ~PathCheck() = default;

    const int endPixel() const;
    const Mat& path() const;
    const Rect& subImage() const;
    const Vector3& worldLocation() const;
    const Vector3& visionLocation() const;

    bool process() {
        bool res = false;

        if (mCloudSub.isValid()) {
            processImage();
            findPathEndPixel();
            res = isPathClear();
            if (res) {
                getCamFramePosition();
                res = !std::isnan(mVisionLoc.getX());
            }
        }
        return res;
    }

private:
    void findPathEndPixel();
    void getCamFramePosition();
    bool isPathClear();
    void processImage();

    PointCloud2Sub& mCloudSub;
    Mat mPath;
    Rect mSubImg;
    Vector3 mWorldLoc;
    Vector3 mVisionLoc { };
    int mEndPixel { };

    static constexpr double col_offset { 0.40 };
    static constexpr double row_offset { 1.00 };
};
//---------------------------------------------------------------------------------------------------------------------
inline const int PathCheck::endPixel() const {
    return mEndPixel;
}
//---------------------------------------------------------------------------------------------------------------------
inline const cv::Mat& PathCheck::path() const {
    return mPath;
}
//---------------------------------------------------------------------------------------------------------------------
inline const cv::Rect& PathCheck::subImage() const {
    return mSubImg;
}
//---------------------------------------------------------------------------------------------------------------------
inline const Vector3& PathCheck::worldLocation() const {
    return mWorldLoc;
}
//---------------------------------------------------------------------------------------------------------------------
inline const Vector3& PathCheck::visionLocation() const {
    return mVisionLoc;
}

#endif /* PATHCHECK_H_ */
