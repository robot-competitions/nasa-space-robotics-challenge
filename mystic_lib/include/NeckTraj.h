/*
 * MoveArm.h
 *
 *  Created on: Dec 6, 2016
 *      Author: rmerriam
 */

#ifndef SRC_NeckTraj_H_
#define SRC_NeckTraj_H_
//---------------------------------------------------------------------------------------------------------------------
#include <ros_transform.h>
using namespace tf2;

#include <ihmc_msgs/NeckTrajectoryRosMessage.h>
using namespace ihmc_msgs;

//  neck_trajectory:=/ihmc_ros/valkyrie/control/neck_trajectory

#include "PublishTemplate.h"
#include "RPY.h"
//---------------------------------------------------------------------------------------------------------------------
class NeckTraj : public PublishTemplate<ihmc_msgs::NeckTrajectoryRosMessage> {
public:
    NeckTraj(const char* msg = "neck_trajectory");
    virtual ~NeckTraj() override {
    }

    void resetPose();
    void incPose(const Vector3& vec) {
        setPose(vec);
    }
    void incPose(const RPY& vec) {
        setPose(vec);
    }
    void incPose(const double& lo_pitch, const double& yaw, const double& hi_pitch) {
        setPose(lo_pitch, yaw, hi_pitch);
    }

    void setPose(const Vector3& vec);
    void setPose(const RPY& vec);
    void setPose(const double& lo_pitch, const double& yaw, const double& hi_pitch);

private:
    virtual void sendMessage() override;
    void setOneDeg(const double& pos, OneDoFJointTrajectoryRosMessage& traj);
};
//---------------------------------------------------------------------------------------------------------------------
inline void NeckTraj::resetPose() {
    setPose(rpy_zero);
}
//---------------------------------------------------------------------------------------------------------------------
inline void NeckTraj::setPose(const Vector3& vec) {
    setPose(vec.getX(), vec.getY(), vec.getZ());
}
//---------------------------------------------------------------------------------------------------------------------
inline void NeckTraj::setPose(const RPY& vec) {
    setPose(vec.mRoll, vec.mPitch, vec.mYaw);
}

#endif /* SRC_NeckTraj_H_ */
