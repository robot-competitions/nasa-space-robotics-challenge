/*
 * PointCloud2Sub.h
 *
 *  Created on: Sep 14, 2016
 *      Author: rmerriam
 */

#ifndef SRC_PointCloud2Sub_H_
#define SRC_PointCloud2Sub_H_

//  points2:=/multisense/camera/points2

#include <SubscribeTopic.h>
#include <pcl_ros/point_cloud.h>
using namespace sensor_msgs;
//---------------------------------------------------------------------------------------------------------------------
typedef pcl::PointCloud<pcl::PointXYZRGB> PCloud;
typedef boost::shared_ptr<const PCloud> PCloudConstPtr;
typedef boost::shared_ptr<PCloud> PCloudPtr;
//---------------------------------------------------------------------------------------------------------------------
class PointCloud2Sub : public SubscribeTopic<PCloudPtr> {

public:
    PointCloud2Sub(const char* msg = "points2");

    const PCloud& pointCloud();

//    geometry_msgs::Vector3 transform(const geometry_msgs::Vector3);

private:
    virtual void process() override;

    uint32_t mImgSeq { };
};
//---------------------------------------------------------------------------------------------------------------------
inline const PCloud& PointCloud2Sub::pointCloud() {
    return mSubscribeTopic;
}

#if 0
/*
 * PointCloud2Sub.h
 *
 *  Created on: Sep 14, 2016
 *      Author: rmerriam
 */

#ifndef SRC_PointCloud2Sub_H_
#define SRC_PointCloud2Sub_H_

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
using namespace cv;

#include <SubscribeTopic.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl_ros/point_cloud.h>
using namespace sensor_msgs;
//---------------------------------------------------------------------------------------------------------------------
class PointCloud2Sub : public SubscribeTopic<PointCloud2Ptr> {
public:
    PointCloud2Sub(const char* msg = "points2");

    bool isImageOk();
    void convertToImage();
    const PointCloud2& pointCloud();

    geometry_msgs::Vector3 transform(const geometry_msgs::Vector3);

private:
    virtual void process() override;
    void updateWindow(const cv::Mat& image);

    uint32_t mImgSeq {};

    cv::Mat mCurImage;
    const std::string OPENCV_WINDOW = "test pcl2";

};
//---------------------------------------------------------------------------------------------------------------------
inline const sensor_msgs::PointCloud2& PointCloud2Sub::pointCloud() {
    return mSubscribeTopic;
}
//---------------------------------------------------------------------------------------------------------------------
inline bool PointCloud2Sub::isImageOk() {
    return mSubscribeTopic.width != 0;
}

#endif
#endif

#endif /* SRC_IMAGEFETCH_H_ */
